#!/bin/bash

funcoes=("aplicacao.atualizar_vwm_painel_awifs()" "aplicacao.atualizar_vwm_painel_deter()" "aplicacao.atualizar_vwm_painel_landsat()" "aplicacao.atualizar_vwm_painel_radarsat2()" "aplicacao.atualizar_vwm_painel_prodes()" "funai.atualizar_visoes_terra_indigena()")

database="cmr_funai"

for ((x=0; x < ${#funcoes[*]}; x++)) ; do
  echo "iniciando atualizacao ${funcoes[$x]}"
  echo ""
  time psql -h 192.168.100.50 -U postgres -d ${database} -c "SELECT ${funcoes[$x]}";
  echo ""
  echo "fim da atualizacao ${funcoes[$x]}"
done
