#!/bin/bash

funcoes=("funai.atualizar_visoes_diaria()" "catalogo.atualizar_visoes_catalogo()")

database="cmr_funai"

for ((x=0; x < ${#funcoes[*]}; x++)) ; do
  echo "iniciando atualizacao ${funcoes[$x]}"
  echo ""
  time psql -h 192.168.100.50 -U postgres -d ${database} -c "SELECT ${funcoes[$x]}";
  echo ""
  echo "fim da atualizacao ${funcoes[$x]}"
done
