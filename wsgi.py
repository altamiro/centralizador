# -*- encoding: utf-8 -*-

import os
import sys

# ativa o virtualenv do projeto
# activate_this = '/var/www/html/centralize/env/bin/activate_this.py'
# exec(open(activate_this).read())

path = os.path.join(os.path.dirname(__file__),os.pardir)
if path not in sys.path:
    sys.path.append(path)

from app import app as application