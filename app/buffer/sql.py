# -*- encoding: utf-8 -*-
# Copyright 2017 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

# insert da tabela img_mascara_antropismo_consolidado_buffer_ate2015_a
INSERT_IMG_MASCARA_ANTROPISMO_CONSOLIDADO_BUFFER_ATE_2016 = "INSERT INTO funai.img_mascara_antropismo_consolidado_buffer_ate2015_a(no_classe, sg_classe, geom) VALUES (%s, %s, ST_Multi(ST_Buffer(ST_Setsrid(ST_Geometryfromtext(%s),4674),0)))"

# select pra pegar o proximo nu_entrega
SELECT_MAX_NU_ENTREGA_TB_ENTREGA_BUFFER_ATE2015 = "SELECT COALESCE(MAX(nu_entrega) + 1,0)::smallint AS nu_entrega, to_char(NOW(),'YYYY-MM-DD')::date AS dt_entrega, 'f'::boolean AS st_finalizado FROM funai.tb_entrega_buffer_ate2015"
