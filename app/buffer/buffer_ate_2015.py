# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>
import psycopg2
import psycopg2.extras
import os, subprocess

from unipath import Path
from osgeo import ogr
from app import app
from sqlalchemy.sql import text

from .sql import *
from ..models import *

db2 = psycopg2.connect(app.config["PSYCOPG2_DATABASE_URI"])  # psycopg2
conn = db2.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)


def _criar_nova_entrega_buffer():
    conn.execute(SELECT_MAX_NU_ENTREGA_TB_ENTREGA_BUFFER_ATE2015)
    r = conn.fetchone()
    s_nu_entrega = r.nu_entrega
    s_dt_entrega = r.dt_entrega.strftime("%Y-%m-%d")
    s_st_finalizado = r.st_finalizado

    try:
        tb_usuario_id = getSessaoUsuario('id')

        entregaBufferAte2015 = TbEntregaBufferAte2015(tb_usuario_id=tb_usuario_id, nu_entrega=s_nu_entrega,
                                                      dt_entrega=s_dt_entrega, st_finalizado=s_st_finalizado,
                                                      qt_total_registro=0)

        db.session.add(entregaBufferAte2015)
        db.session.commit()
        return entregaBufferAte2015.id
    except (Exception, ValueError) as e:
        db.session.rollback()
        app.logger.error("ROLLBACK")
        app.logger.error("__error_insert_tb_entrega_buffer_ate2015__")
        app.logger.exception("error: %s", e)


def novo_pacote(directory):
    json = {
        "message": "Carga dos dados de auditoria buffer efetuada com sucesso.",
        "status": 1,
        "error": ""
    }

    booTransacao = True
    s_tb_entrega_id = _criar_nova_entrega_buffer()

    for arquivo in sorted(os.listdir(directory)):
        if arquivo.endswith(".shp"):
            # pega o arquivo com caminho completo
            s_caminho_arquivo = os.path.join(directory, arquivo)

            shapefile = ogr.Open(s_caminho_arquivo)
            layer = shapefile.GetLayer(0)

            for i in range(layer.GetFeatureCount()):
                feature = layer.GetFeature(i)
                s_geom = feature.GetGeometryRef().ExportToWkt()
                s_no_classe = 'Antropismo Consolidado'
                s_sg_classe = 'AC'

                try:
                    conn.execute("BEGIN")
                    app.logger.info("(BEGIN - Auditoria Buffer) Arquivo: %s", s_caminho_arquivo)

                    conn.execute(INSERT_IMG_MASCARA_ANTROPISMO_CONSOLIDADO_BUFFER_ATE_2016,
                                 [s_no_classe, s_sg_classe, s_geom])

                    conn.execute("COMMIT")
                    app.logger.info("(COMMIT - Auditoria Buffer) Arquivo: %s", s_caminho_arquivo)

                    conn.execute("SELECT MAX(id) AS id FROM funai.img_mascara_antropismo_consolidado_buffer_ate2015_a")
                    last_inserted_id = conn.fetchone().id

                    usuarioEntregaBufferAte2015 = TbUsuarioEntregaBufferAte2015(tb_entrega_id=s_tb_entrega_id,
                                                                                img_mascara_buffer_ate2015_id=last_inserted_id)
                    db.session.add(usuarioEntregaBufferAte2015)
                    db.session.commit()

                except (Exception, ValueError, psycopg2.DataError) as e:
                    conn.execute("ROLLBACK")
                    app.logger.error("ROLLBACK")
                    app.logger.error("__error_insert_img_mascara_antropismo_consolidado_buffer_ate2015_a__")
                    app.logger.exception("error: %s", e)
                    json["error"] = e
                    db.engine.execute(text("DELETE FROM funai.tb_entrega_buffer_ate2015 WHERE id = {}".format(
                        s_tb_entrega_id)).execution_options(autocommit=True))
                    booTransacao = False

    if booTransacao:
        db.engine.execute(
            text(
                "UPDATE funai.tb_entrega_buffer_ate2015 AS e SET qt_total_registro=(SELECT COUNT(id)::integer AS qt_total_registro FROM funai.tb_usuario_entrega_buffer_ate2015 WHERE tb_entrega_id = e.id), st_finalizado='t' WHERE id = {}".format(
                    s_tb_entrega_id)).execution_options(
                autocommit=True))
        if os.path.exists(directory):
            Path(directory).rmtree()
    else:
        json["message"] = "Ocorreu um erro efetuar carga dos dados de Auditoria Buffer. Favor verificar!"
        json["status"] = 0

    return json


def gerar_shapefile_pacote(entrega_id):
    entregaBufferAte2015 = TbEntregaBufferAte2015.query.get(entrega_id)
    no_arquivo = "shapefile_auditoria_buffer_entrega_{}_data_{}".format(entregaBufferAte2015.nu_entrega,
                                                                        entregaBufferAte2015.dt_entrega.strftime(
                                                                            "%d_%m_%Y"))
    no_caminho_tmp = app.config['UPLOAD_FOLDER']
    no_caminho = app.config['SHP_FOLDER']

    command_rm_shp = "rm -rf {}*.zip".format(no_caminho)
    subprocess.call(command_rm_shp, shell=True)

    ogr = "ogr2ogr -lco ENCODING=UTF-8 -skipfailures -a_srs EPSG:4674 -t_srs EPSG:4674 -s_srs EPSG:4674 -nlt POLYGON -f 'ESRI Shapefile' {}/{}.shp PG:'host={} user={} dbname={} password={}' -sql 'SELECT * FROM aplicacao.vw_dado_shp_mascara_antropismo_consolidado_buffer_ate2015 WHERE entrega_id = {}' 2> /dev/null".format(
        no_caminho_tmp, no_arquivo, app.config['DBHOST'], app.config['DBUSER'], app.config['DBNAME'],
        app.config['DBPASS'], entregaBufferAte2015.id)

    command_zip = "cd {} && zip -r {}{}.zip {}*".format(no_caminho_tmp, no_caminho, no_arquivo, no_arquivo)
    command_rm = "rm -rf {}/{}*".format(no_caminho_tmp, no_arquivo)

    subprocess.call(ogr, shell=True)
    subprocess.call(command_zip, shell=True)
    subprocess.call(command_rm, shell=True)
    return "{}.zip".format(no_arquivo)
