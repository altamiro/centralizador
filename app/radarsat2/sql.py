# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

# selecione o numero maximo de entrega cadastrada.
QUERY_PROXIMA_ENTREGA = "SELECT (COALESCE(MAX(nu_entrega),0) + 1)::smallint AS nu_entrega FROM funai.tb_entrega_trimestral_radarsat2 WHERE st_finalizado = 't'"

# insert da tabela tb_entrega_trimestral_radarsat2
INSERT_ENTREGA_TRIMESTRAL = "INSERT INTO funai.tb_entrega_trimestral_radarsat2(nu_entrega, dt_entrega) VALUES (%s, %s)"

# delete da tabela tb_entrega_trimestral_radarsat2
DELETE_ENTREGA_TRIMESTRAL = "DELETE FROM funai.tb_entrega_trimestral_radarsat2 WHERE st_finalizado = 'f' AND dt_entrega = %s"

# update da tabela tb_entrega_trimestral_radarsat2
UPDATE_ENTREGA_TRIMESTRAL = "UPDATE funai.tb_entrega_trimestral_radarsat2 AS e SET st_finalizado = 't', qt_total_registro_alerta=(SELECT COUNT(id) AS qt_total_registro_alerta FROM funai.img_alerta_trimestral_radarsat2_a AS a WHERE a.tb_entrega_trimestral_radarsat2_id = e.id), qt_total_registro_agua=(SELECT COUNT(id) AS qt_total_registro_agua FROM funai.img_agua_trimestral_radarsat2_a AS a WHERE a.tb_entrega_trimestral_radarsat2_id = e.id) WHERE dt_entrega = %s"

# busca o numero da entrega de acordo com a data informada
QUERY_BUSCAR_ENTREGA_POR_DATA = "SELECT id, nu_entrega FROM funai.tb_entrega_trimestral_radarsat2 WHERE dt_entrega = %s"

# verificar se existe registro com a data informada
QUERY_TOTAL_ENTREGA_POR_DATA = "SELECT COUNT(id) AS total FROM funai.tb_entrega_trimestral_radarsat2 WHERE dt_entrega = %s"

# pega o max id de acordo a data informada
QUERY_MAX_ENTREGA_POR_DATA = "SELECT MAX(id) AS id, COUNT(id) AS total FROM funai.tb_entrega_trimestral_radarsat2 WHERE dt_entrega = %s"

# busca os dados da classe de acordo com nome informado
QUERY_CLASSE = "SELECT id, no_classe, no_classe_mda FROM funai.tb_classe_radarsat2 WHERE LOWER(TRIM(no_classe_mda)) = LOWER(TRIM(%s))"

# verificar se existe registro com a data informada
QUERY_TOTAL_CLASSE_POR_NO_CLASSE_MDA = "SELECT COUNT(id) AS total FROM funai.tb_classe_radarsat2 WHERE LOWER(TRIM(no_classe_mda)) = LOWER(TRIM(%s))"

# insert da tabela tb_classe_radarsat2
INSERT_CLASSE_MDA = "INSERT INTO funai.tb_classe_radarsat2(no_classe_mda) VALUES (TRIM(%s))"

# insert da tabela img_agua_trimestral_radarsat2_a
INSERT_AGUA_TRIMESTRAL_RADARSAT2 = "INSERT INTO funai.img_agua_trimestral_radarsat2_a(tb_entrega_trimestral_radarsat2_id, nu_frame, geom) VALUES (%s, %s, ST_Multi(ST_Buffer(ST_Setsrid(ST_Geometryfromtext(%s),4674),0)))"

# insert da tabela img_alerta_trimestral_radarsat2_a
INSERT_ALERTA_TRIMESTRAL_RADARSAT2 = "INSERT INTO funai.img_alerta_trimestral_radarsat2_a(tb_entrega_trimestral_radarsat2_id, tb_classe_radarsat2_id, nu_frame, nu_feicao_mda, nu_votes, nu_intensidade_dano, dt_t_zero, dt_t_um, dt_deteccao, geom) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, ST_Multi(ST_Buffer(ST_Setsrid(ST_Geometryfromtext(%s),4674),0)))"

"""
    Aqui inicia as consultas, inserts, update e delete do alerta radarsat2 mensal
"""

# update da tabela tb_entrega_radarsat2
UPDATE_ENTREGA_MENSAL = "UPDATE funai.tb_entrega_radarsat2 AS e SET st_finalizado = 't', qt_total_registro=(SELECT COUNT(id) AS qt_total_registro FROM funai.vw_alerta_radarsat2_a WHERE nu_entrega = e.nu_entrega) WHERE dt_entrega = %s"

# insert da tabela img_intersecao_radarsat2_a
INSERT_INTERSECAO_RADARSAT2 = "INSERT INTO funai.img_intersecao_radarsat2_a(tb_entrega_radarsat2_id, nu_frame, dt_t_zero, dt_t_um, geom)  VALUES (%s, %s, %s, %s, ST_Multi(ST_Buffer(ST_Setsrid(ST_Geometryfromtext(%s),4674),0)))"

# delete da tabela tb_entrega_radarsat2
DELETE_INTERSECAO_RADARSAT2 = "DELETE FROM funai.img_intersecao_radarsat2_a WHERE tb_entrega_radarsat2_id = %s"

# busca o numero da entrega de acordo com a data informada
QUERY_INTERSECAO_POR_ENTREGA_NU_FRAME = "SELECT id FROM funai.img_intersecao_radarsat2_a WHERE tb_entrega_radarsat2_id = %s AND nu_frame = %s"

# insert da tabela img_alerta_radarsat2_a
INSERT_ALERTA_MENSAL_RADARSAT2 = "INSERT INTO funai.img_alerta_radarsat2_a(img_intersecao_radarsat2_a_id, tb_classe_radarsat2_id, nu_feicao_mda, nu_votes, nu_intensidade_dano, dt_t_zero, dt_t_um, dt_deteccao, geom) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, ST_Multi(ST_Buffer(ST_Setsrid(ST_Geometryfromtext(%s),4674),0)))"

# selecione o numero maximo de entrega cadastrada.
QUERY_PROXIMA_ENTREGA_MENSAL = "SELECT (COALESCE(MAX(nu_entrega),0) + 1)::smallint AS nu_entrega FROM funai.tb_entrega_radarsat2 WHERE st_finalizado = 't'"

# insert da tabela tb_entrega_radarsat2
INSERT_ENTREGA_MENSAL = "INSERT INTO funai.tb_entrega_radarsat2(nu_entrega, dt_entrega) VALUES (%s, %s)"

# delete da tabela tb_entrega_radarsat2
DELETE_ENTREGA_MENSAL = "DELETE FROM funai.tb_entrega_radarsat2 WHERE st_finalizado = 'f' AND dt_entrega = %s"

# busca o numero da entrega de acordo com a data informada
QUERY_BUSCAR_ENTREGA_MENSAL_POR_DATA = "SELECT id, nu_entrega FROM funai.tb_entrega_radarsat2 WHERE dt_entrega = %s"

# verificar se existe registro com a data informada
QUERY_TOTAL_ENTREGA_MENSAL_POR_DATA = "SELECT COUNT(id) AS total FROM funai.tb_entrega_radarsat2 WHERE dt_entrega = %s"

# pega o max id de acordo a data informada
QUERY_MAX_ENTREGA_MENSAL_POR_DATA = "SELECT MAX(id) AS id, COUNT(id) AS total FROM funai.tb_entrega_radarsat2 WHERE dt_entrega = %s"

# busca o id do alerta radarsat2 de acordo com os parametros informado
QUERY_ALERTA_POR_FEICAO_MDA_ENTREGA_FRAME = "SELECT id FROM funai.vw_informacao_alerta_radarsat2 WHERE nu_feicao_mda = %s AND nu_frame = %s AND nu_entrega = %s"

# insert da tabela img_alerta_dano_maior_radarsat2_a
INSERT_ALERTA_MENSAL_RADARSAT2_DANO_MAIOR = "INSERT INTO funai.img_alerta_dano_maior_radarsat2_a(img_alerta_radarsat2_a_id, nu_feicao_mda_original, geom) VALUES (%s, %s, ST_Multi(ST_Buffer(ST_Setsrid(ST_Geometryfromtext(%s),4674),0)))"

# deleta os dados da tabela img_alerta_dano_maior_radarsat2_a de acordo com o nu_entrega
DELETE_IMG_ALERTA_DANO_MAIOR_RADARSAT2_A = "DELETE FROM funai.img_alerta_dano_maior_radarsat2_a AS d WHERE d.img_alerta_radarsat2_a_id IN (SELECT a.id FROM funai.img_alerta_radarsat2_a AS a WHERE a.img_intersecao_radarsat2_a_id IN (SELECT i.id FROM funai.img_intersecao_radarsat2_a AS i INNER JOIN funai.tb_entrega_radarsat2 AS e ON (e.id = i.tb_entrega_radarsat2_id) WHERE e.nu_entrega = %s))"

# deleta os dados da tabela img_alerta_radarsat2_a de acordo com o nu_entrega
DELETE_IMG_ALERTA_RADARSAT2_A = "DELETE FROM funai.img_alerta_radarsat2_a WHERE img_intersecao_radarsat2_a_id IN (SELECT i.id FROM funai.img_intersecao_radarsat2_a AS i INNER JOIN funai.tb_entrega_radarsat2 AS e ON (e.id = i.tb_entrega_radarsat2_id) WHERE e.nu_entrega = %s)"

# deleta os dados da tabela img_intersecao_radarsat2_a de acordo com o nu_entrega
DELETE_IMG_INTERSECAO_RADARSAT2_A = "DELETE FROM funai.img_intersecao_radarsat2_a AS i WHERE i.tb_entrega_radarsat2_id IN (SELECT e.id FROM funai.tb_entrega_radarsat2 AS e WHERE e.nu_entrega = %s)"

# deleta os dados da tabela tb_entrega_radarsat2 de acordo com o nu_entrega
DELETE_TB_ENTREGA_RADARSAT2 = "DELETE FROM funai.tb_entrega_radarsat2 WHERE nu_entrega = %s"
