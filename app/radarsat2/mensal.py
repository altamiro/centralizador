# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>
import psycopg2
import psycopg2.extras
import os

from unipath import Path
from osgeo import ogr
from app import app

from .sql import *
from app.settings.utils import buscar_arquivo

db2 = psycopg2.connect(app.config["PSYCOPG2_DATABASE_URI"])  # psycopg2
conn = db2.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)


# funcao que busca o numero da entrega de acordo com a data
def _nu_entrega_por_dt_entrega(dt_entrega):
    # busca o numero da entrega para retornar
    conn.execute(QUERY_BUSCAR_ENTREGA_MENSAL_POR_DATA, [dt_entrega])
    return conn.fetchone().nu_entrega


# funcao que busca o id da entrega de acordo com a data
def _entrega_id_por_dt_entrega(dt_entrega):
    # busca o id da entrega para retornar
    conn.execute(QUERY_BUSCAR_ENTREGA_MENSAL_POR_DATA, [dt_entrega])
    rs = conn.fetchone()
    return rs.id


# funcao que criar uma nova entrega_radarsat2
def _criar_entrega_radarsat2(dt_entrega, nu_entrega=None):
    conn.execute("BEGIN")
    try:

        if nu_entrega is None:
            # executa a query para pegar o numero da proxima entega
            conn.execute(QUERY_PROXIMA_ENTREGA_MENSAL)
            nu_entrega = conn.fetchone().nu_entrega

        # criar um novo registro na tabela tb_entrega_mensal_radarsat2
        conn.execute(INSERT_ENTREGA_MENSAL, [nu_entrega, dt_entrega])
        conn.execute("COMMIT")

        return _entrega_id_por_dt_entrega(dt_entrega)
    except (Exception, ValueError, psycopg2.DataError) as e:
        conn.execute("ROLLBACK")
        app.logger.error("__error_criar_entrega_radarsat2__")
        app.logger.exception("error: %s", e)
        return e


# funcao que busca uma entrega e caso nao exista cria uma nova
def _buscar_entrega_por_dt_entrega(dt_entrega, nu_entrega=None):
    conn.execute(QUERY_MAX_ENTREGA_MENSAL_POR_DATA, [dt_entrega])
    rs = conn.fetchone()

    if rs.total > 0:
        return rs.id
    else:
        return _criar_entrega_radarsat2(dt_entrega, nu_entrega)


# funcao que atualiza a entrega_radarsat2
def _atualizar_entrega_radarsat2(dt_entrega):
    conn.execute("BEGIN")
    try:
        conn.execute(UPDATE_ENTREGA_MENSAL, [dt_entrega])
        conn.execute("COMMIT")
    except (Exception, ValueError, psycopg2.DataError) as e:
        conn.execute("ROLLBACK")
        app.logger.error("__error_atualizar_entrega_radarsat2__")
        app.logger.exception("error: %s", e)
        return e


# funcao que cria uma nova classe de acordo com a descricao informada
def _criar_classe(no_classe_mda):
    conn.execute("BEGIN")
    try:
        # criar um novo registro na tabela         tb_classe_radarsat2
        conn.execute(INSERT_CLASSE_MDA, [no_classe_mda])
        conn.execute("COMMIT")

        return _classe_id_por_no_classe_mda(no_classe_mda)

    except (Exception, ValueError, psycopg2.DataError) as e:
        conn.execute("ROLLBACK")
        app.logger.error("__error_criar_classe__")
        app.logger.exception("error: %s", e)
        return e


# funcao que retorna o id da classe de acordo com nome informado
def _classe_id_por_no_classe_mda(no_classe_mda):
    conn.execute(QUERY_CLASSE, [no_classe_mda])
    return conn.fetchone().id


# funcao que busca uma entrega e caso nao exista cria uma nova
def _buscar_classe_por_no_classe_mda(no_classe_mda):
    conn.execute(QUERY_TOTAL_CLASSE_POR_NO_CLASSE_MDA, [no_classe_mda])
    total = conn.fetchone().total

    if total > 0:
        return _classe_id_por_no_classe_mda(no_classe_mda)
    else:
        return _criar_classe(no_classe_mda)


def _buscar_intersecao_id_por_entrega_id_e_nu_frame(entrega_id, nu_frame):
    # retorna o id da intersecao de acordo com a entrega_id e nu_frame
    conn.execute(QUERY_INTERSECAO_POR_ENTREGA_NU_FRAME, [entrega_id, nu_frame])
    return conn.fetchone().id


def _buscar_alerta_id_por_nu_feicao_mda_nu_frame_nu_entrega(nu_feicao_mda, nu_frame, nu_entrega):
    # retorna o id do alerta de acordo com a nu_feicao_mda, nu_frame e nu_entrega
    conn.execute(QUERY_ALERTA_POR_FEICAO_MDA_ENTREGA_FRAME, [nu_feicao_mda, nu_frame, nu_entrega])
    return conn.fetchone().id


def _atualizar_visoes_materializada():
    app.logger.info("__inicio_atualizando_visao_materializada__")
    # executa o REFRESH nas visoes
    conn.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY funai.vwm_painel_radarsat2_belo_monte_frame")
    conn.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY funai.vwm_painel_radarsat2_belo_monte_municipio")
    conn.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY funai.vwm_painel_radarsat2_belo_monte_terra_indigena")
    conn.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY funai.vwm_painel_radarsat2_belo_monte_ti_municipio")
    conn.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY aplicacao.vwm_tabela_informacao_atualizacao")
    app.logger.info("__fim_atualizando_visao_materializada__")
    app.logger.info("__inicio_vacumm_full_visao_materializada__")
    # executa o VACUUM FULL nas visoes
    conn.execute("VACUUM FULL funai.vwm_painel_radarsat2_belo_monte_frame")
    conn.execute("VACUUM FULL funai.vwm_painel_radarsat2_belo_monte_municipio")
    conn.execute("VACUUM FULL funai.vwm_painel_radarsat2_belo_monte_terra_indigena")
    conn.execute("VACUUM FULL funai.vwm_painel_radarsat2_belo_monte_ti_municipio")
    conn.execute("VACUUM FULL aplicacao.vwm_tabela_informacao_atualizacao")
    app.logger.info("__fim_vacumm_full_visao_materializada__")
    app.logger.info("__inicio_vacumm_analyze_visao_materializada__")
    # executa o VACUUM ANALYZE nas visoes
    conn.execute("VACUUM ANALYZE funai.vwm_painel_radarsat2_belo_monte_frame")
    conn.execute("VACUUM ANALYZE funai.vwm_painel_radarsat2_belo_monte_municipio")
    conn.execute("VACUUM ANALYZE funai.vwm_painel_radarsat2_belo_monte_terra_indigena")
    conn.execute("VACUUM ANALYZE funai.vwm_painel_radarsat2_belo_monte_ti_municipio")
    conn.execute("VACUUM ANALYZE aplicacao.vwm_tabela_informacao_atualizacao")
    app.logger.info("__fim_vacumm_analyze_visao_materializada__")


def _deletar_dados_da_entrega(nu_entrega):
    conn.execute("BEGIN")
    try:

        # apaga os registros das tabelas abaixo de acordo com nu_entrega
        conn.execute(DELETE_IMG_ALERTA_DANO_MAIOR_RADARSAT2_A, [nu_entrega])
        conn.execute(DELETE_IMG_ALERTA_RADARSAT2_A, [nu_entrega])
        conn.execute(DELETE_IMG_INTERSECAO_RADARSAT2_A, [nu_entrega])
        conn.execute(DELETE_TB_ENTREGA_RADARSAT2, [nu_entrega])
        conn.execute("COMMIT")

        return True
    except (Exception, ValueError, psycopg2.DataError) as e:
        conn.execute("ROLLBACK")
        app.logger.error("__error_deletar_dados_da_entrega__")
        app.logger.exception("error: %s", e)
        return e


def processar_dados_mensal(dt_entrega, directory, nu_entrega=None):
    json = {
        "message": "Entrega Mensal do dia %s/%s/%s, foi efetuado carga com sucesso." % (
            dt_entrega.split("-")[2], dt_entrega.split("-")[1], dt_entrega.split("-")[0]),
        "status": 1,
        "error": ""
    }

    if nu_entrega is not None:
        _deletar_dados_da_entrega(nu_entrega)

    conn.execute(QUERY_TOTAL_ENTREGA_MENSAL_POR_DATA, [dt_entrega])
    total = conn.fetchone().total

    if total > 0:
        json["message"] = "Já existe carga da entrega mensal do dia %s/%s/%s." % (
            dt_entrega.split("-")[2], dt_entrega.split("-")[1], dt_entrega.split("-")[0])
        json["status"] = 0
    else:
        app.logger.info("__iniciando_processamento_dos_dados_mensal__")
        booTransacao = True
        booIntersecaoTransacao = True
        booForestDamageTransacao = True
        booArquivoIntersection = False
        booArquivoForestDamage = False
        booArquivoForestDamageHighest = False

        for arquivo in sorted(os.listdir(directory)):
            if arquivo.endswith(".shp") and arquivo.lower().find("frame_intersection") > 0:
                booArquivoIntersection = True
            elif arquivo.endswith(".shp") and arquivo.lower().find("forestdamage") > 0 and arquivo.lower().find(
                    "highest_damage") == -1:
                booArquivoForestDamage = True
            elif arquivo.endswith(".shp") and arquivo.lower().find("forestdamage") > 0 and arquivo.lower().find(
                    "highest_damage") > 0:
                booArquivoForestDamageHighest = True

        if booArquivoIntersection and booArquivoForestDamage:

            # pega o id da entrega mensal
            tb_entrega_radarsat2_id = _buscar_entrega_por_dt_entrega(dt_entrega, nu_entrega)

            app.logger.info("__inicio_processamento_inserir_frame_intersecao__")
            for arquivo in sorted(os.listdir(directory)):
                if arquivo.endswith(".shp") and arquivo.lower().find("frame_intersection") > 0:
                    # pega o arquivo com caminho completo
                    s_caminho_arquivo = os.path.join(directory, arquivo)

                    # pega o numero do frame direto do arquivo
                    s_nu_frame = int(arquivo.lower().split("_")[1][5:])

                    # quebra em array a string do arquivo
                    _arr_arquivo = arquivo.split('_')

                    # Data da Imagem Anterior
                    s_dt_t_zero = "%s-%s-%s" % (_arr_arquivo[2][:4], _arr_arquivo[2][4:6], _arr_arquivo[2][6:8])

                    # Data da Imagem Atual
                    s_dt_t_um = "%s-%s-%s" % (_arr_arquivo[3][:4], _arr_arquivo[3][4:6], _arr_arquivo[3][6:8])

                    conn.execute("BEGIN")
                    app.logger.info("(BEGIN - Intersacao) Arquivo: %s", arquivo)

                    shapefile = ogr.Open(s_caminho_arquivo)
                    layer = shapefile.GetLayer()

                    union_layer = ogr.Geometry(ogr.wkbPolygon)

                    for feature in layer:
                        geom = feature.GetGeometryRef()
                        union_layer = union_layer.Union(geom)

                    # pega a geometria
                    s_geom = union_layer.ExportToWkt()

                    try:

                        conn.execute(INSERT_INTERSECAO_RADARSAT2,
                                     (tb_entrega_radarsat2_id, s_nu_frame, s_dt_t_zero, s_dt_t_um, s_geom))

                        app.logger.info("INSERT: %s", s_caminho_arquivo)

                    except (Exception, ValueError, psycopg2.DataError) as e:
                        conn.execute("ROLLBACK")
                        app.logger.error("ROLLBACK")
                        app.logger.error("__error_insert_img_intersecao_radarsat2_a__")
                        app.logger.exception("error: %s", e)
                        conn.execute(DELETE_ENTREGA_MENSAL, [dt_entrega])
                        app.logger.info("DELETE ENTREGA MENSAL")
                        json["error"] = e
                        booTransacao = False
                        booIntersecaoTransacao = False

                    if booTransacao:
                        conn.execute("COMMIT")
                        app.logger.info("(COMMIT - Intersacao) Arquivo: %s", arquivo)

            app.logger.info("__fim_processamento_inserir_frame_intersecao__")

            if booIntersecaoTransacao:
                app.logger.info("__inicio_processamento_inserir_mensal_alerta_radarsat2_a__")
                for arquivo in sorted(os.listdir(directory)):
                    if arquivo.endswith(".shp") and arquivo.lower().find("forestdamage") > 0 and arquivo.lower().find(
                            "highest_damage") == -1:
                        # pega o arquivo com caminho completo
                        s_caminho_arquivo = os.path.join(directory, arquivo)

                        # quebra em array a string do arquivo
                        _arr_arquivo = arquivo.split('_')

                        conn.execute("BEGIN")
                        app.logger.info("(BEGIN - Alerta) Arquivo: %s", arquivo)

                        shapefile = ogr.Open(s_caminho_arquivo)
                        layer = shapefile.GetLayer(0)

                        for i in range(layer.GetFeatureCount()):
                            feature = layer.GetFeature(i)
                            s_geom = feature.GetGeometryRef().ExportToWkt()

                            # pega o numero do frame no arquivo
                            s_nu_frame = int(feature.GetField('frame'))

                            # Data da Imagem Anterior
                            _dt_t_zero = feature.GetField('date_T0')
                            s_dt_t_zero = "%s-%s-%s" % (_dt_t_zero[:4], _dt_t_zero[4:6], _dt_t_zero[6:8])

                            # Data da Imagem Atual
                            _dt_t_um = feature.GetField('date_T1')
                            s_dt_t_um = "%s-%s-%s" % (_dt_t_um[:4], _dt_t_um[4:6], _dt_t_um[6:8])

                            # pega o id da intersecao de acordo com a entrega e o numero do frame
                            s_img_intersecao_radarsat2_a_id = _buscar_intersecao_id_por_entrega_id_e_nu_frame(
                                _entrega_id_por_dt_entrega(dt_entrega), s_nu_frame)

                            s_nu_feicao_mda = feature.GetField('FID')
                            s_nu_intensidade_dano = feature.GetField('Damage_Wgt')
                            s_tb_classe_radarsat2_id = _buscar_classe_por_no_classe_mda(feature.GetField('class'))
                            s_dt_deteccao = None
                            s_nu_votes = None

                            if feature.GetFieldIndex('img_dt_det') > 0:
                                _dt_deteccao = feature.GetField('img_dt_det')
                                s_dt_deteccao = "%s-%s-%s" % (_dt_deteccao[:4], _dt_deteccao[4:6], _dt_deteccao[6:8])

                            if feature.GetFieldIndex('Votes') > 0:
                                s_nu_votes = feature.GetField('Votes')

                            try:

                                conn.execute(INSERT_ALERTA_MENSAL_RADARSAT2,
                                             [s_img_intersecao_radarsat2_a_id, s_tb_classe_radarsat2_id,
                                              s_nu_feicao_mda,
                                              s_nu_votes, s_nu_intensidade_dano, s_dt_t_zero, s_dt_t_um, s_dt_deteccao,
                                              s_geom])

                                app.logger.info("INSERT: %s", s_caminho_arquivo)

                            except (Exception, ValueError, psycopg2.DataError) as e:
                                conn.execute("ROLLBACK")
                                app.logger.error("ROLLBACK")
                                app.logger.error("__error_insert_img_alerta_radarsat2_a__")
                                app.logger.exception("error: %s", e)
                                conn.execute(DELETE_INTERSECAO_RADARSAT2, [tb_entrega_radarsat2_id])
                                app.logger.info("DELETE INTERSECAO")
                                conn.execute(DELETE_ENTREGA_MENSAL, [dt_entrega])
                                app.logger.info("DELETE ENTREGA MENSAL")
                                json["error"] = e
                                booTransacao = False
                                booForestDamageTransacao = False

                        if booTransacao:
                            conn.execute("COMMIT")
                            app.logger.info("(COMMIT - Alerta) Arquivo: %s", arquivo)

                app.logger.info("__fim_processamento_inserir_mensal_alerta_radarsat2_a__")

            if booArquivoForestDamageHighest and booForestDamageTransacao:
                app.logger.info("__inicio_processamento_inserir_mensal_alerta_dano_maior_radarsat2_a__")
                for arquivo in sorted(os.listdir(directory)):
                    if arquivo.endswith(".shp") and arquivo.lower().find("forestdamage") > 0 and arquivo.lower().find(
                            "highest_damage") > 0:
                        # pega o arquivo com caminho completo
                        s_caminho_arquivo = os.path.join(directory, arquivo)

                        # quebra em array a string do arquivo
                        _arr_arquivo = arquivo.split('_')

                        # pega o numero do frame no arquivo
                        s_nu_frame = int(_arr_arquivo[1][5:])

                        conn.execute("BEGIN")
                        app.logger.info("(BEGIN - Alerta Dano Maior) Arquivo: %s", arquivo)

                        shapefile = ogr.Open(s_caminho_arquivo)
                        layer = shapefile.GetLayer(0)

                        for i in range(layer.GetFeatureCount()):
                            feature = layer.GetFeature(i)
                            s_geom = feature.GetGeometryRef().ExportToWkt()
                            nu_feicao_mda_original = feature.GetField('ORIG_FID')
                            s_img_alerta_radarsat2_a_id = _buscar_alerta_id_por_nu_feicao_mda_nu_frame_nu_entrega(
                                nu_feicao_mda_original, s_nu_frame, _entrega_id_por_dt_entrega(dt_entrega))

                            try:

                                conn.execute(INSERT_ALERTA_MENSAL_RADARSAT2_DANO_MAIOR,
                                             [s_img_alerta_radarsat2_a_id, nu_feicao_mda_original, s_geom])

                                app.logger.info("INSERT: %s", s_caminho_arquivo)

                            except (Exception, ValueError, psycopg2.DataError) as e:
                                conn.execute("ROLLBACK")
                                app.logger.error("ROLLBACK")
                                app.logger.error("__error_insert_img_alerta_dano_maior_radarsat2_a__")
                                app.logger.exception("error: %s", e)
                                json["error"] = e
                                booTransacao = False

                        if booTransacao:
                            conn.execute("COMMIT")
                            app.logger.info("(COMMIT - Alerta Dano Maior) Arquivo: %s", arquivo)

                app.logger.info("__fim_processamento_inserir_mensal_alerta_dano_maior_radarsat2_a__")

            if booTransacao:
                if os.path.exists(directory):
                    Path(directory).rmtree()
                _atualizar_entrega_radarsat2(dt_entrega)
                _atualizar_visoes_materializada()
            else:
                json["message"] = "Ocorreu um erro efetuar carga dos dados trimestral. Favor verificar!"
                json["status"] = 0

        else:
            json[
                "message"] = "Não foi possível ler os arquivos necessários! Favor verificar se existe os arquivos 'frame_intersection e forestDamage'."
            json["status"] = 0

    return json
