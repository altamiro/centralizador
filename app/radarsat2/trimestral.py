# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>
import psycopg2
import psycopg2.extras
import os

from unipath import Path
from osgeo import ogr
from app import app

from .sql import *

db2 = psycopg2.connect(app.config["PSYCOPG2_DATABASE_URI"])  # psycopg2
conn = db2.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)


# funcao que busca o numero da entrega de acordo com a data
def _nu_entrega_por_dt_entrega(dt_entrega):
    # busca o numero da entrega para retornar
    conn.execute(QUERY_BUSCAR_ENTREGA_POR_DATA, [dt_entrega])
    return conn.fetchone().nu_entrega


# funcao que busca o id da entrega de acordo com a data
def _entrega_id_por_dt_entrega(dt_entrega):
    # busca o id da entrega para retornar
    conn.execute(QUERY_BUSCAR_ENTREGA_POR_DATA, [dt_entrega])
    return conn.fetchone().id


# funcao que criar uma nova entrega_radarsat2
def _criar_entrega_radarsat2(dt_entrega):
    conn.execute("BEGIN")
    try:
        # executa a query para pegar o numero da proxima entega
        conn.execute(QUERY_PROXIMA_ENTREGA)
        nu_entrega = conn.fetchone().nu_entrega

        # criar um novo registro na tabela tb_entrega_trimestral_radarsat2
        conn.execute(INSERT_ENTREGA_TRIMESTRAL, [nu_entrega, dt_entrega])
        conn.execute("COMMIT")

        return _entrega_id_por_dt_entrega(dt_entrega)
    except (Exception, ValueError, psycopg2.DataError) as e:
        conn.execute("ROLLBACK")
        app.logger.error("__error_criar_entrega_radarsat2__")
        app.logger.exception("error: %s", e)
        return e


# funcao que busca uma entrega e caso nao exista cria uma nova
def _buscar_entrega_por_dt_entrega(dt_entrega):
    conn.execute(QUERY_MAX_ENTREGA_POR_DATA, [dt_entrega])
    rs = conn.fetchone()

    if rs.total > 0:
        return rs.id
    else:
        return _criar_entrega_radarsat2(dt_entrega)


# funcao que atualiza a entrega_radarsat2
def _atualizar_entrega_radarsat2(dt_entrega):
    conn.execute("BEGIN")
    try:
        conn.execute(UPDATE_ENTREGA_TRIMESTRAL, [dt_entrega])
        conn.execute("COMMIT")
    except (Exception, ValueError, psycopg2.DataError) as e:
        conn.execute("ROLLBACK")
        app.logger.error("__error_atualizar_entrega_radarsat2__")
        app.logger.exception("error: %s", e)
        return e


# funcao que cria uma nova classe de acordo com a descricao informada
def _criar_classe(no_classe_mda):
    conn.execute("BEGIN")
    try:
        # criar um novo registro na tabela         tb_classe_radarsat2
        conn.execute(INSERT_CLASSE_MDA, [no_classe_mda])
        conn.execute("COMMIT")

        return _classe_id_por_no_classe_mda(no_classe_mda)

    except (Exception, ValueError, psycopg2.DataError) as e:
        conn.execute("ROLLBACK")
        app.logger.error("__error_criar_classe__")
        app.logger.exception("error: %s", e)
        return e


# funcao que retorna o id da classe de acordo com nome informado
def _classe_id_por_no_classe_mda(no_classe_mda):
    conn.execute(QUERY_CLASSE, [no_classe_mda])
    return conn.fetchone().id


# funcao que busca uma entrega e caso nao exista cria uma nova
def _buscar_classe_por_no_classe_mda(no_classe_mda):
    conn.execute(QUERY_TOTAL_CLASSE_POR_NO_CLASSE_MDA, [no_classe_mda])
    total = conn.fetchone().total

    if total > 0:
        return _classe_id_por_no_classe_mda(no_classe_mda)
    else:
        return _criar_classe(no_classe_mda)


def _atualizar_visoes_materializada():
    app.logger.info("__inicio_atualizando_visao_materializada__")
    conn.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY aplicacao.vwm_tabela_informacao_atualizacao")
    # executa o VACUUM FULL nas visoes
    conn.execute("VACUUM FULL aplicacao.vwm_tabela_informacao_atualizacao")
    # executa o VACUUM ANALYZE nas visoes
    conn.execute("VACUUM ANALYZE aplicacao.vwm_tabela_informacao_atualizacao")
    app.logger.info("__fim_atualizando_visao_materializada__")


def processar_dados_trimestral(dt_entrega, directory):
    json = {
        "message": "Entrega Trimestral do dia %s/%s/%s, foi efetuado carga com sucesso." % (
            dt_entrega.split("-")[2], dt_entrega.split("-")[1], dt_entrega.split("-")[0]),
        "status": 1,
        "error": ""
    }

    conn.execute(QUERY_TOTAL_ENTREGA_POR_DATA, [dt_entrega])
    total = conn.fetchone().total

    if total > 0:
        json["message"] = "Já existe carga da entrega trimestral do dia %s/%s/%s." % (
            dt_entrega.split("-")[2], dt_entrega.split("-")[1], dt_entrega.split("-")[0])
        json["status"] = 0
    else:
        app.logger.info("__iniciando_processamento_dos_dados_trimestral__")
        booTransacao = True
        booAguaTransacao = True
        booArquivoWater = False
        booArquivoForestDamage = False

        for arquivo in sorted(os.listdir(directory)):
            if arquivo.endswith(".shp") and arquivo.lower().find("water") > 0:
                booArquivoWater = True
            elif arquivo.endswith(".shp") and arquivo.lower().find("forestdamage") > 0:
                booArquivoForestDamage = True

        if booArquivoWater and booArquivoForestDamage:

            # pega o id da entrega trimestral
            tb_entrega_trimestral_radarsat2_id = _buscar_entrega_por_dt_entrega(dt_entrega)

            app.logger.info("__inicio_processamento_inserir_trimestral_agua_radarsat2__")
            for arquivo in sorted(os.listdir(directory)):
                if arquivo.endswith(".shp") and arquivo.lower().find("water") > 0:
                    # pega o arquivo com caminho completo
                    s_caminho_arquivo = os.path.join(directory, arquivo)
                    # pega o numero do frame direto do arquivo
                    nu_frame = int(arquivo.lower().split("_")[1].replace("frame", ""))

                    conn.execute("BEGIN")
                    app.logger.info("(BEGIN - Agua) Arquivo: %s", arquivo)

                    shapefile = ogr.Open(s_caminho_arquivo)
                    layer = shapefile.GetLayer()

                    for feature in layer:
                        geom = feature.GetGeometryRef().ExportToWkt()

                        try:

                            conn.execute(INSERT_AGUA_TRIMESTRAL_RADARSAT2,
                                         [tb_entrega_trimestral_radarsat2_id, nu_frame, geom])

                            app.logger.info("INSERT: %s", s_caminho_arquivo)

                        except (Exception, ValueError, psycopg2.DataError) as e:
                            conn.execute("ROLLBACK")
                            app.logger.error("ROLLBACK")
                            app.logger.error("__error_insert_img_agua_trimestral_radarsat2_a__")
                            app.logger.exception("error: %s", e)
                            conn.execute(DELETE_ENTREGA_TRIMESTRAL, [dt_entrega])
                            app.logger.info("DELETE ENTREGA TRIMESTRAL")
                            json["error"] = e
                            booTransacao = False
                            booAguaTransacao = False

                    if booTransacao:
                        conn.execute("COMMIT")
                        app.logger.info("(COMMIT - Agua) Arquivo: %s", arquivo)

            app.logger.info("__fim_processamento_inserir_trimestral_agua_radarsat2__")

            if booAguaTransacao:
                app.logger.info("__inicio_processamento_inserir_trimestral_alerta_radarsat2_a__")
                for arquivo in sorted(os.listdir(directory)):
                    if arquivo.endswith(".shp") and arquivo.lower().find("forestdamage") > 0:
                        # pega o arquivo com caminho completo
                        s_caminho_arquivo = os.path.join(directory, arquivo)

                        # quebra em array a string do arquivo
                        _arr_arquivo = arquivo.split('_')

                        conn.execute("BEGIN")
                        app.logger.info("(BEGIN - Alerta) Arquivo: %s", arquivo)

                        shapefile = ogr.Open(s_caminho_arquivo)
                        layer = shapefile.GetLayer(0)

                        for i in range(layer.GetFeatureCount()):
                            feature = layer.GetFeature(i)
                            s_geom = feature.GetGeometryRef().ExportToWkt()
                            s_nu_feicao_mda = feature.GetField('FID')
                            s_nu_intensidade_dano = feature.GetField('Damage_Wgt')
                            s_tb_classe_radarsat2_id = _buscar_classe_por_no_classe_mda(feature.GetField('class'))
                            s_dt_deteccao = None
                            s_nu_votes = None

                            # pega o numero do frame no arquivo
                            s_nu_frame = int(feature.GetField('frame'))

                            # Data da Imagem Anterior
                            _dt_t_zero = feature.GetField('date_T0')
                            s_dt_t_zero = "%s-%s-%s" % (_dt_t_zero[:4], _dt_t_zero[4:6], _dt_t_zero[6:8])

                            # Data da Imagem Atual
                            _dt_t_um = feature.GetField('date_T1')
                            s_dt_t_um = "%s-%s-%s" % (_dt_t_um[:4], _dt_t_um[4:6], _dt_t_um[6:8])

                            if feature.GetFieldIndex('img_dt_det') > 0:
                                _dt_deteccao = feature.GetField('img_dt_det')
                                s_dt_deteccao = "%s-%s-%s" % (_dt_deteccao[:4], _dt_deteccao[4:6], _dt_deteccao[6:8])

                            if feature.GetFieldIndex('Votes') > 0:
                                s_nu_votes = feature.GetField('Votes')

                            try:

                                conn.execute(INSERT_ALERTA_TRIMESTRAL_RADARSAT2,
                                             [tb_entrega_trimestral_radarsat2_id, s_tb_classe_radarsat2_id, s_nu_frame,
                                              s_nu_feicao_mda, s_nu_votes, s_nu_intensidade_dano, s_dt_t_zero,
                                              s_dt_t_um,
                                              s_dt_deteccao, s_geom])

                                app.logger.info("INSERT: %s", s_caminho_arquivo)

                            except (Exception, ValueError, psycopg2.DataError) as e:
                                conn.execute("ROLLBACK")
                                app.logger.error("ROLLBACK")
                                app.logger.error("__error_insert_img_agua_trimestral_radarsat2_a__")
                                app.logger.exception("error: %s", e)
                                conn.execute(DELETE_ENTREGA_TRIMESTRAL, [dt_entrega])
                                app.logger.info("DELETE ENTREGA TRIMESTRAL")
                                json["error"] = e
                                booTransacao = False

                        if booTransacao:
                            conn.execute("COMMIT")
                            app.logger.info("(COMMIT - Alerta) Arquivo: %s", arquivo)

                app.logger.info("__fim_processamento_inserir_trimestral_alerta_radarsat2_a__")

            if booTransacao:
                _atualizar_entrega_radarsat2(dt_entrega)
                _atualizar_visoes_materializada()
                if os.path.exists(directory):
                    Path(directory).rmtree()
            else:
                json["message"] = "Ocorreu um erro efetuar carga dos dados trimestral. Favor verificar!"
                json["status"] = 0

        else:
            json[
                "message"] = "Não foi possível ler os arquivos necessários! Favor verificar se existe os arquivos 'water e forestDamage'."
            json["status"] = 0

    return json
