# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

import json

from app import app, db

from io import BytesIO
from reportlab.lib.enums import TA_LEFT, TA_CENTER, TA_JUSTIFY, TA_RIGHT
from reportlab.lib.units import inch, cm, mm
from reportlab.lib.colors import HexColor
from reportlab.lib.pagesizes import portrait, landscape, A4
from reportlab.pdfgen import canvas
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.platypus import Table, TableStyle, Paragraph
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle

bottom_margin = cm
left_margin = cm


def coord(x, y, h, unit=1):
    x, y = x * unit, h - y * unit
    return x, y


def getPageWidth(canv):
    (w, h) = canv._pagesize
    return w


def getPageHeight(canv):
    (w, h) = canv._pagesize
    return h


def getPageTop(canv):
    (w, h) = canv._pagesize
    return h - cm


def getPageRight(canv):
    (w, h) = canv._pagesize
    return w - inch


def frameCabecalhoRodape(canv):
    w = getPageWidth(canv)
    h = getPageHeight(canv)

    width = int(w - 40)
    height = 36
    if w > 800:
        width = int(w - 40)
        height = 50

    top_margin = getPageTop(canv)
    right_margin = getPageRight(canv)

    # definicao do cabecalho
    canv.drawImage(app.config['IMG_FOLDER'] + "cabecalho_aviso.png", left_margin, top_margin - 30, width=width,
                   height=height)

    # definicao do rodape
    canv.drawImage(app.config['IMG_FOLDER'] + "rodape_aviso.png", left_margin, bottom_margin - 20, width=width,
                   height=height - 5)
    canv.setFont('Times-BoldItalic', 11)
    canv.drawCentredString(0.5 * w, 0.5 * cm, "Projeto CMR - #%d" % canv.getPageNumber())


def aviso_diario_pdf(entregaData, templateData):
    styles = getSampleStyleSheet()

    # font = TTFont('Arial', '/usr/share/fonts/msttcore/arial.ttf')
    font = TTFont('Times New Roman', '/usr/share/fonts/msttcore/times.ttf')
    registerFont(font)

    output = BytesIO()

    canv = canvas.Canvas(output, pagesize=portrait(A4), invariant=1, initialFontName='Times New Roman',
                         initialFontSize=11)

    canv.setLineWidth(.2)
    frameCabecalhoRodape(canv)

    num_top_parag = 100

    p1 = Paragraph('''<b>%s</b>''' % entregaData.no_ciclo_descricao, styles["BodyText"])
    p2 = Paragraph('''%s <b>%s</b>''' % (templateData['aviso_de_entrega_diario_ref'], entregaData.no_referencia),
                   styles["BodyText"])
    p3 = Paragraph(
        '''%s <b>%s (%s)</b>''' % (templateData['orbita_em_analise'], entregaData.nu_orbita, entregaData.st_situacao),
        styles["BodyText"])
    p4 = Paragraph(
        '''%s <b>%s</b>''' % (templateData['dado_disponibilizado_no_banco_de_dados'], entregaData.dt_disponibilizado),
        styles["BodyText"])

    dataTabela1 = [
        [p1],
        [p2],
        [p3],
        [p4]
    ]

    table1 = Table(dataTabela1, getPageRight(canv))
    table1.setStyle(TableStyle([
        ('ALIGN', (1, 1), (-2, -2), 'RIGHT'),
        ('BACKGROUND', (0, 0), (-1, -0), HexColor('#C0C0C0')),
        ('INNERGRID', (0, 0), (-1, -1), 0.10, HexColor('#DDDDDD')),
        ('BOX', (0, 0), (-1, -1), 0.10, HexColor('#DDDDDD')),
    ]))
    table1.wrapOn(canv, getPageRight(canv), getPageTop(canv))
    table1.drawOn(canv, left_margin, getPageTop(canv) - num_top_parag)

    p5 = Paragraph(
        '''<font size=18><b><u>%s</u></b></font>''' % templateData['aviso_de_entrega_diario_landsat_8'],
        ParagraphStyle(name='p5', parent=styles['BodyText'], alignment=TA_CENTER))
    p5.wrapOn(canv, getPageRight(canv), getPageTop(canv) - 50)
    p5.drawOn(canv, left_margin, getPageTop(canv) - (num_top_parag + 25))

    p6 = Paragraph(
        '''<b>%s</b>''' % templateData['1_resultado_sumario_estatistico_diario_landsat_8'],
        ParagraphStyle(name='p6', parent=styles['BodyText'], alignment=TA_LEFT))
    p6.wrapOn(canv, getPageRight(canv), getPageTop(canv) - 50)
    p6.drawOn(canv, left_margin, getPageTop(canv) - (num_top_parag + 60))

    p7 = Paragraph(
        '''<b>&bull; %s %s</b>''' % (templateData['1_terra_indigena_na_aml'], entregaData.nu_orbita),
        ParagraphStyle(name='p7', parent=styles['BodyText'], alignment=TA_LEFT))
    p7.wrapOn(canv, getPageRight(canv), getPageTop(canv) - 50)
    p7.drawOn(canv, left_margin + 60, getPageTop(canv) - (num_top_parag + 85))

    p8 = Paragraph(
        '''<span>%s</span>''' % templateData['1_tabela_1_distribuicao_do_dado_diario_por_terra_indigena'],
        ParagraphStyle(name='p8', parent=styles['BodyText'], alignment=TA_CENTER))
    p8.wrapOn(canv, getPageRight(canv), getPageTop(canv) - 50)
    p8.drawOn(canv, left_margin, getPageTop(canv) - (num_top_parag + 110))

    pHeader1 = Paragraph('Código Funai',
                         ParagraphStyle(name='pHeader1', fontSize=8, fontName='Times-Bold', alignment=TA_CENTER))
    pHeader2 = Paragraph('Nome da Terra Indígena',
                         ParagraphStyle(name='pHeader2', fontSize=8, fontName='Times-Bold', alignment=TA_CENTER))
    pHeader3 = Paragraph('Data da Imagem',
                         ParagraphStyle(name='pHeader3', fontSize=8, fontName='Times-Bold', alignment=TA_CENTER))
    pHeader4 = Paragraph('CR (ha)',
                         ParagraphStyle(name='pHeader4', fontSize=8, fontName='Times-Bold', alignment=TA_CENTER))
    pHeader5 = Paragraph('DG (ha)',
                         ParagraphStyle(name='pHeader5', fontSize=8, fontName='Times-Bold', alignment=TA_CENTER))
    pHeader6 = Paragraph('DR (ha)',
                         ParagraphStyle(name='pHeader6', fontSize=8, fontName='Times-Bold', alignment=TA_CENTER))
    pHeader7 = Paragraph('FF (ha)',
                         ParagraphStyle(name='pHeader7', fontSize=8, fontName='Times-Bold', alignment=TA_CENTER))
    pHeader8 = Paragraph('Total (ha)',
                         ParagraphStyle(name='pHeader8', fontSize=8, fontName='Times-Bold', alignment=TA_CENTER))

    dataTerraIndigena = []
    dataTerraIndigena.append([pHeader1, pHeader2, pHeader3, pHeader4, pHeader5, pHeader6, pHeader7, pHeader8])

    app.logger.info(len(entregaData.js_terra_indigena))

    for r in range(len(entregaData.js_terra_indigena)):
        if r <= 28:
            dataTerraIndigena.append([
                Paragraph("""%s""" % entregaData.js_terra_indigena[r]['co_funai'],
                          ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman', alignment=TA_CENTER)),
                entregaData.js_terra_indigena[r]['no_ti'],
                Paragraph("""%s""" % entregaData.js_terra_indigena[r]['dt_t_um_formatada'],
                          ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman', alignment=TA_CENTER)),
                Paragraph("""%s""" % entregaData.js_terra_indigena[r]['cr_nu_area_ha'],
                          ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman', alignment=TA_RIGHT)),
                Paragraph("""%s""" % entregaData.js_terra_indigena[r]['dg_nu_area_ha'],
                          ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman', alignment=TA_RIGHT)),
                Paragraph("""%s""" % entregaData.js_terra_indigena[r]['dr_nu_area_ha'],
                          ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman', alignment=TA_RIGHT)),
                Paragraph("""%s""" % entregaData.js_terra_indigena[r]['ff_nu_area_ha'],
                          ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman', alignment=TA_RIGHT)),
                Paragraph("""%s""" % entregaData.js_terra_indigena[r]['total_nu_area_ha'],
                          ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman', alignment=TA_RIGHT))
            ])

    tableTerraIndigena = Table(dataTerraIndigena)
    tableTerraIndigena.setStyle(TableStyle([
        ('FONT', (0, 0), (-1, -1), 'Times New Roman'),
        ('FONTSIZE', (0, 0), (-1, -1), 9),
        ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
        ('INNERGRID', (0, 0), (-1, -1), 0.10, HexColor('#DDDDDD')),
        ('BOX', (0, 0), (-1, -1), 0.10, HexColor('#DDDDDD')),
    ]))
    tableTerraIndigena.wrapOn(canv, getPageRight(canv), getPageTop(canv) - 50)
    tableTerraIndigena.drawOn(canv, left_margin, *coord(18, 297, getPageHeight(canv), mm))

    if len(entregaData.js_terra_indigena) > 28:
        canv.showPage()
        canv.setPageSize(portrait(A4))
        frameCabecalhoRodape(canv)
        dataTerraIndigena = []
        dataTerraIndigena.append([pHeader1, pHeader2, pHeader3, pHeader4, pHeader5, pHeader6, pHeader7, pHeader8])
        for r in range(len(entregaData.js_terra_indigena)):
            if r >= 29:
                if entregaData.js_terra_indigena[r]['co_funai']:
                    dataTerraIndigena.append([
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['co_funai'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman',
                                                 alignment=TA_CENTER)),
                        entregaData.js_terra_indigena[r]['no_ti'],
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['dt_t_um_formatada'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman',
                                                 alignment=TA_CENTER)),
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['cr_nu_area_ha'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman',
                                                 alignment=TA_RIGHT)),
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['dg_nu_area_ha'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman',
                                                 alignment=TA_RIGHT)),
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['dr_nu_area_ha'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman',
                                                 alignment=TA_RIGHT)),
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['ff_nu_area_ha'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman',
                                                 alignment=TA_RIGHT)),
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['total_nu_area_ha'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times New Roman',
                                                 alignment=TA_RIGHT))
                    ])
                else:
                    dataTerraIndigena.append([
                        '',
                        '',
                        Paragraph("""Total""",
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times-Bold',
                                                 alignment=TA_RIGHT)),
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['cr_nu_area_ha'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times-Bold',
                                                 alignment=TA_RIGHT)),
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['dg_nu_area_ha'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times-Bold',
                                                 alignment=TA_RIGHT)),
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['dr_nu_area_ha'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times-Bold',
                                                 alignment=TA_RIGHT)),
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['ff_nu_area_ha'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times-Bold',
                                                 alignment=TA_RIGHT)),
                        Paragraph("""%s""" % entregaData.js_terra_indigena[r]['total_nu_area_ha'],
                                  ParagraphStyle(name='rows', fontSize=9, fontName='Times-Bold',
                                                 alignment=TA_RIGHT))
                    ])

        tableTerraIndigena = Table(dataTerraIndigena)
        tableTerraIndigena.setStyle(TableStyle([
            ('FONT', (0, 0), (-1, -1), 'Times New Roman'),
            ('FONTSIZE', (0, 0), (-1, -1), 9),
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
            ('INNERGRID', (0, 0), (-1, -1), 0.10, HexColor('#DDDDDD')),
            ('BOX', (0, 0), (-1, -1), 0.10, HexColor('#DDDDDD')),
        ]))
        tableTerraIndigena.wrapOn(canv, getPageRight(canv), getPageTop(canv))
        tableTerraIndigena.drawOn(canv, left_margin, getPageTop(canv) - (num_top_parag + 20))

    canv.showPage()
    canv.setPageSize(portrait(A4))
    frameCabecalhoRodape(canv)

    #
    # canv.setPageSize(landscape(A4))
    # frameCabecalhoRodape(canv)
    # canv.drawString(left_margin, 500, '-----')

    canv.save()

    pdf_out = output.getvalue()
    output.close()

    return pdf_out
