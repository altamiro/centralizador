# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

import rarfile
import zipfile
import fnmatch
import shutil
import os
import locale

from app import app, db
from app.models import VisaoEntregaAvisoDiario
from sqlalchemy.sql import select, func
from decimal import *


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


# funcao que descompactar arquivo zip que foi compactado com winrar
def descompactar_rar(arquivo, caminho):
    with rarfile.RarFile(arquivo) as file:
        try:
            file.extract(caminho, pwd=None)
        except RuntimeError:
            descompactar_rar(arquivo, caminho)


# funcao que descompactar arquivo zip
def descompactar_zip(arquivo, caminho):
    with zipfile.ZipFile(arquivo) as f:
        try:
            f.extractall(caminho, pwd=None)
        except RuntimeError:
            descompactar_zip(arquivo, caminho)


def descompactar(arquivo, caminho):
    if zipfile.is_zipfile(arquivo):
        descompactar_zip(arquivo, caminho)
    else:
        descompactar_rar(arquivo, caminho)


def buscar_arquivo(extensao, caminho):
    for path, dirlist, filelist in os.walk(caminho):
        for name in fnmatch.filter(filelist, extensao):
            yield os.path.join(path, name)


def mover_arquivos(caminho):
    for dbf in buscar_arquivo("*.dbf", caminho):
        shutil.move(dbf, caminho)

    for prj in buscar_arquivo("*.prj", caminho):
        shutil.move(prj, caminho)

    for shp in buscar_arquivo("*.shp", caminho):
        shutil.move(shp, caminho)

    for shx in buscar_arquivo("*.shx", caminho):
        shutil.move(shx, caminho)


def formatarValor(valor):
    locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
    valor = locale.currency(valor, grouping=True, symbol=None)
    return valor


def json_aviso_diario_terra_indigena(id):
    jsonTerraIndigena = []
    entregaAvisoDiario = VisaoEntregaAvisoDiario.query.get(id)

    SQL = "co_funai, " \
          "no_ti, " \
          "no_ciclo, " \
          "nu_orbita, " \
          "dt_t_um_formatada, " \
          "dt_t_um, " \
          "SUM(cr_nu_area_ha) AS cr_nu_area_ha, " \
          "SUM(dg_nu_area_ha) AS dg_nu_area_ha, " \
          "SUM(dr_nu_area_ha) AS dr_nu_area_ha, " \
          "SUM(ff_nu_area_ha) AS ff_nu_area_ha, " \
          "SUM(total_nu_area_ha) AS total_nu_area_ha " \
          "FROM " \
          "aviso.vw_monitora_aviso_diario_por_terra_indigena " \
          "WHERE " \
          "ciclo_id = %s " \
          "AND nu_orbita IN (%s) " \
          "GROUP BY ROLLUP ((co_funai,no_ti,no_ciclo,nu_orbita,dt_t_um_formatada,dt_t_um)) " \
          "ORDER BY no_ti, dt_t_um" % (entregaAvisoDiario.ciclo_id, entregaAvisoDiario.nu_orbita_aspas)

    s = select([SQL])

    dataTerraIndigena = db.engine.execute(s).fetchall()

    if len(dataTerraIndigena) > 0:
        for rs in dataTerraIndigena:
            jsonTerraIndigena.append({
                'co_funai': rs.co_funai,
                'no_ti': rs.no_ti,
                'no_ciclo': rs.no_ciclo,
                'nu_orbita': rs.nu_orbita,
                'dt_t_um_formatada': rs.dt_t_um_formatada,
                'cr_nu_area_ha': [formatarValor(float(Decimal(rs.cr_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.cr_nu_area_ha).to_eng_string()) == 0.00],
                'dg_nu_area_ha': [formatarValor(float(Decimal(rs.dg_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.dg_nu_area_ha).to_eng_string()) == 0.00],
                'dr_nu_area_ha': [formatarValor(float(Decimal(rs.dr_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.dr_nu_area_ha).to_eng_string()) == 0.00],
                'ff_nu_area_ha': [formatarValor(float(Decimal(rs.ff_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.ff_nu_area_ha).to_eng_string()) == 0.00],
                'total_nu_area_ha': [formatarValor(float(Decimal(rs.total_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.total_nu_area_ha).to_eng_string()) == 0.00]
            })

    return jsonTerraIndigena


def json_aviso_diario_coordenacao_regional(id):
    jsonCoordenacaoRegional = []
    entregaAvisoDiario = VisaoEntregaAvisoDiario.query.get(id)

    SQL = "ds_cr, " \
          "no_ciclo, " \
          "nu_orbita, " \
          "dt_t_um_formatada, " \
          "dt_t_um, " \
          "SUM(cr_nu_area_ha) AS cr_nu_area_ha, " \
          "SUM(dg_nu_area_ha) AS dg_nu_area_ha, " \
          "SUM(dr_nu_area_ha) AS dr_nu_area_ha, " \
          "SUM(ff_nu_area_ha) AS ff_nu_area_ha, " \
          "SUM(total_nu_area_ha) AS total_nu_area_ha " \
          "FROM " \
          "aviso.vw_monitora_aviso_diario_por_ti_coordenacao_regional " \
          "WHERE " \
          "ciclo_id = %s " \
          "AND nu_orbita IN (%s) " \
          "GROUP BY ROLLUP ((ds_cr,no_ciclo,nu_orbita,dt_t_um_formatada,dt_t_um)) " \
          "ORDER BY ds_cr, dt_t_um" % (entregaAvisoDiario.ciclo_id, entregaAvisoDiario.nu_orbita_aspas)

    s = select([SQL])

    dataCoordenacaoRegional = db.engine.execute(s).fetchall()

    if len(dataCoordenacaoRegional) > 0:
        for rs in dataCoordenacaoRegional:
            jsonCoordenacaoRegional.append({
                'ds_cr': rs.ds_cr,
                'no_ciclo': rs.no_ciclo,
                'nu_orbita': rs.nu_orbita,
                'dt_t_um_formatada': rs.dt_t_um_formatada,
                'cr_nu_area_ha': [formatarValor(float(Decimal(rs.cr_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.cr_nu_area_ha).to_eng_string()) == 0.00],
                'dg_nu_area_ha': [formatarValor(float(Decimal(rs.dg_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.dg_nu_area_ha).to_eng_string()) == 0.00],
                'dr_nu_area_ha': [formatarValor(float(Decimal(rs.dr_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.dr_nu_area_ha).to_eng_string()) == 0.00],
                'ff_nu_area_ha': [formatarValor(float(Decimal(rs.ff_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.ff_nu_area_ha).to_eng_string()) == 0.00],
                'total_nu_area_ha': [formatarValor(float(Decimal(rs.total_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.total_nu_area_ha).to_eng_string()) == 0.00]
            })

    return jsonCoordenacaoRegional


def json_aviso_diario_estado(id):
    jsonEstado = []
    entregaAvisoDiario = VisaoEntregaAvisoDiario.query.get(id)

    SQL = "no_uf, " \
          "sg_uf, " \
          "no_ciclo, " \
          "nu_orbita, " \
          "dt_t_um_formatada, " \
          "dt_t_um, " \
          "SUM(cr_nu_area_ha) AS cr_nu_area_ha, " \
          "SUM(dg_nu_area_ha) AS dg_nu_area_ha, " \
          "SUM(dr_nu_area_ha) AS dr_nu_area_ha, " \
          "SUM(ff_nu_area_ha) AS ff_nu_area_ha, " \
          "SUM(total_nu_area_ha) AS total_nu_area_ha " \
          "FROM " \
          "aviso.vw_monitora_aviso_diario_por_uf " \
          "WHERE " \
          "ciclo_id = %s " \
          "AND nu_orbita IN (%s) " \
          "GROUP BY ROLLUP ((no_uf,sg_uf,no_ciclo,nu_orbita,dt_t_um_formatada,dt_t_um)) " \
          "ORDER BY no_uf, dt_t_um" % (entregaAvisoDiario.ciclo_id, entregaAvisoDiario.nu_orbita_aspas)

    s = select([SQL])

    dataEstado = db.engine.execute(s).fetchall()

    if len(dataEstado) > 0:
        for rs in dataEstado:
            jsonEstado.append({
                'no_uf': rs.no_uf,
                'sg_uf': rs.sg_uf,
                'no_ciclo': rs.no_ciclo,
                'nu_orbita': rs.nu_orbita,
                'dt_t_um_formatada': rs.dt_t_um_formatada,
                'cr_nu_area_ha': [formatarValor(float(Decimal(rs.cr_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.cr_nu_area_ha).to_eng_string()) == 0.00],
                'dg_nu_area_ha': [formatarValor(float(Decimal(rs.dg_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.dg_nu_area_ha).to_eng_string()) == 0.00],
                'dr_nu_area_ha': [formatarValor(float(Decimal(rs.dr_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.dr_nu_area_ha).to_eng_string()) == 0.00],
                'ff_nu_area_ha': [formatarValor(float(Decimal(rs.ff_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.ff_nu_area_ha).to_eng_string()) == 0.00],
                'total_nu_area_ha': [formatarValor(float(Decimal(rs.total_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.total_nu_area_ha).to_eng_string()) == 0.00]
            })

    return jsonEstado


def json_aviso_diario_imagens(id):
    jsonImagens = []
    entregaAvisoDiario = VisaoEntregaAvisoDiario.query.get(id)

    SQL = "id, " \
          "ciclo_id, " \
          "no_imagem, " \
          "dt_imagem, " \
          "nu_nuvem, " \
          "nu_orbita, " \
          "nu_ponto, " \
          "ds_orbita_ponto " \
          "FROM " \
          "aplicacao.vw_monitoramento_consolidado_catalogo_landsat " \
          "WHERE " \
          "ciclo_id = %s " \
          "AND nu_orbita IN (%s) " \
          "ORDER BY nu_nuvem DESC" % (
              entregaAvisoDiario.ciclo_id, entregaAvisoDiario.nu_orbita_aspas)

    s = select([SQL])

    dataImagens = db.engine.execute(s).fetchall()

    if len(dataImagens) > 0:
        for rs in dataImagens:
            jsonImagens.append({
                'ciclo_id': rs.ciclo_id,
                'no_imagem': rs.no_imagem,
                'dt_imagem': rs.dt_imagem,
                'nu_nuvem': [formatarValor(float(Decimal(rs.nu_nuvem).to_eng_string())), '-'][
                    float(Decimal(rs.nu_nuvem).to_eng_string()) == 0.00],
                'nu_orbita': rs.nu_orbita,
                'nu_ponto': rs.nu_ponto,
                'ds_orbita_ponto': rs.ds_orbita_ponto
            })

    return jsonImagens


def json_mensal_radarsat2_por_frame(nu_entrega):
    jsonFrame = []

    SQL = "nu_entrega, " \
          "nu_frame, " \
          "dt_t_zero, " \
          "dt_t_um, " \
          "COALESCE(SUM(frame_nu_area_ha),0.00) AS frame_nu_area_ha, " \
          "COALESCE(SUM(corte_raso_nu_area_ha),0.00) AS cr_nu_area_ha, " \
          "COALESCE(SUM(corte_seletivo_nu_area_ha),0.00) AS cs_nu_area_ha, " \
          "COALESCE(SUM(nova_infraestrutura_nu_area_ha),0.00) AS ni_nu_area_ha, " \
          "COALESCE(SUM(nova_estrada_nu_area_ha),0.00) AS ne_nu_area_ha, " \
          "COALESCE(SUM(seletivo_natural_nu_area_ha),0.00) AS sn_nu_area_ha, " \
          "COALESCE(SUM(nu_area_total),0.00) AS nu_area_total " \
          "FROM " \
          "funai.vwm_painel_radarsat2_belo_monte_frame " \
          "WHERE " \
          "nu_entrega = %s " \
          "GROUP BY " \
          "ROLLUP((nu_entrega,nu_frame,dt_t_zero,dt_t_um)) " \
          "ORDER BY " \
          "nu_area_total DESC" % (nu_entrega)

    s = select([SQL])

    data = db.engine.execute(s).fetchall()

    if len(data) > 0:
        for rs in data:
            jsonFrame.append({
                'nu_entrega': rs.nu_entrega,
                'nu_frame': rs.nu_frame,
                'dt_t_zero': rs.dt_t_zero,
                'dt_t_um': rs.dt_t_um,
                'frame_nu_area_ha': [formatarValor(float(Decimal(rs.frame_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.frame_nu_area_ha).to_eng_string()) == 0.00],
                'cr_nu_area_ha': [formatarValor(float(Decimal(rs.cr_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.cr_nu_area_ha).to_eng_string()) == 0.00],
                'cs_nu_area_ha': [formatarValor(float(Decimal(rs.cs_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.cs_nu_area_ha).to_eng_string()) == 0.00],
                'ni_nu_area_ha': [formatarValor(float(Decimal(rs.ni_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.ni_nu_area_ha).to_eng_string()) == 0.00],
                'ne_nu_area_ha': [formatarValor(float(Decimal(rs.ne_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.ne_nu_area_ha).to_eng_string()) == 0.00],
                'sn_nu_area_ha': [formatarValor(float(Decimal(rs.sn_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.sn_nu_area_ha).to_eng_string()) == 0.00],
                'nu_area_total': [formatarValor(float(Decimal(rs.nu_area_total).to_eng_string())), '-'][
                    float(Decimal(rs.nu_area_total).to_eng_string()) == 0.00]
            })

    return jsonFrame


def json_mensal_radarsat2_por_terra_indigena(nu_entrega):
    jsonTerraIndigena = []

    SQL = "co_funai, " \
          "no_ti, " \
          "nu_entrega, " \
          "COALESCE(SUM(corte_raso_nu_area_ha),0.00) AS cr_nu_area_ha, " \
          "COALESCE(SUM(corte_seletivo_nu_area_ha),0.00) AS cs_nu_area_ha, " \
          "COALESCE(SUM(nova_infraestrutura_nu_area_ha),0.00) AS ni_nu_area_ha, " \
          "COALESCE(SUM(nova_estrada_nu_area_ha),0.00) AS ne_nu_area_ha, " \
          "COALESCE(SUM(seletivo_natural_nu_area_ha),0.00) AS sn_nu_area_ha, " \
          "COALESCE(SUM(nu_area_total),0.00) AS nu_area_total " \
          "FROM " \
          "funai.vwm_painel_radarsat2_belo_monte_terra_indigena " \
          "WHERE " \
          "nu_entrega = %s " \
          "GROUP BY " \
          "ROLLUP((co_funai,no_ti,nu_entrega)) " \
          "ORDER BY " \
          "nu_area_total DESC" % (nu_entrega)

    s = select([SQL])

    data = db.engine.execute(s).fetchall()

    if len(data) > 0:
        for rs in data:
            jsonTerraIndigena.append({
                'co_funai': rs.co_funai,
                'no_ti': rs.no_ti,
                'nu_entrega': rs.nu_entrega,
                'cr_nu_area_ha': [formatarValor(float(Decimal(rs.cr_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.cr_nu_area_ha).to_eng_string()) == 0.00],
                'cs_nu_area_ha': [formatarValor(float(Decimal(rs.cs_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.cs_nu_area_ha).to_eng_string()) == 0.00],
                'ni_nu_area_ha': [formatarValor(float(Decimal(rs.ni_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.ni_nu_area_ha).to_eng_string()) == 0.00],
                'ne_nu_area_ha': [formatarValor(float(Decimal(rs.ne_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.ne_nu_area_ha).to_eng_string()) == 0.00],
                'sn_nu_area_ha': [formatarValor(float(Decimal(rs.sn_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.sn_nu_area_ha).to_eng_string()) == 0.00],
                'nu_area_total': [formatarValor(float(Decimal(rs.nu_area_total).to_eng_string())), '-'][
                    float(Decimal(rs.nu_area_total).to_eng_string()) == 0.00]
            })

    return jsonTerraIndigena


def json_mensal_radarsat2_por_municipio(nu_entrega):
    jsonMunicipio = []

    SQL = "nu_geocodigo, " \
          "no_municipio, " \
          "sg_uf, " \
          "nu_entrega, " \
          "COALESCE(nu_percentual_ti_no_municipio,SUM(nu_percentual_ti_no_municipio)) AS nu_percentual_ti_no_municipio, " \
          "COALESCE(SUM(corte_raso_nu_area_ha),0.00) AS cr_nu_area_ha, " \
          "COALESCE(SUM(corte_seletivo_nu_area_ha),0.00) AS cs_nu_area_ha, " \
          "COALESCE(SUM(nova_infraestrutura_nu_area_ha),0.00) AS ni_nu_area_ha, " \
          "COALESCE(SUM(nova_estrada_nu_area_ha),0.00) AS ne_nu_area_ha, " \
          "COALESCE(SUM(seletivo_natural_nu_area_ha),0.00) AS sn_nu_area_ha, " \
          "COALESCE(SUM(nu_area_total),0.00) AS nu_area_total " \
          "FROM " \
          "funai.vwm_painel_radarsat2_belo_monte_municipio " \
          "WHERE " \
          "nu_entrega = %s " \
          "GROUP BY " \
          "ROLLUP((nu_geocodigo,no_municipio,sg_uf,nu_entrega,nu_percentual_ti_no_municipio)) " \
          "ORDER BY " \
          "nu_area_total DESC" % (nu_entrega)

    s = select([SQL])

    data = db.engine.execute(s).fetchall()

    if len(data) > 0:
        for rs in data:
            jsonMunicipio.append({
                'nu_geocodigo': rs.nu_geocodigo,
                'no_municipio': rs.no_municipio,
                'sg_uf': rs.sg_uf,
                'nu_entrega': rs.nu_entrega,
                'nu_percentual_ti_no_municipio':
                    [formatarValor(float(Decimal(rs.nu_percentual_ti_no_municipio).to_eng_string())), '-'][
                        float(Decimal(rs.nu_percentual_ti_no_municipio).to_eng_string()) == 0.00],
                'cr_nu_area_ha': [formatarValor(float(Decimal(rs.cr_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.cr_nu_area_ha).to_eng_string()) == 0.00],
                'cs_nu_area_ha': [formatarValor(float(Decimal(rs.cs_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.cs_nu_area_ha).to_eng_string()) == 0.00],
                'ni_nu_area_ha': [formatarValor(float(Decimal(rs.ni_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.ni_nu_area_ha).to_eng_string()) == 0.00],
                'ne_nu_area_ha': [formatarValor(float(Decimal(rs.ne_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.ne_nu_area_ha).to_eng_string()) == 0.00],
                'sn_nu_area_ha': [formatarValor(float(Decimal(rs.sn_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.sn_nu_area_ha).to_eng_string()) == 0.00],
                'nu_area_total': [formatarValor(float(Decimal(rs.nu_area_total).to_eng_string())), '-'][
                    float(Decimal(rs.nu_area_total).to_eng_string()) == 0.00]
            })

    return jsonMunicipio


def json_mensal_radarsat2_por_terra_indigena_municipio(nu_entrega):
    jsonTerraIndigenaMunicipio = []

    SQL = "(SELECT COUNT(b.id) AS total_ti FROM funai.vwm_painel_radarsat2_belo_monte_ti_municipio AS b WHERE b.co_funai = a.co_funai AND b.nu_entrega = a.nu_entrega ) AS total_ti, " \
          "a.co_funai, " \
          "a.no_ti, " \
          "a.ds_fase_ti, " \
          "a.nu_geocodigo, " \
          "a.no_municipio, " \
          "a.nu_entrega, " \
          "COALESCE(SUM(a.nu_area_municipio_ha),0.00) AS nu_area_municipio_ha, " \
          "COALESCE(SUM(a.nu_area_ti_no_municipio_ha),0.00) AS nu_area_ti_no_municipio_ha, " \
          "COALESCE(SUM(a.nu_percentual_ti_no_municipio),0.00) AS nu_percentual_ti_no_municipio, " \
          "COALESCE(SUM(a.corte_raso_nu_area_ha),0.00) AS cr_nu_area_ha, " \
          "COALESCE(SUM(a.corte_seletivo_nu_area_ha),0.00) AS cs_nu_area_ha, " \
          "COALESCE(SUM(a.nova_infraestrutura_nu_area_ha),0.00) AS ni_nu_area_ha, " \
          "COALESCE(SUM(a.nova_estrada_nu_area_ha),0.00) AS ne_nu_area_ha, " \
          "COALESCE(SUM(a.seletivo_natural_nu_area_ha),0.00) AS sn_nu_area_ha, " \
          "COALESCE(SUM(a.nu_area_total),0.00) AS nu_area_total " \
          "FROM " \
          "funai.vwm_painel_radarsat2_belo_monte_ti_municipio AS a " \
          "WHERE " \
          "a.nu_entrega = %s " \
          "GROUP BY " \
          "ROLLUP((a.co_funai,a.no_ti,a.ds_fase_ti,a.nu_geocodigo,a.no_municipio,a.nu_entrega)) " \
          "ORDER BY " \
          "a.no_ti, a.no_municipio" % (nu_entrega)

    s = select([SQL])

    data = db.engine.execute(s).fetchall()

    if len(data) > 0:
        for rs in data:
            jsonTerraIndigenaMunicipio.append({
                'nu_total_ti': rs.total_ti,
                'co_funai': rs.co_funai,
                'no_ti': rs.no_ti,
                'ds_fase_ti': rs.ds_fase_ti,
                'nu_geocodigo': rs.nu_geocodigo,
                'no_municipio': rs.no_municipio,
                'nu_entrega': rs.nu_entrega,
                'nu_area_municipio_ha': [formatarValor(float(Decimal(rs.nu_area_municipio_ha).to_eng_string())), '-'][
                    float(Decimal(rs.nu_area_municipio_ha).to_eng_string()) == 0.00],
                'nu_area_ti_no_municipio_ha':
                    [formatarValor(float(Decimal(rs.nu_area_ti_no_municipio_ha).to_eng_string())), '-'][
                        float(Decimal(rs.nu_area_ti_no_municipio_ha).to_eng_string()) == 0.00],
                'nu_percentual_ti_no_municipio':
                    [formatarValor(float(Decimal(rs.nu_percentual_ti_no_municipio).to_eng_string())), '-'][
                        float(Decimal(rs.nu_percentual_ti_no_municipio).to_eng_string()) == 0.00],
                'cr_nu_area_ha': [formatarValor(float(Decimal(rs.cr_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.cr_nu_area_ha).to_eng_string()) == 0.00],
                'cs_nu_area_ha': [formatarValor(float(Decimal(rs.cs_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.cs_nu_area_ha).to_eng_string()) == 0.00],
                'ni_nu_area_ha': [formatarValor(float(Decimal(rs.ni_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.ni_nu_area_ha).to_eng_string()) == 0.00],
                'ne_nu_area_ha': [formatarValor(float(Decimal(rs.ne_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.ne_nu_area_ha).to_eng_string()) == 0.00],
                'sn_nu_area_ha': [formatarValor(float(Decimal(rs.sn_nu_area_ha).to_eng_string())), '-'][
                    float(Decimal(rs.sn_nu_area_ha).to_eng_string()) == 0.00],
                'nu_area_total': [formatarValor(float(Decimal(rs.nu_area_total).to_eng_string())), '-'][
                    float(Decimal(rs.nu_area_total).to_eng_string()) == 0.00]
            })

    return jsonTerraIndigenaMunicipio


def json_mensal_radarsat2_por_terra_indigena_municipio_by_nome(nu_entrega):
    jsonTerraIndigenaMunicipio = []

    SQL = "(SELECT COUNT(b.id) AS total_ti FROM funai.vwm_painel_radarsat2_belo_monte_ti_municipio AS b WHERE b.co_funai = a.co_funai AND b.nu_entrega = a.nu_entrega ) AS total_ti, " \
          "a.co_funai, " \
          "a.no_ti, " \
          "a.ds_fase_ti " \
          "FROM " \
          "funai.vwm_painel_radarsat2_belo_monte_ti_municipio AS a " \
          "WHERE " \
          "a.nu_entrega = %s " \
          "GROUP BY " \
          "total_ti, a.co_funai, a.no_ti, a.ds_fase_ti " \
          "ORDER BY " \
          "a.no_ti" % (nu_entrega)

    s = select([SQL])

    data = db.engine.execute(s).fetchall()

    if len(data) > 0:
        for rs in data:
            jsonTerraIndigenaMunicipio.append({
                'nu_total_ti': rs.total_ti,
                'co_funai': rs.co_funai,
                'no_ti': rs.no_ti,
                'ds_fase_ti': rs.ds_fase_ti
            })

    return jsonTerraIndigenaMunicipio


def json_base_imagem_monitoramento_consolidado(ciclo_id, nu_orbita):
    jsonBaseImagem = []

    SQL = "no_imagem, " \
          "ds_orbita_ponto, " \
          "nu_orbita, " \
          "ciclo_id, " \
          "nu_nuvem, " \
          "dt_imagem " \
          "FROM " \
          "aplicacao.vw_monitoramento_consolidado_catalogo_landsat " \
          "WHERE " \
          "ciclo_id = %s " \
          "AND " \
          "nu_orbita IN ('%s') " \
          "ORDER BY nu_nuvem DESC" % (ciclo_id, nu_orbita)

    s = select([SQL])

    data = db.engine.execute(s).fetchall()

    if len(data) > 0:
        for rs in data:
            jsonBaseImagem.append({
                'ds_orbita_ponto': rs.ds_orbita_ponto,
                'no_imagem': rs.no_imagem,
                'dt_imagem': rs.dt_imagem,
                'nu_nuvem': [formatarValor(float(Decimal(rs.nu_nuvem).to_eng_string())), '-'][
                    float(Decimal(rs.nu_nuvem).to_eng_string()) == 0.00]
            })

    return jsonBaseImagem


def backup_tabela_interprete():
    resultSet = db.engine.execute(
        "SELECT id, no_interprete, tp_interprete, no_tabela, REPLACE(no_tabela,'monitoramento.','backup.') || to_char(date 'NOW()' - integer '1','_DD_MM') AS no_tabela_tmp_del, REPLACE(no_tabela,'monitoramento.','backup.') || to_char(NOW(),'_DD_MM') AS no_tabela_tmp_new FROM funai.tb_interprete_monitora WHERE st_ativo = 't' AND tp_interprete = 'A' ORDER BY id ASC").fetchall()

    if len(resultSet) > 0:

        db.engine.execute("BEGIN")

        try:

            for rs in resultSet:
                db.engine.execute("DROP TABLE IF EXISTS {0} CASCADE".format(rs.no_tabela_tmp_new))
                db.engine.execute("CREATE TABLE {0} AS SELECT * FROM {1}".format(rs.no_tabela_tmp_new, rs.no_tabela))
                db.engine.execute("DROP TABLE IF EXISTS {0} CASCADE".format(rs.no_tabela_tmp_del))
                db.engine.execute("COMMIT")

        except (Exception, ValueError) as e:
            db.engine.execute("ROLLBACK")


def sincronizar_tabela_monitoramento_alexandre():
    resultSet = db.engine.execute(
        "SELECT id, no_interprete, tp_interprete, no_tabela, REPLACE(no_tabela,'monitoramento.','backup.') || to_char(date 'NOW()' - integer '1','_DD_MM') AS no_tabela_tmp_del, REPLACE(no_tabela,'monitoramento.','backup.') || to_char(NOW(),'_DD_MM') AS no_tabela_tmp_new FROM funai.tb_interprete_monitora WHERE id = 9").fetchall()

    if len(resultSet) > 0:

        db.engine.execute("BEGIN")

        try:

            for rs in resultSet:
                db.engine.execute("DROP TABLE IF EXISTS {0} CASCADE".format(rs.no_tabela_tmp_new))
                db.engine.execute("CREATE TABLE {0} AS SELECT * FROM {1}".format(rs.no_tabela_tmp_new, rs.no_tabela))
                db.engine.execute("DROP TABLE IF EXISTS {0} CASCADE".format(rs.no_tabela_tmp_del))

                db.engine.execute(
                    "INSERT INTO monitoramento.monitoramento_miriam_silva_a(no_estagio, ds_observacao, no_imagem, dt_imagem, nu_orbita, nu_ponto, dt_t_zero, dt_t_um, nu_area_km2, nu_area_ha, geom, dt_atualizacao, dt_cadastro) SELECT no_estagio, ds_observacao, no_imagem, dt_imagem, nu_orbita, nu_ponto, dt_t_zero, dt_t_um, nu_area_km2, nu_area_ha, geom, dt_atualizacao, dt_cadastro FROM monitoramento.monitoramento_alexandre_silva_a")

                db.engine.execute("DELETE FROM {0}".format(rs.no_tabela))
                db.engine.execute("ALTER SEQUENCE IF EXISTS {0}_id_seq RESTART WITH 1".format(rs.no_tabela))
                db.engine.execute("COMMIT")

        except (Exception, ValueError) as e:
            db.engine.execute("ROLLBACK")


def sincronizar_tabela_monitoramento_aryanne():
    resultSet = db.engine.execute(
        "SELECT id, no_interprete, tp_interprete, no_tabela, REPLACE(no_tabela,'monitoramento.','backup.') || to_char(date 'NOW()' - integer '1','_DD_MM') AS no_tabela_tmp_del, REPLACE(no_tabela,'monitoramento.','backup.') || to_char(NOW(),'_DD_MM') AS no_tabela_tmp_new FROM funai.tb_interprete_monitora WHERE id = 10").fetchall()

    if len(resultSet) > 0:

        db.engine.execute("BEGIN")

        try:

            for rs in resultSet:
                db.engine.execute("DROP TABLE IF EXISTS {0} CASCADE".format(rs.no_tabela_tmp_new))
                db.engine.execute("CREATE TABLE {0} AS SELECT * FROM {1}".format(rs.no_tabela_tmp_new, rs.no_tabela))
                db.engine.execute("DROP TABLE IF EXISTS {0} CASCADE".format(rs.no_tabela_tmp_del))

                db.engine.execute(
                    "INSERT INTO monitoramento.monitoramento_guilherme_micas_a(no_estagio, ds_observacao, no_imagem, dt_imagem, nu_orbita, nu_ponto, dt_t_zero, dt_t_um, nu_area_km2, nu_area_ha, geom, dt_atualizacao, dt_cadastro) SELECT no_estagio, ds_observacao, no_imagem, dt_imagem, nu_orbita, nu_ponto, dt_t_zero, dt_t_um, nu_area_km2, nu_area_ha, geom, dt_atualizacao, dt_cadastro FROM monitoramento.monitoramento_aryanne_rodrigues_a")

                db.engine.execute("DELETE FROM {0}".format(rs.no_tabela))
                db.engine.execute("ALTER SEQUENCE IF EXISTS {0}_id_seq RESTART WITH 1".format(rs.no_tabela))
                db.engine.execute("COMMIT")

        except (Exception, ValueError) as e:
            db.engine.execute("ROLLBACK")


def json_filtro_aviso_diario_relatorio_ti():
    json = []
    jti = []
    jcr = []
    juf = []

    SQL = "co_funai, no_ti FROM aviso.vwm_relatorio_monitora_aviso_diario_por_terra_indigena GROUP BY co_funai, no_ti ORDER BY no_ti"

    s = select([SQL])

    data = db.engine.execute(s).fetchall()

    if len(data) > 0:
        for rs in data:
            jti.append({
                'co_funai': rs.co_funai,
                'no_ti': rs.no_ti
            })

    json.append({'ti': jti})

    SQL = "id_cr, ds_cr FROM aviso.vwm_relatorio_monitora_aviso_diario_por_terra_indigena GROUP BY id_cr, ds_cr ORDER BY ds_cr"

    s = select([SQL])

    data = db.engine.execute(s).fetchall()

    if len(data) > 0:
        for rs in data:
            jcr.append({
                'id_cr': rs.id_cr,
                'ds_cr': rs.ds_cr
            })
    
    json.append({'cr': jcr})

    SQL = "id_uf, sg_uf FROM aviso.vwm_relatorio_monitora_aviso_diario_por_terra_indigena GROUP BY id_uf, sg_uf ORDER BY sg_uf"

    s = select([SQL])

    data = db.engine.execute(s).fetchall()

    if len(data) > 0:
        for rs in data:
            juf.append({
                'id_uf': rs.id_uf,
                'sg_uf': rs.sg_uf
            })

    json.append({'uf': juf})

    return json

def json_filtro_aviso_diario_relatorio_cr():
    json = []

    SQL = "id_cr, ds_cr FROM aviso.vwm_relatorio_monitora_aviso_diario_por_ti_coordenacao_regional GROUP BY id_cr, ds_cr ORDER BY ds_cr"

    s = select([SQL])

    data = db.engine.execute(s).fetchall()

    if len(data) > 0:
        for rs in data:
            json.append({
                'id_cr': rs.id_cr,
                'ds_cr': rs.ds_cr
            })

    return json

def json_filtro_aviso_diario_relatorio_uf():
    json = []

    SQL = "sg_uf, no_uf FROM aviso.vwm_relatorio_monitora_aviso_diario_por_uf GROUP BY sg_uf, no_uf ORDER BY no_uf"

    s = select([SQL])

    data = db.engine.execute(s).fetchall()

    if len(data) > 0:
        for rs in data:
            json.append({
                'sg_uf': rs.sg_uf,
                'no_uf': rs.no_uf
            })

    return json
