# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

from functools import wraps
from flask import session, redirect, url_for


def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if "usuario" in session:
            return f(*args, **kwargs)
        else:
            return redirect(url_for("login"))

    return wrap
