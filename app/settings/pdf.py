# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

from app import app, db
from fpdf import FPDF, HTMLMixin


class radarSat2_PDF(FPDF):
    def header(self):
        w = 204
        h = 16
        if self.cur_orientation == 'L':
            w = 296
            h = 22

        # Logo
        self.image(app.config['IMG_FOLDER'] + "cabecalho_aviso.png", x=4, w=w, h=h)
        # Line break
        self.ln(2)

    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')


class avisoDiario_PDF(FPDF, HTMLMixin):
    def header(self):
        # Logo
        self.image(app.config['IMG_FOLDER'] + "cabecalho_aviso.png", x=4, w=204, h=16)
        # Line break
        self.ln(2)

    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Logo
        self.image(app.config['IMG_FOLDER'] + "rodape_aviso.png", x=4, y=self.get_y(), w=204, h=16)
        # # Arial italic 8
        self.set_font('Arial', 'BI', 10)
        # # Page number
        self.cell(0, 10, 'Projeto CMR - ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')
