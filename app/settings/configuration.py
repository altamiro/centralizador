# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

import logging

from os import environ, getcwd
from os.path import join, dirname, abspath
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)


class Config(object):
    """
        Configuration base, for all environments
    """
    DBHOST = environ.get("DBHOST")
    DBNAME = environ.get("DBNAME")
    DBUSER = environ.get("DBUSER")
    DBPASS = environ.get("DBPASS")
    ROOT_PATH = "/var/www/html/centralize/"
    IMG_FOLDER = join(ROOT_PATH, "app/static/img/")
    SHP_FOLDER = join(ROOT_PATH, "app/static/shp/")
    UPLOAD_FOLDER = join(ROOT_PATH, "tmp")
    LOGGING_LOCATION = join(ROOT_PATH, "app.log")
    DEBUG = False
    TESTING = False
    ALLOWED_EXTENSIONS = set(['zip', 'rar'])
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    LOGGING_FORMAT = "%(asctime)s - %(levelname)s - %(message)s\n"
    LOGGING_DATEFMT = "%d/%m/%Y %H:%M:%S"
    LOGGING_FILEMODE = "w"
    LOGGING_LEVEL = logging.DEBUG
    SECRET_KEY = environ.get("SECRET_KEY")
    MAIL_SERVER = environ.get("MAIL_SERVER")
    MAIL_PORT = environ.get("MAIL_PORT")
    MAIL_USERNAME = environ.get("MAIL_USERNAME")
    MAIL_PASSWORD = environ.get("MAIL_PASSWORD")
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    EMAIL_SENDER = "DBA/HEX - Sistema de Alerta <{}>".format(environ.get("MAIL_USERNAME"))
    EMAIL_GABRIEL = environ.get("EMAIL_GABRIEL")
    EMAIL_MAIRA = environ.get("EMAIL_MAIRA")
    DETER_URL = environ.get("DETER_URL")
    DETER_USERNAME = environ.get("DETER_USERNAME")
    DETER_PASSWORD = environ.get("DETER_PASSWORD")
    DETER_TIPO = environ.get("DETER_TIPO")
    AWIFS_URL = environ.get("AWIFS_URL")
    AWIFS_USERNAME = environ.get("AWIFS_USERNAME")
    AWIFS_PASSWORD = environ.get("AWIFS_PASSWORD")
    AWIFS_TIPO = environ.get("AWIFS_TIPO")
    TERRA_INDIGENA_URL = environ.get("TERRA_INDIGENA_URL")
    TERRA_INDIGENA_ARQUIVO = environ.get("TERRA_INDIGENA_ARQUIVO")
    TERRA_INDIGENA_ESTUDO_URL = environ.get("TERRA_INDIGENA_ESTUDO_URL")
    TERRA_INDIGENA_ESTUDO_ARQUIVO = environ.get(
        "TERRA_INDIGENA_ESTUDO_ARQUIVO")
    COORDENACAO_REGIONAL_URL = environ.get("COORDENACAO_REGIONAL_URL")
    COORDENACAO_REGIONAL_ARQUIVO = environ.get("COORDENACAO_REGIONAL_ARQUIVO")
    ALDEIA_INDIGENA_URL = environ.get("ALDEIA_INDIGENA_URL")
    ALDEIA_INDIGENA_ARQUIVO = environ.get("ALDEIA_INDIGENA_ARQUIVO")
    DETER_PUBLIC_AWIFS_URL = environ.get("DETER_PUBLIC_AWIFS_URL")
    DETER_PUBLIC_AWIFS_ARQUIVO = environ.get("DETER_PUBLIC_AWIFS_ARQUIVO")


class ProductionConfig(Config):
    APPLICATION_ROOT = "/centralize"
    SQLALCHEMY_DATABASE_URI = "postgresql://%s:%s@%s/%s" % (
        environ.get("DBUSER"), environ.get("DBPASS"), environ.get("DBHOST"), environ.get("DBNAME"))
    PSYCOPG2_DATABASE_URI = "host=%s dbname=%s user=%s password=%s" % (
        environ.get("DBHOST"), environ.get("DBNAME"), environ.get("DBUSER"), environ.get("DBPASS"))


class DevelopmentConfig(Config):
    ROOT_PATH = "/home/altamiro/Storage/Projetos/Trabalho/funai/centralize/"
    IMG_FOLDER = join(ROOT_PATH, "app/static/img/")
    SHP_FOLDER = join(ROOT_PATH, "app/static/shp/")
    UPLOAD_FOLDER = join(ROOT_PATH, "tmp")
    LOGGING_LOCATION = join(ROOT_PATH, "app.log")
    DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = "postgresql://%s:%s@%s/%s" % (
    environ.get("DBUSER"), environ.get("DBPASS"), environ.get("DBHOST"), environ.get("DBNAME"))
    PSYCOPG2_DATABASE_URI = "host=%s dbname=%s user=%s password=%s" % (
    environ.get("DBHOST"), environ.get("DBNAME"), environ.get("DBUSER"), environ.get("DBPASS"))


class TestingConfig(Config):
    TESTING = True
