# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>
import psycopg2
import psycopg2.extras

from flask import render_template, request, jsonify, redirect, url_for, session, make_response, send_file
from flask_weasyprint import HTML, CSS, render_pdf
from werkzeug.utils import secure_filename
from datetime import timedelta
from unipath import Path
from os import mkdir
from os.path import join, exists
from sqlalchemy.sql import select, func, and_, or_, text
from decimal import *

from app import app
from .models import *
from .settings.utils import *
from .settings.decorators import login_required
from .radarsat2.trimestral import processar_dados_trimestral
from .radarsat2.mensal import processar_dados_mensal
from .buffer.buffer_ate_2015 import novo_pacote, gerar_shapefile_pacote


@app.before_request
def sessao_permanente():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=60)


@app.context_processor
def processar_variavel_global():
    url = str(request.url).replace("http://", "").split("/")

    style_app2 = 0

    if url[0].startswith('localhost'):
        baseUrl = "http://%s/" % (url[0])
        if url[1] == 'visualizar-entrega-mensal-radarsat2':
            style_app2 = 1
    else:
        baseUrl = "http://%s/%s/" % (url[0], url[1])
        if url[2] == 'visualizar-entrega-mensal-radarsat2':
            style_app2 = 1
    return dict(baseUrl=baseUrl, style_app2=style_app2)


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        ds_email = request.form["ds_email"]
        ds_senha = request.form["ds_senha"].encode("utf-8")
        usuario = Usuario.query.filter_by(ds_email=ds_email).first()

        if usuario is not None and usuario.st_ativo == False:
            message = "E-mail %s inativo! Verifique com Administrador." % (
                ds_email)
            return jsonify({"status": "error", "message": message})
        elif usuario is not None and usuario.verify_password(ds_senha):
            session["usuario"] = {
                "id": usuario.id,
                "no_usuario": usuario.no_usuario,
                "ds_email": usuario.ds_email,
                "st_analista": usuario.st_analista,
                "st_administrador": usuario.st_administrador,
                "st_ativo": usuario.st_ativo
            }
            return jsonify({"status": "ok"})
        else:
            message = "E-mail %s ou Senha Inválido!" % (ds_email)
            return jsonify({"status": "error", "message": message})
    else:
        return render_template("login.html")


@app.route("/")
@login_required
def index():
    tabela_informacao_aplicacao = VisaoTabelaInformacaoAtualizacao.query.order_by(
        VisaoTabelaInformacaoAtualizacao.dt_cadastro.desc()).limit(10).all()

    entrega_aviso_diario = VisaoEntregaAvisoDiario.query.order_by(
        VisaoEntregaAvisoDiario.id.desc()).limit(8).all()

    info_area_total_ti_amz = VisaoInfoAreaTotalTerraIndigenaAMZ.query.all()

    sm_backup_mapeamento=VisaoServidorServicoMonitorado().to_status_mount()

    sm_web_banco_outro=VisaoServidorServicoMonitorado().to_status_notilike_mount()

    return render_template("index.html", tabela_informacao_aplicacao=tabela_informacao_aplicacao,
                           entrega_aviso_diario=entrega_aviso_diario,
                           sm_backup_mapeamento=sm_backup_mapeamento,
                           sm_web_banco_outro=sm_web_banco_outro,
                           info_area_total_ti_amz=info_area_total_ti_amz)


@app.route("/logout")
@login_required
def logout():
    session.clear()
    return redirect(url_for("login"))


@app.route("/usuario")
@login_required
def usuario():
    usuario = Usuario.query.filter_by(
        st_ativo=True).order_by(Usuario.no_usuario).all()
    return render_template("usuario.html", usuario=usuario)


@app.route("/usuario/formulario", methods=["GET", "POST"])
@app.route("/usuario/formulario/<int:id>", methods=["GET", "POST"])
@login_required
def usuario_formulario(id=0):
    if request.method == "POST":
        id = request.form["id"]
        no_usuario = request.form["no_usuario"]
        ds_email = request.form["ds_email"]
        ds_senha = request.form["ds_senha"].encode("utf-8")
        st_analista = request.form["st_analista"]
        st_administrador = request.form["st_administrador"]
        st_ativo = request.form["st_ativo"]
        if len(id) > 0:
            try:
                usuario = Usuario.query.get(id)
                usuario.no_usuario = no_usuario
                usuario.ds_email = ds_email
                if len(ds_senha) > 0:
                    usuario.password = ds_senha
                usuario.st_analista = st_analista
                usuario.st_administrador = st_administrador
                usuario.st_ativo = st_ativo
                usuario.dt_atualizacao = "now()"
                db.session.add(usuario)
                db.session.commit()

                message = "Usuário atualizado com sucesso!"
                url = url_for("usuario")

                session_id = getSessaoUsuario('id')
                if int(session_id) == int(id):
                    message = "Usuário atualizado com sucesso! Você precisa efetuar o login no sistema novamente."
                    url = url_for("logout")

                return jsonify({"status": "ok", "message": message, "url": url})
            except Exception as e:
                app.logger.exception(e)
                db.session.rollback()
                return jsonify({"status": "error", "message": "Ocorreu um erro ao atualizar os dados."})
        else:
            try:
                usuario = Usuario(no_usuario, ds_email, ds_senha,
                                  st_analista, st_administrador)
                db.session.add(usuario)
                db.session.commit()
                url = url_for("usuario")
                return jsonify({"status": "ok", "message": "Usuário cadastrado com sucesso.", "url": url})
            except Exception as e:
                app.logger.exception(e)
                db.session.rollback()
                return jsonify({"status": "error", "message": "Ocorreu um erro ao cadastrar os dados."})
    else:
        usuario = Usuario.query.get(id)
        return render_template("usuario_formulario.html", usuario=usuario)


@app.route("/aviso-diario")
@login_required
def aviso_diario():
    ciclos_monitorados_legado = VisaoCicloMonitoramentoConsolidado.query.filter(
        or_(VisaoCicloMonitoramentoConsolidado.id <= 6, VisaoCicloMonitoramentoConsolidado.id == 19)).all()
    ciclos_monitorados = VisaoCicloMonitoramentoConsolidado.query.filter(
        and_(VisaoCicloMonitoramentoConsolidado.id >= 7, VisaoCicloMonitoramentoConsolidado.id != 19)).all()
    ciclos_monitorados_ano_atual = VisaoCicloMonitoramentoConsolidado.query.filter(
        and_(VisaoCicloMonitoramentoConsolidado.id >= 18, VisaoCicloMonitoramentoConsolidado.id != 19, VisaoCicloMonitoramentoConsolidado.id <= 18)).all()

    filtro_aviso_diario_relatorio_ti = json_filtro_aviso_diario_relatorio_ti()
    json_ti = filtro_aviso_diario_relatorio_ti[0]['ti']
    json_cr = filtro_aviso_diario_relatorio_ti[1]['cr']
    json_uf = filtro_aviso_diario_relatorio_ti[2]['uf']
    filtro_aviso_diario_relatorio_cr = json_filtro_aviso_diario_relatorio_cr()
    filtro_aviso_diario_relatorio_uf = json_filtro_aviso_diario_relatorio_uf()

    return render_template('aviso_diario.html', ciclos_monitorados_legado=ciclos_monitorados_legado,
                           ciclos_monitorados=ciclos_monitorados,
                           ciclos_monitorados_ano_atual=ciclos_monitorados_ano_atual,
                           json_ti=json_ti,
                           json_cr=json_cr,
                           json_uf=json_uf,
                           filtro_aviso_diario_relatorio_cr=filtro_aviso_diario_relatorio_cr,
                           filtro_aviso_diario_relatorio_uf=filtro_aviso_diario_relatorio_uf)


@app.route("/buscar-orbita-ciclo", methods=['POST'])
@login_required
def buscar_orbita_ciclo():
    json = []

    result = VisaoMonitoramentoNuOrbita.query.filter_by(
        ciclo_id=request.form['ciclo_id']).all()

    if len(result) > 0:
        for rs in result:
            json.append({
                'nu_orbita': rs.nu_orbita,
                'ds_descricao': rs.ds_descricao
            })

    return jsonify({'data': json})


@app.route("/buscar-data-t-um-ciclo", methods=['POST'])
@login_required
def buscar_data_t_um_ciclo():
    json = []

    param = str(request.form.getlist('nu_orbita[]')).replace('[', '').replace(']', '').replace('u', '').replace(', ',
                                                                                                                ',').replace(
        "'", '').split(',')

    result = VisaoMonitoramentoDataTUm.query.filter_by(ciclo_id=request.form['ciclo_id']).filter(
        VisaoMonitoramentoDataTUm.nu_orbita.in_(param)).all()

    if len(result) > 0:
        for rs in result:
            json.append({
                'dt_t_um': str(rs.dt_t_um),
                'ds_descricao': '(' + rs.nu_orbita + ')' + ' ' + rs.ds_descricao
            })

    return jsonify({'data': json})


@app.route("/pesquisar-ciclo-fitro", methods=['POST'])
@login_required
def pesquisar_ciclo_por_fitro():
    json = []
    orbita_and = "AND 1=1"
    dt_and = "AND 1=1"

    params = {
        'ciclo_id': request.form['ciclo_id'],
        'tp_filtro': request.form['tp_filtro'],
        'nu_orbita': str(request.form.getlist('nu_orbita[]')).replace('[', '').replace(']', '').replace('u',
                                                                                                        '').replace(
            ', ', ','),
        'dt_t_um': str(request.form.getlist('dt_t_um[]')).replace('[', '').replace(']', '').replace('u', '').replace(
            ', ', ',')
    }

    if len(params['nu_orbita']) > 0:
        orbita_and = "AND nu_orbita IN (" + params['nu_orbita'] + ") "

    if len(params['dt_t_um']) > 0:
        dt_and = "AND dt_t_um IN (" + params['dt_t_um'] + ") "

    if 'ti' in params['tp_filtro']:  # pesquisa por ti

        SQL = "co_funai, " \
              "no_ti, " \
              "no_ciclo, " \
              "nu_orbita, " \
              "dt_t_um_formatada, " \
              "dt_t_um, " \
              "SUM(cr_nu_area_ha) AS cr_nu_area_ha, " \
              "SUM(dg_nu_area_ha) AS dg_nu_area_ha, " \
              "SUM(dr_nu_area_ha) AS dr_nu_area_ha, " \
              "SUM(ff_nu_area_ha) AS ff_nu_area_ha, " \
              "SUM(total_nu_area_ha) AS total_nu_area_ha " \
              "FROM " \
              "aviso.vwm_relatorio_monitora_aviso_diario_por_terra_indigena " \
              "WHERE " \
              "ciclo_id = %s " \
              "%s " \
              "%s " \
              "GROUP BY ROLLUP ((co_funai,no_ti,no_ciclo,nu_orbita,dt_t_um_formatada,dt_t_um)) " \
              "ORDER BY no_ti, dt_t_um" % (
                  params['ciclo_id'], orbita_and, dt_and)

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 0:
            for rs in data:
                json.append({
                    'co_funai': rs.co_funai,
                    'no_ti': rs.no_ti,
                    'no_ciclo': rs.no_ciclo,
                    'nu_orbita': rs.nu_orbita,
                    'dt_t_um_formatada': rs.dt_t_um_formatada,
                    'cr_nu_area_ha': float(Decimal(rs.cr_nu_area_ha).to_eng_string()),
                    'dg_nu_area_ha': float(Decimal(rs.dg_nu_area_ha).to_eng_string()),
                    'dr_nu_area_ha': float(Decimal(rs.dr_nu_area_ha).to_eng_string()),
                    'ff_nu_area_ha': float(Decimal(rs.ff_nu_area_ha).to_eng_string()),
                    'total_nu_area_ha': float(Decimal(rs.total_nu_area_ha).to_eng_string())
                })

    elif 'cr' in params['tp_filtro']:  # pesquisa por cr

        SQL = "ds_cr, " \
              "no_ciclo, " \
              "nu_orbita, " \
              "dt_t_um_formatada, " \
              "dt_t_um, " \
              "SUM(cr_nu_area_ha) AS cr_nu_area_ha, " \
              "SUM(dg_nu_area_ha) AS dg_nu_area_ha, " \
              "SUM(dr_nu_area_ha) AS dr_nu_area_ha, " \
              "SUM(ff_nu_area_ha) AS ff_nu_area_ha, " \
              "SUM(total_nu_area_ha) AS total_nu_area_ha " \
              "FROM " \
              "aviso.vwm_relatorio_monitora_aviso_diario_por_ti_coordenacao_regional " \
              "WHERE " \
              "ciclo_id = %s " \
              "%s " \
              "%s " \
              "GROUP BY ROLLUP ((ds_cr,no_ciclo,nu_orbita,dt_t_um_formatada,dt_t_um)) " \
              "ORDER BY ds_cr, dt_t_um" % (
                  params['ciclo_id'], orbita_and, dt_and)

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 0:
            for rs in data:
                json.append({
                    'ds_cr': rs.ds_cr,
                    'no_ciclo': rs.no_ciclo,
                    'nu_orbita': rs.nu_orbita,
                    'dt_t_um_formatada': rs.dt_t_um_formatada,
                    'cr_nu_area_ha': float(Decimal(rs.cr_nu_area_ha).to_eng_string()),
                    'dg_nu_area_ha': float(Decimal(rs.dg_nu_area_ha).to_eng_string()),
                    'dr_nu_area_ha': float(Decimal(rs.dr_nu_area_ha).to_eng_string()),
                    'ff_nu_area_ha': float(Decimal(rs.ff_nu_area_ha).to_eng_string()),
                    'total_nu_area_ha': float(Decimal(rs.total_nu_area_ha).to_eng_string())
                })

    elif 'uf' in params['tp_filtro']:  # pesquisa por df

        SQL = "no_uf, " \
              "sg_uf, " \
              "no_ciclo, " \
              "nu_orbita, " \
              "dt_t_um_formatada, " \
              "dt_t_um, " \
              "SUM(cr_nu_area_ha) AS cr_nu_area_ha, " \
              "SUM(dg_nu_area_ha) AS dg_nu_area_ha, " \
              "SUM(dr_nu_area_ha) AS dr_nu_area_ha, " \
              "SUM(ff_nu_area_ha) AS ff_nu_area_ha, " \
              "SUM(total_nu_area_ha) AS total_nu_area_ha " \
              "FROM " \
              "aviso.vwm_relatorio_monitora_aviso_diario_por_uf " \
              "WHERE " \
              "ciclo_id = %s " \
              "%s " \
              "%s " \
              "GROUP BY ROLLUP ((no_uf,sg_uf,no_ciclo,nu_orbita,dt_t_um_formatada,dt_t_um)) " \
              "ORDER BY no_uf, dt_t_um" % (
                  params['ciclo_id'], orbita_and, dt_and)

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 0:
            for rs in data:
                json.append({
                    'no_uf': rs.no_uf,
                    'sg_uf': rs.sg_uf,
                    'no_ciclo': rs.no_ciclo,
                    'nu_orbita': rs.nu_orbita,
                    'dt_t_um_formatada': rs.dt_t_um_formatada,
                    'cr_nu_area_ha': float(Decimal(rs.cr_nu_area_ha).to_eng_string()),
                    'dg_nu_area_ha': float(Decimal(rs.dg_nu_area_ha).to_eng_string()),
                    'dr_nu_area_ha': float(Decimal(rs.dr_nu_area_ha).to_eng_string()),
                    'ff_nu_area_ha': float(Decimal(rs.ff_nu_area_ha).to_eng_string()),
                    'total_nu_area_ha': float(Decimal(rs.total_nu_area_ha).to_eng_string())
                })

    return jsonify({'data': json})


@app.route("/atualizar-monitoramento-consolidado", methods=['POST'])
@login_required
def atualizar_monitoramento_consolidado():
    data = {
        'message': 'CARGA efetuada com sucesso e tabela dos INTÉRPRETES liberada! Pode gerar o novo Aviso Diário.',
        'status': 1,
        'error': ''
    }

    db2 = psycopg2.connect(app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
    conn = db2.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

    conn.execute(
        "SELECT COUNT(id) AS nu_total, age(to_char(NOW(),'YYYY-MM-DD HH24:MI:SS')::timestamp, to_char(NOW(),'YYYY-MM-DD 16:00')::timestamp)::varchar(9) AS hr_inicio, CASE WHEN '00:00:00'::interval > age(to_char(NOW(),'YYYY-MM-DD HH24:MI:SS')::timestamp, to_char(NOW(),'YYYY-MM-DD 16:00')::timestamp) THEN 'f' ELSE 'd' END::char(1) AS st_msg FROM funai.img_monitoramento_consolidado_a WHERE to_char(dt_cadastro,'YYYY-MM-DD') = to_char(NOW(),'YYYY-MM-DD')")
    result = conn.fetchone()

    if result.nu_total > 0:
        data['message'] = "<h4>Os dados do monitoramento consolidado já foram atualizados hoje.</h4>"
        data['status'] = 0
    elif result.nu_total == 0 and result.st_msg == 'f':
        data[
            'message'] = "<h4 class='text-danger'>Os dados não podem ser atualizados... Faltam %shs para liberar o procedimento. Aguarde.. <br /><br /><small class='pull-right'>Liberado apenas às 16:00h</small></h4>" % (
            result.hr_inicio)
        data['status'] = 0
    else:
        backup_tabela_interprete()
        app.logger.info("__inicio_processamento_monitoramento_consolidado__")

        conn.execute(
            "SELECT id, no_interprete, tp_interprete, no_tabela, REPLACE(no_tabela,'monitoramento.','backup.') || to_char(date 'NOW()' - integer '1','_DD_MM') AS no_tabela_tmp_del, REPLACE(no_tabela,'monitoramento.','backup.') || to_char(NOW(),'_DD_MM') AS no_tabela_tmp_new FROM funai.tb_interprete_monitora WHERE st_ativo = 't' AND tp_interprete = 'A' ORDER BY id ASC")
        resultSet = conn.fetchall()

        booTransacao = 0

        if len(resultSet) > 0:

            for rs in resultSet:

                conn.execute(
                    "SELECT c.id AS tb_ciclo_monitoramento_id, a.no_estagio, a.no_imagem, a.dt_imagem, a.nu_orbita, a.nu_ponto, a.dt_t_zero, a.dt_t_um, a.geom FROM {0} AS a, funai.vm_ciclo_monitoramento AS c WHERE c.dt_monitorada = a.dt_t_um".format(
                        rs.no_tabela))
                result = conn.fetchall()

                if len(result) > 0:

                    tb_interprete_monitora_id = rs.id

                    for rs in result:

                        conn.execute("BEGIN")
                        app.logger.info("BEGIN")

                        try:

                            conn.execute(
                                "INSERT INTO funai.img_monitoramento_consolidado_a(tb_ciclo_monitoramento_id, no_estagio, no_imagem, dt_imagem, nu_orbita, nu_ponto, dt_t_zero, dt_t_um, geom) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, ST_Multi(ST_Setsrid(ST_Buffer(%s,0.0),4674)))",
                                [rs.tb_ciclo_monitoramento_id, rs.no_estagio, rs.no_imagem, rs.dt_imagem,
                                 rs.nu_orbita, rs.nu_ponto, rs.dt_t_zero, rs.dt_t_um, rs.geom])

                            conn.execute(
                                "SELECT id FROM funai.img_monitoramento_consolidado_a ORDER BY id DESC LIMIT 1")
                            last_inserted_id = conn.fetchone().id

                            conn.execute(
                                "INSERT INTO funai.tb_interprete_monitoramento_consolidado(tb_interprete_monitora_id, img_monitoramento_consolidado_a_id) VALUES(%s, %s)",
                                [tb_interprete_monitora_id, last_inserted_id])

                            app.logger.info("INSERT: %s - %s - %s - %s - %s - %s - %s - %s",
                                            rs.tb_ciclo_monitoramento_id, rs.no_estagio, rs.no_imagem,
                                            rs.dt_imagem, rs.nu_orbita, rs.nu_ponto, rs.dt_t_zero, rs.dt_t_um)

                        except (Exception, ValueError, psycopg2.DataError) as e:
                            conn.execute("ROLLBACK")
                            app.logger.error("ROLLBACK")

                            app.logger.error(
                                "__error_insert_img_monitoramento_consolidado_a__")
                            app.logger.exception("error: %s", e)
                            data['error'] = str(e)
                            booTransacao = 1

                    conn.execute("COMMIT")
                    app.logger.info("COMMIT")

        if booTransacao == 0:
            for rs in resultSet:
                conn.execute("BEGIN")
                app.logger.info("BEGIN 2")
                try:
                    conn.execute("DELETE FROM {0}".format(rs.no_tabela))
                    conn.execute(
                        "ALTER SEQUENCE IF EXISTS {0}_id_seq RESTART WITH 1".format(rs.no_tabela))

                    conn.execute("COMMIT")
                    app.logger.info("COMMIT 2")

                except (Exception, ValueError, psycopg2.DataError) as e:
                    conn.execute("ROLLBACK")
                    app.logger.error("ROLLBACK 2")

                    app.logger.error("__error_delete_alter_a__")
                    app.logger.exception("error: %s", e)
                    data['error'] = str(e)
                    booTransacao = 1

        if booTransacao:
            data['message'] = 'Ocorreu um erro ao atualizar os dados do monitoramento consolidado. Favor verificar!'
            data['status'] = 0
        else:
            sincronizar_tabela_monitoramento_alexandre()
            sincronizar_tabela_monitoramento_aryanne()
            # app.logger.info("__inicio_atualizando_visao_materializada__")
            # executa o REFRESH nas visoes
            # conn.execute("SELECT funai.atualizar_visoes_diaria()")
            # app.logger.info("__fim_atualizando_visao_materializada__")

    db2.close()
    app.logger.info("Fecha conexao")

    app.logger.info(data)

    return jsonify(data)


@app.route("/radarsat2-trimestral")
@login_required
def radarsat2_trimestral():
    data = []
    entrega_trimestral = TbEntregaTrimestralRadarSat2.query.filter_by(st_finalizado=True).order_by(
        TbEntregaTrimestralRadarSat2.nu_entrega.desc()).all()

    if entrega_trimestral:
        for rs in entrega_trimestral:
            data.append({
                'nu_entrega': rs.nu_entrega,
                'dt_entrega': rs.dt_entrega.strftime('%d/%m/%Y'),
                'st_finalizado': rs.st_finalizado,
                'qt_total_registro_alerta': rs.qt_total_registro_alerta,
                'qt_total_registro_agua': rs.qt_total_registro_agua,
                'dt_cadastro': rs.dt_cadastro.strftime('%d/%m/%Y às %H:%M:%S')
            })

    return render_template('radarsat2_trimestral.html', entrega_trimestral=data)


@app.route("/nova-entrega-radarsat2-trimestral", methods=['POST'])
@login_required
def nova_entrega_radarsat2_trimestral():
    data = {
        'message': '',
        'status': 1,
        'error': ''
    }

    file = request.files['filezip']
    dt_entrega = request.form['dt_entrega'].split('/')
    dt_entrega = "%s-%s-%s" % (dt_entrega[2], dt_entrega[1], dt_entrega[0])
    directory_temporary = join(app.config['UPLOAD_FOLDER'], 'trimestral')

    if exists(directory_temporary):
        Path(directory_temporary).rmtree()

    if not exists(directory_temporary):
        mkdir(directory_temporary)

    try:
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filename = join(directory_temporary, filename)

            if exists(filename):
                Path(filename)

            file.save(filename)

            descompactar(filename, directory_temporary)
            mover_arquivos(directory_temporary)
    except Exception as e:
        data['message'] = 'Error'
        data['status'] = 0
        data['error'] = e
    finally:
        if exists(filename):
            Path(filename).rmtree()
            data = processar_dados_trimestral(dt_entrega, directory_temporary)

    return jsonify(data)


@app.route("/radarsat2-mensal")
@login_required
def radarsat2_mensal():
    data = []
    entrega_mensal = VisaoEntregaMensalDadosRadarSat2.query.filter_by(st_finalizado=True).order_by(
        VisaoEntregaMensalDadosRadarSat2.nu_entrega.desc()).all()

    if entrega_mensal:
        for rs in entrega_mensal:
            data.append({
                'id': rs.id,
                'nu_entrega': rs.nu_entrega,
                'dt_entrega': rs.dt_entrega,
                'st_finalizado': rs.st_finalizado,
                'qt_total_registro': rs.qt_total_registro,
                'dt_cadastro': rs.dt_cadastro
            })

    return render_template('radarsat2_mensal.html', entrega_mensal=data)


@app.route("/nova-entrega-radarsat2-mensal", methods=['POST'])
@login_required
def nova_entrega_radarsat2_mensal():
    data = {
        'message': '',
        'status': 1,
        'error': ''
    }

    file = request.files['filezip']
    dt_entrega = request.form['dt_entrega'].split('/')
    dt_entrega = "%s-%s-%s" % (dt_entrega[2], dt_entrega[1], dt_entrega[0])
    directory_temporary = join(app.config['UPLOAD_FOLDER'], 'mensal')

    if exists(directory_temporary):
        Path(directory_temporary).rmtree()

    if not exists(directory_temporary):
        mkdir(directory_temporary)

    try:
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filename = join(directory_temporary, filename)

            if exists(filename):
                Path(filename)

            file.save(filename)

            descompactar(filename, directory_temporary)
            mover_arquivos(directory_temporary)
    except Exception as e:
        data['message'] = 'Error'
        data['status'] = 0
        data['error'] = e
    finally:
        if exists(filename):
            Path(filename).rmtree()
            data = processar_dados_mensal(
                dt_entrega, directory_temporary, nu_entrega=None)

    return jsonify(data)


@app.route("/relatorio-mensal-radarsat2/<int:nu_entrega>/<path:dt_entrega>")
@login_required
def visualizar_relatorio_mensal_radarsat2(nu_entrega, dt_entrega):
    return render_template("radarsat2_relatorio_mensal.html", nu_entrega=nu_entrega, dt_entrega=dt_entrega)


@app.route("/pesquisar-relatorio-mensal-radarsat2", methods=['POST'])
@login_required
def pesquisar_relatorio_mensal_radarsat2():
    json = []
    jsonEntrega = []

    params = {
        'nu_entrega': request.form['nu_entrega'],
        'ds_aba': request.form['ds_aba'],
    }

    # pesquisa por deteccao-por-frame
    if 'deteccao-por-frame' == params['ds_aba']:

        SQL = "nu_entrega, " \
              "nu_frame, " \
              "dt_t_zero, " \
              "dt_t_um, " \
              "SUM(frame_nu_area_ha) AS frame_nu_area_ha, " \
              "SUM(corte_raso_nu_area_ha) AS cr_nu_area_ha, " \
              "SUM(corte_seletivo_nu_area_ha) AS cs_nu_area_ha, " \
              "SUM(nova_infraestrutura_nu_area_ha) AS ni_nu_area_ha, " \
              "SUM(nova_estrada_nu_area_ha) AS ne_nu_area_ha, " \
              "SUM(seletivo_natural_nu_area_ha) AS sn_nu_area_ha, " \
              "SUM(nu_area_total) AS nu_area_total " \
              "FROM " \
              "funai.vwm_painel_radarsat2_belo_monte_frame " \
              "WHERE " \
              "nu_entrega = %s " \
              "GROUP BY " \
              "ROLLUP((nu_entrega,nu_frame,dt_t_zero,dt_t_um)) " \
              "ORDER BY " \
              "nu_area_total DESC" % (params['nu_entrega'])

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 0:
            for rs in data:
                json.append({
                    'nu_entrega': rs.nu_entrega,
                    'nu_frame': rs.nu_frame,
                    'dt_t_zero': rs.dt_t_zero,
                    'dt_t_um': rs.dt_t_um,
                    'frame_nu_area_ha': float(Decimal(rs.frame_nu_area_ha).to_eng_string()),
                    'cr_nu_area_ha': float(Decimal(rs.cr_nu_area_ha).to_eng_string()),
                    'cs_nu_area_ha': float(Decimal(rs.cs_nu_area_ha).to_eng_string()),
                    'ni_nu_area_ha': float(Decimal(rs.ni_nu_area_ha).to_eng_string()),
                    'ne_nu_area_ha': float(Decimal(rs.ne_nu_area_ha).to_eng_string()),
                    'sn_nu_area_ha': float(Decimal(rs.sn_nu_area_ha).to_eng_string()),
                    'nu_area_total': float(Decimal(rs.nu_area_total).to_eng_string())
                })

    # pesquisa por deteccao-por-terra-indigena
    elif 'deteccao-por-terra-indigena' == params['ds_aba']:

        SQL = "co_funai, " \
              "no_ti, " \
              "nu_entrega, " \
              "SUM(corte_raso_nu_area_ha) AS cr_nu_area_ha, " \
              "SUM(corte_seletivo_nu_area_ha) AS cs_nu_area_ha, " \
              "SUM(nova_infraestrutura_nu_area_ha) AS ni_nu_area_ha, " \
              "SUM(nova_estrada_nu_area_ha) AS ne_nu_area_ha, " \
              "SUM(seletivo_natural_nu_area_ha) AS sn_nu_area_ha, " \
              "SUM(nu_area_total) AS nu_area_total " \
              "FROM " \
              "funai.vwm_painel_radarsat2_belo_monte_terra_indigena " \
              "WHERE " \
              "nu_entrega = %s " \
              "GROUP BY " \
              "ROLLUP((co_funai,no_ti,nu_entrega)) " \
              "ORDER BY " \
              "nu_area_total DESC" % (params['nu_entrega'])

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 0:
            for rs in data:
                json.append({
                    'co_funai': rs.co_funai,
                    'no_ti': rs.no_ti,
                    'nu_entrega': rs.nu_entrega,
                    'cr_nu_area_ha': float(Decimal(rs.cr_nu_area_ha).to_eng_string()),
                    'cs_nu_area_ha': float(Decimal(rs.cs_nu_area_ha).to_eng_string()),
                    'ni_nu_area_ha': float(Decimal(rs.ni_nu_area_ha).to_eng_string()),
                    'ne_nu_area_ha': float(Decimal(rs.ne_nu_area_ha).to_eng_string()),
                    'sn_nu_area_ha': float(Decimal(rs.sn_nu_area_ha).to_eng_string()),
                    'nu_area_total': float(Decimal(rs.nu_area_total).to_eng_string())
                })

    # pesquisa por deteccao-por-municipio
    elif 'deteccao-por-municipio' == params['ds_aba']:

        SQL = "nu_geocodigo, " \
              "no_municipio, " \
              "sg_uf, " \
              "nu_entrega, " \
              "SUM(corte_raso_nu_area_ha) AS cr_nu_area_ha, " \
              "SUM(corte_seletivo_nu_area_ha) AS cs_nu_area_ha, " \
              "SUM(nova_infraestrutura_nu_area_ha) AS ni_nu_area_ha, " \
              "SUM(nova_estrada_nu_area_ha) AS ne_nu_area_ha, " \
              "SUM(seletivo_natural_nu_area_ha) AS sn_nu_area_ha, " \
              "SUM(nu_area_total) AS nu_area_total " \
              "FROM " \
              "funai.vwm_painel_radarsat2_belo_monte_municipio " \
              "WHERE " \
              "nu_entrega = %s " \
              "GROUP BY " \
              "ROLLUP((nu_geocodigo,no_municipio,sg_uf,nu_entrega)) " \
              "ORDER BY " \
              "nu_area_total DESC" % (params['nu_entrega'])

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 0:
            for rs in data:
                json.append({
                    'nu_geocodigo': rs.nu_geocodigo,
                    'no_municipio': rs.no_municipio,
                    'sg_uf': rs.sg_uf,
                    'nu_entrega': rs.nu_entrega,
                    'cr_nu_area_ha': float(Decimal(rs.cr_nu_area_ha).to_eng_string()),
                    'cs_nu_area_ha': float(Decimal(rs.cs_nu_area_ha).to_eng_string()),
                    'ni_nu_area_ha': float(Decimal(rs.ni_nu_area_ha).to_eng_string()),
                    'ne_nu_area_ha': float(Decimal(rs.ne_nu_area_ha).to_eng_string()),
                    'sn_nu_area_ha': float(Decimal(rs.sn_nu_area_ha).to_eng_string()),
                    'nu_area_total': float(Decimal(rs.nu_area_total).to_eng_string())
                })

    elif 'deteccao-por-terra-indigena-municipio' == params[
            'ds_aba']:  # pesquisa por deteccao-por-terra-indigena-municipio

        SQL = "co_funai, " \
              "no_ti, " \
              "ds_fase_ti, " \
              "nu_geocodigo, " \
              "no_municipio, " \
              "nu_entrega, " \
              "SUM(nu_area_municipio_ha) AS nu_area_municipio_ha, " \
              "SUM(nu_area_ti_no_municipio_ha) AS nu_area_ti_no_municipio_ha, " \
              "SUM(nu_percentual_ti_no_municipio) AS nu_percentual_ti_no_municipio, " \
              "SUM(corte_raso_nu_area_ha) AS cr_nu_area_ha, " \
              "SUM(corte_seletivo_nu_area_ha) AS cs_nu_area_ha, " \
              "SUM(nova_infraestrutura_nu_area_ha) AS ni_nu_area_ha, " \
              "SUM(nova_estrada_nu_area_ha) AS ne_nu_area_ha, " \
              "SUM(seletivo_natural_nu_area_ha) AS sn_nu_area_ha, " \
              "SUM(nu_area_total) AS nu_area_total " \
              "FROM " \
              "funai.vwm_painel_radarsat2_belo_monte_ti_municipio " \
              "WHERE " \
              "nu_entrega = %s " \
              "GROUP BY " \
              "ROLLUP((co_funai,no_ti,ds_fase_ti,nu_geocodigo,no_municipio,nu_entrega)) " \
              "ORDER BY " \
              "no_ti" % (params['nu_entrega'])

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 0:
            for rs in data:
                json.append({
                    'co_funai': rs.co_funai,
                    'no_ti': rs.no_ti,
                    'ds_fase_ti': rs.ds_fase_ti,
                    'nu_geocodigo': rs.nu_geocodigo,
                    'no_municipio': rs.no_municipio,
                    'nu_entrega': rs.nu_entrega,
                    'nu_area_municipio_ha': float(Decimal(rs.nu_area_municipio_ha).to_eng_string()),
                    'nu_area_ti_no_municipio_ha': float(Decimal(rs.nu_area_ti_no_municipio_ha).to_eng_string()),
                    'nu_percentual_ti_no_municipio': float(Decimal(rs.nu_percentual_ti_no_municipio).to_eng_string()),
                    'cr_nu_area_ha': float(Decimal(rs.cr_nu_area_ha).to_eng_string()),
                    'cs_nu_area_ha': float(Decimal(rs.cs_nu_area_ha).to_eng_string()),
                    'ni_nu_area_ha': float(Decimal(rs.ni_nu_area_ha).to_eng_string()),
                    'ne_nu_area_ha': float(Decimal(rs.ne_nu_area_ha).to_eng_string()),
                    'sn_nu_area_ha': float(Decimal(rs.sn_nu_area_ha).to_eng_string()),
                    'nu_area_total': float(Decimal(rs.nu_area_total).to_eng_string())
                })

    return jsonify({'data': json, 'entrega': jsonEntrega})


@app.route("/tabela-alterar-comentario", methods=["POST"])
@app.route("/tabela-alterar-comentario/<int:id>", methods=["GET"])
@login_required
def tabela_alterar_comentario(id=0):
    if request.method == "POST":
        no_tabela = str(request.form["no_tabela"])
        ds_comentario = request.form["ds_comentario"]

        if len(no_tabela) > 0:
            try:
                query = "COMMENT ON TABLE %s IS '%s';" % (
                    no_tabela, ds_comentario.strip())

                db2 = psycopg2.connect(
                    app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
                conn = db2.cursor()
                conn.execute(query)
                db2.commit()
                db2.close()

                message = "O comentário da tabela: %s, foi alterado com sucesso." % no_tabela

                return jsonify({"status": "ok", "message": message})
            except Exception as e:
                app.logger.exception(e)
                message = "Ocorreu um erro ao tentar alterar o comentário da tabela: %s. Favor verificar!" % no_tabela
                return jsonify({"status": "error", "message": message})
            finally:
                db22 = psycopg2.connect(
                    app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
                conn1 = db22.cursor()
                conn1.execute(
                    "REFRESH MATERIALIZED VIEW CONCURRENTLY aplicacao.vwm_tabela_informacao_atualizacao")
                db22.commit()
                db22.close()

        else:
            redirect(url_for("tabela_informacao_atualizacao"))

    else:

        if id == 0:
            redirect(url_for("tabela_informacao_atualizacao"))
        else:
            tabela_informacao_aplicacao = VisaoTabelaInformacaoAtualizacao.query.get(
                id)
            return render_template("tabela_alterar_comentario.html", data=tabela_informacao_aplicacao)


@app.route("/tabela-informacao-atualizacao")
@login_required
def tabela_informacao_atualizacao():
    tabela_informacao_aplicacao = VisaoTabelaInformacaoAtualizacao.query.order_by(
        VisaoTabelaInformacaoAtualizacao.dt_cadastro.desc()).all()
    return render_template("tabela_informacao_atualizacao.html",
                           tabela_informacao_aplicacao=tabela_informacao_aplicacao)


@app.route("/pesquisar-relatorio-fitro", methods=['POST'])
@login_required
def pesquisar_relatorio_por_fitro():
    json = []

    params = {
        'tp_filtro': request.form['tp_filtro'],
        'dt_inicial': "%s-%s-%s" % (request.form['dt_inicial'].split("/")[2], request.form['dt_inicial'].split("/")[1],
                                    request.form['dt_inicial'].split("/")[0]),
        'dt_final': "%s-%s-%s" % (request.form['dt_final'].split("/")[2], request.form['dt_final'].split("/")[1],
                                  request.form['dt_final'].split("/")[0]),
        'op_ti_flt_ti': str(request.form.getlist('op_ti_flt_ti[]')).replace('[', '').replace(']', '').replace('u',
                                                                                                              '').replace(
            ', ', ','),
        'op_ti_flt_cr': str(request.form.getlist('op_ti_flt_cr[]')).replace('[', '').replace(']', '').replace('u',
                                                                                                              '').replace(
            ', ', ','),
        'op_ti_flt_uf': str(request.form.getlist('op_ti_flt_uf[]')).replace('[', '').replace(']', '').replace('u',
                                                                                                              '').replace(
            ', ', ','),
        'op_cr_flt_cr': str(request.form.getlist('op_cr_flt_cr[]')).replace('[', '').replace(']', '').replace('u',
                                                                                                              '').replace(
            ', ', ','),
        'op_uf_flt_uf': str(request.form.getlist('op_uf_flt_uf[]')).replace('[', '').replace(']', '').replace('u',
                                                                                                              '').replace(
            ', ', ',')
    }

    if 'ti' in params['tp_filtro']:  # pesquisa por ti

        SQL1 = "co_funai, " \
               "no_ti, " \
               "ds_cr, " \
               "COALESCE(sg_uf,'') AS sg_uf, " \
               "COALESCE(SUM(cr_nu_area_ha),'0.00') AS cr_nu_area_ha, " \
               "COALESCE(SUM(dg_nu_area_ha),'0.00') AS dg_nu_area_ha, " \
               "COALESCE(SUM(dr_nu_area_ha),'0.00') AS dr_nu_area_ha, " \
               "COALESCE(SUM(ff_nu_area_ha),'0.00') AS ff_nu_area_ha, " \
               "COALESCE(SUM(total_nu_area_ha),'0.00') AS total_nu_area_ha " \
               "FROM " \
               "aviso.vwm_relatorio_monitora_aviso_diario_por_terra_indigena " \
               "WHERE " \
               "dt_t_um BETWEEN '%s' AND '%s' " % (
                   params['dt_inicial'], params['dt_final'])

        SQLP1 = ''
        if len(params['op_ti_flt_ti']) > 0:
            SQLP1 = "AND co_funai::varchar IN (%s) " % (params['op_ti_flt_ti'])

        SQLP2 = ''
        if len(params['op_ti_flt_cr']) > 0:
            SQLP2 = "AND id_cr::varchar IN (%s) " % (params['op_ti_flt_cr'])

        SQLP3 = ''
        if len(params['op_ti_flt_uf']) > 0:
            SQLP3 = "AND id_uf::varchar IN (%s) " % (params['op_ti_flt_uf'])

        SQL2 = "GROUP BY ROLLUP ((co_funai,no_ti,ds_cr,sg_uf)) " \
               "ORDER BY no_ti"

        SQL = SQL1 + SQLP1 + SQLP2 + SQLP3 + SQL2

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 1:
            for rs in data:
                json.append({
                    'co_funai': rs.co_funai,
                    'no_ti': rs.no_ti,
                    'sg_uf': rs.sg_uf,
                    'ds_cr': rs.ds_cr,
                    'cr_nu_area_ha': float(Decimal(rs.cr_nu_area_ha).to_eng_string()),
                    'dg_nu_area_ha': float(Decimal(rs.dg_nu_area_ha).to_eng_string()),
                    'dr_nu_area_ha': float(Decimal(rs.dr_nu_area_ha).to_eng_string()),
                    'ff_nu_area_ha': float(Decimal(rs.ff_nu_area_ha).to_eng_string()),
                    'total_nu_area_ha': float(Decimal(rs.total_nu_area_ha).to_eng_string())
                })

    elif 'cr' in params['tp_filtro']:  # pesquisa por cr

        SQL1 = "ds_cr, " \
               "COALESCE(SUM(cr_nu_area_ha),'0.00') AS cr_nu_area_ha, " \
               "COALESCE(SUM(dg_nu_area_ha),'0.00') AS dg_nu_area_ha, " \
               "COALESCE(SUM(dr_nu_area_ha),'0.00') AS dr_nu_area_ha, " \
               "COALESCE(SUM(ff_nu_area_ha),'0.00') AS ff_nu_area_ha, " \
               "COALESCE(SUM(total_nu_area_ha),'0.00') AS total_nu_area_ha " \
               "FROM " \
               "aviso.vwm_relatorio_monitora_aviso_diario_por_ti_coordenacao_regional " \
               "WHERE " \
               "dt_t_um BETWEEN '%s' AND '%s' " % (
                   params['dt_inicial'], params['dt_final'])

        SQLP1 = ''
        if len(params['op_cr_flt_cr']) > 0:
            SQLP1 = "AND id_cr::varchar IN (%s) " % (params['op_cr_flt_cr'])

        SQL2 = "GROUP BY ROLLUP ((ds_cr)) " \
               "ORDER BY ds_cr"

        SQL = SQL1 + SQLP1 + SQL2

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 1:
            for rs in data:
                json.append({
                    'ds_cr': rs.ds_cr,
                    'cr_nu_area_ha': float(Decimal(rs.cr_nu_area_ha).to_eng_string()),
                    'dg_nu_area_ha': float(Decimal(rs.dg_nu_area_ha).to_eng_string()),
                    'dr_nu_area_ha': float(Decimal(rs.dr_nu_area_ha).to_eng_string()),
                    'ff_nu_area_ha': float(Decimal(rs.ff_nu_area_ha).to_eng_string()),
                    'total_nu_area_ha': float(Decimal(rs.total_nu_area_ha).to_eng_string())
                })

    elif 'uf' in params['tp_filtro']:  # pesquisa por df

        SQL1 = "no_uf, " \
               "sg_uf, " \
               "COALESCE(SUM(cr_nu_area_ha),'0.00') AS cr_nu_area_ha, " \
               "COALESCE(SUM(dg_nu_area_ha),'0.00') AS dg_nu_area_ha, " \
               "COALESCE(SUM(dr_nu_area_ha),'0.00') AS dr_nu_area_ha, " \
               "COALESCE(SUM(ff_nu_area_ha),'0.00') AS ff_nu_area_ha, " \
               "COALESCE(SUM(total_nu_area_ha),'0.00') AS total_nu_area_ha " \
               "FROM " \
               "aviso.vwm_relatorio_monitora_aviso_diario_por_uf " \
               "WHERE " \
               "dt_t_um BETWEEN '%s' AND '%s' " % (
                   params['dt_inicial'], params['dt_final'])

        SQLP1 = ''
        if len(params['op_uf_flt_uf']) > 0:
            SQLP1 = "AND sg_uf::varchar IN (%s) " % (params['op_uf_flt_uf'])

        SQL2 = "GROUP BY ROLLUP ((no_uf,sg_uf)) " \
               "ORDER BY no_uf"

        SQL = SQL1 + SQLP1 + SQL2

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 1:
            for rs in data:
                json.append({
                    'no_uf': rs.no_uf,
                    'sg_uf': rs.sg_uf,
                    'cr_nu_area_ha': float(Decimal(rs.cr_nu_area_ha).to_eng_string()),
                    'dg_nu_area_ha': float(Decimal(rs.dg_nu_area_ha).to_eng_string()),
                    'dr_nu_area_ha': float(Decimal(rs.dr_nu_area_ha).to_eng_string()),
                    'ff_nu_area_ha': float(Decimal(rs.ff_nu_area_ha).to_eng_string()),
                    'total_nu_area_ha': float(Decimal(rs.total_nu_area_ha).to_eng_string())
                })

    return jsonify({'data': json})


@app.route("/mascara-antropismo")
@login_required
def mascara_antropismo():
    return render_template('mascara_antropismo.html')


@app.route("/pesquisar-relatorio-mascara", methods=['POST'])
@login_required
def pesquisar_relatorio_mascara():
    json = []

    params = {
        'tp_filtro': request.form['tp_filtro']
    }

    if 'ti' in params['tp_filtro']:  # pesquisa por ti

        SQL = "co_funai, " \
              "no_ti, " \
              "SUM(ti_total_nu_area_ha) AS ti_total_nu_area_ha, " \
              "SUM(mascara_nu_area_ha) AS mascara_nu_area_ha, " \
              "SUM(nu_area_vegetacao_natural_ha) AS nu_area_vegetacao_natural_ha " \
              "FROM " \
              "funai.vwm_mascara_antropismo_por_terra_indigena " \
              "GROUP BY ROLLUP ((co_funai,no_ti)) " \
              "ORDER BY no_ti"

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 1:
            for rs in data:
                json.append({
                    'co_funai': rs.co_funai,
                    'no_ti': rs.no_ti,
                    'ti_total_nu_area_ha': float(Decimal(rs.ti_total_nu_area_ha).to_eng_string()),
                    'mascara_nu_area_ha': float(Decimal(rs.mascara_nu_area_ha).to_eng_string()),
                    'nu_area_vegetacao_natural_ha': float(Decimal(rs.nu_area_vegetacao_natural_ha).to_eng_string())
                })

    elif 'cr' in params['tp_filtro']:  # pesquisa por cr

        SQL = "ds_cr, " \
              "SUM(nu_area_ha) AS nu_area_ha, " \
              "SUM(nu_area_km2) AS nu_area_km2 " \
              "FROM " \
              "funai.vwm_mascara_antropismo_por_ti_coordenacao_regional " \
              "GROUP BY ROLLUP ((ds_cr)) " \
              "ORDER BY ds_cr"

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 1:
            for rs in data:
                json.append({
                    'ds_cr': rs.ds_cr,
                    'nu_area_ha': float(Decimal(rs.nu_area_ha).to_eng_string()),
                    'nu_area_km2': float(Decimal(rs.nu_area_km2).to_eng_string())
                })

    elif 'uf' in params['tp_filtro']:  # pesquisa por df

        SQL = "no_uf, " \
              "sg_uf, " \
              "SUM(nu_area_ha) AS nu_area_ha, " \
              "SUM(nu_area_km2) AS nu_area_km2 " \
              "FROM " \
              "funai.vwm_mascara_antropismo_por_uf " \
              "GROUP BY ROLLUP ((no_uf,sg_uf)) " \
              "ORDER BY no_uf"

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 1:
            for rs in data:
                json.append({
                    'no_uf': rs.no_uf,
                    'sg_uf': rs.sg_uf,
                    'nu_area_ha': float(Decimal(rs.nu_area_ha).to_eng_string()),
                    'nu_area_km2': float(Decimal(rs.nu_area_km2).to_eng_string())
                })

    return jsonify({'data': json})


@app.route("/gerar-aviso-diario")
@login_required
def gerar_aviso_diario():
    template_aviso = {}

    ciclos_monitorados = VisaoCicloMonitoramentoConsolidado.query.filter(
        and_(VisaoCicloMonitoramentoConsolidado.id >= 7, VisaoCicloMonitoramentoConsolidado.id != 19)).all()
    dataEntregaAviso = VisaoEntregaAvisoDiario.query.all()
    dataAbaEntregaAviso = VisaoEntregaAvisoDiarioAbaAno.query.all()
    maxEntregaAviso = VisaoEntregaAvisoDiarioNuAvisoMaximo.query.first()

    data = TemplateRelatorio.query.filter_by(tp_relatorio=1).all()
    for r in data:
        template_aviso[r.no_chave] = r.ds_valor

    return render_template("gerar_aviso_diario.html", template_aviso=template_aviso, entrega_aviso=dataEntregaAviso,
                           ciclos_monitorados=ciclos_monitorados, ultimo_nu_aviso=maxEntregaAviso.nu_aviso,
                           aba_entrega_ano=dataAbaEntregaAviso)


@app.route("/visualizar-entrega-aviso-diario/<int:id>")
@app.route("/visualizar-entrega-aviso-diario/<int:id>/<tp>")
@login_required
def visualizar_entrega_aviso_diario(id, tp='html'):
    template_aviso = {}
    js_terra_indigena = {}
    js_coordenacao_regional = {}
    js_estado = {}
    js_imagens = {}

    base_cartografica = VisaoBaseCartograficaAvisoDiario.query.all()

    entregaAvisoDiario = VisaoEntregaAvisoDiario.query.get(id)

    if entregaAvisoDiario.js_terra_indigena is not None:
        js_terra_indigena = entregaAvisoDiario.js_terra_indigena

    if entregaAvisoDiario.js_coordenacao_regional is not None:
        js_coordenacao_regional = entregaAvisoDiario.js_coordenacao_regional

    if entregaAvisoDiario.js_estado is not None:
        js_estado = entregaAvisoDiario.js_estado
        js_coordenacao_regional = entregaAvisoDiario.js_coordenacao_regional

    if entregaAvisoDiario.js_imagens is not None:
        js_imagens = entregaAvisoDiario.js_imagens

    data = TemplateRelatorio.query.filter_by(tp_relatorio=1).all()
    for r in data:
        template_aviso[r.no_chave] = r.ds_valor

    if len(js_imagens) > 0:
        nu_titulo_base_imagem = 4
        nu_titulo_grade_landsat = 5
        nu_titulo_disponibilizacao_dados = 6
        nu_tabela_base_imagem = 6
        nu_tabela_disponibilizacao_dados = 7
        template_aviso['5_disponibilizacao_dos_dados_tabela_6'] = str(
            template_aviso['5_disponibilizacao_dos_dados_tabela_6']).replace('Tabela 6.',
                                                                             'Tabela %s.' % nu_tabela_disponibilizacao_dados)
    else:
        nu_titulo_base_imagem = 0
        nu_titulo_grade_landsat = 4
        nu_titulo_disponibilizacao_dados = 5
        nu_tabela_base_imagem = 0
        nu_tabela_disponibilizacao_dados = 6
        template_aviso['5_disponibilizacao_dos_dados_tabela_6'] = str(
            template_aviso['5_disponibilizacao_dos_dados_tabela_6']).replace('Tabela 6.',
                                                                             'Tabela %s.' % nu_tabela_disponibilizacao_dados)

    if tp == 'html':
        return render_template("visualizar_entrega_aviso_diario.html", tp=tp, template_aviso=template_aviso,
                               entrega_aviso_diario=entregaAvisoDiario,
                               dataTerraIndigena=js_terra_indigena,
                               dataCoordenacaoRegional=js_coordenacao_regional,
                               dataEstado=js_estado,
                               base_cartografica=base_cartografica,
                               dataImagem=js_imagens,
                               nu_titulo_base_imagem=nu_titulo_base_imagem,
                               nu_titulo_grade_landsat=nu_titulo_grade_landsat,
                               nu_titulo_disponibilizacao_dados=nu_titulo_disponibilizacao_dados,
                               nu_tabela_base_imagem=nu_tabela_base_imagem,
                               nu_tabela_disponibilizacao_dados=nu_tabela_disponibilizacao_dados)
    else:
        html = render_template("visualizar_entrega_aviso_diario.html", tp=tp, template_aviso=template_aviso,
                               entrega_aviso_diario=entregaAvisoDiario,
                               dataTerraIndigena=js_terra_indigena,
                               dataCoordenacaoRegional=js_coordenacao_regional,
                               dataEstado=js_estado,
                               base_cartografica=base_cartografica,
                               dataImagem=js_imagens,
                               nu_titulo_base_imagem=nu_titulo_base_imagem,
                               nu_titulo_grade_landsat=nu_titulo_grade_landsat,
                               nu_titulo_disponibilizacao_dados=nu_titulo_disponibilizacao_dados,
                               nu_tabela_base_imagem=nu_tabela_base_imagem,
                               nu_tabela_disponibilizacao_dados=nu_tabela_disponibilizacao_dados)
        return render_pdf(HTML(string=html), download_filename=entregaAvisoDiario.no_arquivo)


@app.route("/salvar-aviso-diario", methods=["POST"])
@login_required
def salvar_aviso_diario():
    tb_usuario_id = request.form["tb_usuario_id"]
    tb_ciclo_monitoramento_id = request.form["tb_ciclo_monitoramento_id"]
    nu_aviso = request.form["nu_aviso"]
    nu_orbita = str(request.form.getlist('nu_orbita')).replace('[', '').replace(']', '').replace('u',
                                                                                                 '').replace(
        ', ', ',')
    st_situacao = request.form["st_situacao"]

    try:
        entregaAviso = EntregaAvisoDiario(
            tb_usuario_id, tb_ciclo_monitoramento_id, nu_aviso, nu_orbita, st_situacao)
        db.session.add(entregaAviso)
        db.session.commit()

        entrega_id = db.session.query(func.max(EntregaAvisoDiario.id)).first()
        entrega = EntregaAvisoDiario.query.get(entrega_id)
        entrega.js_terra_indigena = json_aviso_diario_terra_indigena(
            entrega_id)
        entrega.js_coordenacao_regional = json_aviso_diario_coordenacao_regional(
            entrega_id)
        entrega.js_estado = json_aviso_diario_estado(entrega_id)
        entrega.js_imagens = json_aviso_diario_imagens(entrega_id)
        db.session.add(entrega)
        db.session.commit()

        return jsonify({"status": "ok", "message": "Aviso Diário cadastrado com sucesso."})
    except Exception as e:
        app.logger.exception(e)
        db.session.rollback()
        return jsonify({"status": "error", "message": "Ocorreu um erro ao cadastrar os dados."})


@app.route("/excluir-aviso-diario", methods=["POST"])
@login_required
def excluir_aviso_diario():
    id = request.form["id"]

    try:
        entregaAviso = EntregaAvisoDiario.query.get(id)
        db.session.delete(entregaAviso)
        db.session.commit()
        return jsonify({"status": "ok", "message": "Aviso Gerado foi excluído com sucesso."})
    except Exception as e:
        app.logger.exception(e)
        db.session.rollback()
        return jsonify({"status": "error", "message": "Ocorreu um erro ao excluir o registro."})


@app.route("/visualizar-entrega-mensal-radarsat2/<int:id>")
@app.route("/visualizar-entrega-mensal-radarsat2/<int:id>/<tp>")
@login_required
def visualizar_entrega_mensal_radarsat2(id, tp='html'):
    template_aviso = {}
    json_regime_de_uso = {'interior_terra_indigena': {},
                          'zona_contorno': {}, 'area_total': {}}
    json_regime_de_uso['interior_terra_indigena']['descricao'] = 'Interior da Terra Indígena'
    json_regime_de_uso['zona_contorno']['descricao'] = 'Zona de contorno (500 m)'
    json_regime_de_uso['area_total']['descricao'] = 'Área Total'

    entregaMensalRadarSat2 = VisaoEntregaMensalDadosRadarSat2.query.get(id)
    nu_entrega = entregaMensalRadarSat2.nu_entrega

    json_dados_frame = json_mensal_radarsat2_por_frame(nu_entrega)
    json_dados_municipio = json_mensal_radarsat2_por_municipio(nu_entrega)
    json_dados_terra_indigena_municipio = json_mensal_radarsat2_por_terra_indigena_municipio(
        nu_entrega)
    json_dados_terra_indigena_municipio_by_nome = json_mensal_radarsat2_por_terra_indigena_municipio_by_nome(
        nu_entrega)

    for r in json_dados_terra_indigena_municipio:
        if r['co_funai'] is None:
            json_regime_de_uso['interior_terra_indigena']['cr_nu_area_ha'] = [r['cr_nu_area_ha'], '0.00'][
                r['cr_nu_area_ha'] == '-']
            json_regime_de_uso['interior_terra_indigena']['cs_nu_area_ha'] = [r['cs_nu_area_ha'], '0.00'][
                r['cs_nu_area_ha'] == '-']
            json_regime_de_uso['interior_terra_indigena']['ni_nu_area_ha'] = [r['ni_nu_area_ha'], '0.00'][
                r['ni_nu_area_ha'] == '-']
            json_regime_de_uso['interior_terra_indigena']['ne_nu_area_ha'] = [r['ne_nu_area_ha'], '0.00'][
                r['ne_nu_area_ha'] == '-']
            json_regime_de_uso['interior_terra_indigena']['sn_nu_area_ha'] = [r['sn_nu_area_ha'], '0.00'][
                r['sn_nu_area_ha'] == '-']
            json_regime_de_uso['interior_terra_indigena']['nu_area_total'] = [r['nu_area_total'], '0.00'][
                r['nu_area_total'] == '-']

    for r in json_dados_municipio:
        if r['nu_geocodigo'] is None:
            json_regime_de_uso['area_total']['cr_nu_area_ha'] = [
                r['cr_nu_area_ha'], '0.00'][r['cr_nu_area_ha'] == '-']
            json_regime_de_uso['area_total']['cs_nu_area_ha'] = [
                r['cs_nu_area_ha'], '0.00'][r['cs_nu_area_ha'] == '-']
            json_regime_de_uso['area_total']['ni_nu_area_ha'] = [
                r['ni_nu_area_ha'], '0.00'][r['ni_nu_area_ha'] == '-']
            json_regime_de_uso['area_total']['ne_nu_area_ha'] = [
                r['ne_nu_area_ha'], '0.00'][r['ne_nu_area_ha'] == '-']
            json_regime_de_uso['area_total']['sn_nu_area_ha'] = [
                r['sn_nu_area_ha'], '0.00'][r['sn_nu_area_ha'] == '-']
            json_regime_de_uso['area_total']['nu_area_total'] = [
                r['nu_area_total'], '0.00'][r['nu_area_total'] == '-']

    _zona_contorno_cr_nu_area_ha = float(
        str(json_regime_de_uso['area_total']['cr_nu_area_ha']).replace('.', '').replace(',', '.')) - float(
        str(json_regime_de_uso['interior_terra_indigena']['cr_nu_area_ha']).replace('.', '').replace(',', '.'))

    _zona_contorno_cs_nu_area_ha = float(
        str(json_regime_de_uso['area_total']['cs_nu_area_ha']).replace('.', '').replace(',', '.')) - float(
        str(json_regime_de_uso['interior_terra_indigena']['cs_nu_area_ha']).replace('.', '').replace(',', '.'))

    _zona_contorno_ni_nu_area_ha = float(
        str(json_regime_de_uso['area_total']['ni_nu_area_ha']).replace('.', '').replace(',', '.')) - float(
        str(json_regime_de_uso['interior_terra_indigena']['ni_nu_area_ha']).replace('.', '').replace(',', '.'))

    _zona_contorno_ne_nu_area_ha = float(
        str(json_regime_de_uso['area_total']['ne_nu_area_ha']).replace('.', '').replace(',', '.')) - float(
        str(json_regime_de_uso['interior_terra_indigena']['ne_nu_area_ha']).replace('.', '').replace(',', '.'))

    _zona_contorno_sn_nu_area_ha = float(
        str(json_regime_de_uso['area_total']['sn_nu_area_ha']).replace('.', '').replace(',', '.')) - float(
        str(json_regime_de_uso['interior_terra_indigena']['sn_nu_area_ha']).replace('.', '').replace(',', '.'))

    _zona_contorno_nu_area_total = float(
        str(json_regime_de_uso['area_total']['nu_area_total']).replace('.', '').replace(',', '.')) - float(
        str(json_regime_de_uso['interior_terra_indigena']['nu_area_total']).replace('.', '').replace(',', '.'))

    json_regime_de_uso['zona_contorno']['cr_nu_area_ha'] = \
        [formatarValor(float(Decimal(_zona_contorno_cr_nu_area_ha).to_eng_string())), '-'][
            float(Decimal(_zona_contorno_cr_nu_area_ha).to_eng_string()) == 0.00 or float(
                Decimal(_zona_contorno_cr_nu_area_ha).to_eng_string()) < 0]

    json_regime_de_uso['zona_contorno']['cs_nu_area_ha'] = \
        [formatarValor(float(Decimal(_zona_contorno_cs_nu_area_ha).to_eng_string())), '-'][
            float(Decimal(_zona_contorno_cs_nu_area_ha).to_eng_string()) == 0.00 or float(
                Decimal(_zona_contorno_cs_nu_area_ha).to_eng_string()) < 0]

    json_regime_de_uso['zona_contorno']['ni_nu_area_ha'] = \
        [formatarValor(float(Decimal(_zona_contorno_ni_nu_area_ha).to_eng_string())), '-'][
            float(Decimal(_zona_contorno_ni_nu_area_ha).to_eng_string()) == 0.00 or float(
                Decimal(_zona_contorno_ni_nu_area_ha).to_eng_string()) < 0]

    json_regime_de_uso['zona_contorno']['ne_nu_area_ha'] = \
        [formatarValor(float(Decimal(_zona_contorno_ne_nu_area_ha).to_eng_string())), '-'][
            float(Decimal(_zona_contorno_ne_nu_area_ha).to_eng_string()) == 0.00 or float(
                Decimal(_zona_contorno_ne_nu_area_ha).to_eng_string()) < 0]

    json_regime_de_uso['zona_contorno']['sn_nu_area_ha'] = \
        [formatarValor(float(Decimal(_zona_contorno_sn_nu_area_ha).to_eng_string())), '-'][
            float(Decimal(_zona_contorno_sn_nu_area_ha).to_eng_string()) == 0.00 or float(
                Decimal(_zona_contorno_sn_nu_area_ha).to_eng_string()) < 0]

    json_regime_de_uso['zona_contorno']['nu_area_total'] = \
        [formatarValor(float(Decimal(_zona_contorno_nu_area_total).to_eng_string())), '-'][
            float(Decimal(_zona_contorno_nu_area_total).to_eng_string()) == 0.00 or float(
                Decimal(_zona_contorno_nu_area_total).to_eng_string()) < 0]

    data = TemplateRelatorio.query.filter_by(tp_relatorio=2).all()
    for r in data:
        template_aviso[r.no_chave] = r.ds_valor

    template_aviso['1_resumo_do_monitoramento'] = str(template_aviso['1_resumo_do_monitoramento']).replace(
        '{__nu_entrega__}', str(nu_entrega))

    template_aviso['1_sumario_estatistico'] = str(template_aviso['1_sumario_estatistico']).replace('{__nu_entrega__}',
                                                                                                   str(nu_entrega))

    if tp == 'html':
        return render_template("visualizar_entrega_mensal_radarsat2.html", tp=tp, template_aviso=template_aviso,
                               entrega_mensal_radarsat2=entregaMensalRadarSat2, dataFrame=json_dados_frame,
                               dataMunicipio=json_dados_municipio,
                               dataTerraIndigenaMunicipio=json_dados_terra_indigena_municipio,
                               dataNomeTIMunicipio=json_dados_terra_indigena_municipio_by_nome,
                               dataRegimeUso=json_regime_de_uso)
    else:
        html = render_template("visualizar_entrega_mensal_radarsat2.html", tp=tp, template_aviso=template_aviso,
                               entrega_mensal_radarsat2=entregaMensalRadarSat2, dataFrame=json_dados_frame,
                               dataMunicipio=json_dados_municipio,
                               dataTerraIndigenaMunicipio=json_dados_terra_indigena_municipio,
                               dataNomeTIMunicipio=json_dados_terra_indigena_municipio_by_nome,
                               dataRegimeUso=json_regime_de_uso)
        return render_pdf(HTML(string=html), download_filename=entregaMensalRadarSat2.no_arquivo)


@app.route("/aviso-diario/formulario", methods=["GET", "POST"])
@app.route("/aviso-diario/formulario/<int:id>", methods=["GET", "POST"])
@login_required
def aviso_diario_formulario(id):
    if request.method == "POST":
        id = request.form["id"]
        s_dt_disponibilizado_banco = request.form["dt_disponibilizado_banco"].split(
            "/")
        s_dt_disponibilizado_banco = "%s-%s-%s" % (
            s_dt_disponibilizado_banco[2], s_dt_disponibilizado_banco[1], s_dt_disponibilizado_banco[0])
        if len(id) > 0:
            try:
                entregaAvisoDiario = EntregaAvisoDiario.query.get(id)
                disponibilizado_banco = str(
                    entregaAvisoDiario.dt_disponibilizado_banco).split(" ")
                dt_disponibilizado_banco = "%s %s" % (
                    s_dt_disponibilizado_banco, disponibilizado_banco[1])
                entregaAvisoDiario.dt_disponibilizado_banco = dt_disponibilizado_banco
                db.session.add(entregaAvisoDiario)
                db.session.commit()
                return jsonify(
                    {"status": "ok", "message": "Aviso atualizado com sucesso!", "url": "/gerar-aviso-diario"})
            except Exception as e:
                app.logger.exception(e)
                db.session.rollback()
                return jsonify({"status": "error", "message": "Ocorreu um erro ao atualizar os dados."})
    else:
        visaoEntregaAvisoDiario = VisaoEntregaAvisoDiario.query.get(id)
        entregaAvisoDiario = EntregaAvisoDiario.query.get(id)
        return render_template("aviso_diario_formulario.html", entrega_aviso_diario=entregaAvisoDiario,
                               visao_entrega_aviso_diario=visaoEntregaAvisoDiario)


@app.route("/radarsat2-substituir-entrega/<int:id>", methods=["GET", "POST"])
@login_required
def radarsat2_substituir_entrega(id):
    if request.method == "POST":
        data = {
            'message': '',
            'status': 1,
            'error': ''
        }

        file = request.files['filezip']
        nu_entrega = request.form['nu_entrega']
        dt_entrega = request.form['dt_entrega'].split('/')
        dt_entrega = "%s-%s-%s" % (dt_entrega[2], dt_entrega[1], dt_entrega[0])
        directory_temporary = join(app.config['UPLOAD_FOLDER'], 'mensal')

        if exists(directory_temporary):
            Path(directory_temporary).rmtree()

        if not exists(directory_temporary):
            mkdir(directory_temporary)

        try:
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                filename = join(directory_temporary, filename)

                if exists(filename):
                    Path(filename)

                file.save(filename)

                descompactar(filename, directory_temporary)
                mover_arquivos(directory_temporary)
        except Exception as e:
            data['message'] = 'Error'
            data['status'] = 0
            data['error'] = e
        finally:
            if exists(filename):
                Path(filename).rmtree()
                data = processar_dados_mensal(
                    dt_entrega, directory_temporary, nu_entrega)

        return jsonify(data)
    else:
        visaoEntregaMensalDadosRadarSat2 = VisaoEntregaMensalDadosRadarSat2.query.get(
            id)
        return render_template("radarsat2_substituir_entrega.html",
                               visao_entrega_mensal_radarsat2=visaoEntregaMensalDadosRadarSat2)


@app.route("/auditoria-buffer")
@login_required
def auditoria_buffer():
    entrega_buffer = VisaoEntregaBufferAte2015.query.filter_by(st_finalizado=True).order_by(
        VisaoEntregaBufferAte2015.nu_entrega.desc()).all()
    return render_template('auditoria_buffer.html', entrega_buffer=entrega_buffer)


@app.route("/auditoria-buffer-novo-pacote", methods=['POST'])
@login_required
def auditoria_buffer_novo_pacote():
    data = {
        'message': '',
        'status': 1,
        'error': ''
    }

    file = request.files['filezip']
    directory_temporary = join(app.config['UPLOAD_FOLDER'], 'buffer')

    if exists(directory_temporary):
        Path(directory_temporary).rmtree()

    if not exists(directory_temporary):
        mkdir(directory_temporary)

    try:
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filename = join(directory_temporary, filename)

            if exists(filename):
                Path(filename)

            file.save(filename)

            descompactar(filename, directory_temporary)
            mover_arquivos(directory_temporary)
    except Exception as e:
        data['message'] = 'Error'
        data['status'] = 0
        data['error'] = e
    finally:
        if exists(filename):
            Path(filename).rmtree()
            data = novo_pacote(directory_temporary)

    return jsonify(data)


@app.route("/auditoria-buffer-excluir-pacote", methods=['POST'])
@login_required
def auditoria_buffer_excluir_pacote():
    entrega_id = request.form['entrega_id']
    nu_entrega = request.form['nu_entrega']

    data = {
        'message': 'Registro excluido com sucesso.',
        'status': 'ok',
        'error': ''
    }

    try:

        db.engine.execute(text(
            "DELETE FROM funai.img_mascara_antropismo_consolidado_buffer_ate2015_a WHERE id IN (SELECT img_mascara_buffer_ate2015_id FROM funai.tb_usuario_entrega_buffer_ate2015 WHERE tb_entrega_id = {})".format(
                entrega_id)).execution_options(autocommit=True))
        db.engine.execute(text("DELETE FROM funai.tb_usuario_entrega_buffer_ate2015 WHERE tb_entrega_id = {}".format(
            entrega_id)).execution_options(autocommit=True))
        db.engine.execute(
            text("DELETE FROM funai.tb_entrega_buffer_ate2015 WHERE id = {}".format(entrega_id)).execution_options(
                autocommit=True))

    except Exception as e:
        data['message'] = "Ocorreu um erro ao excluir o registro da Auditoria Buffer da Entrega {}.".format(
            nu_entrega)
        data['status'] = 'error'
        data['error'] = e

    return jsonify(data)


@app.route("/auditoria-buffer-gerar-shp/<int:entrega_id>", methods=['GET'])
@login_required
def auditoria_buffer_gerar_shp(entrega_id):
    no_caminho = app.config['SHP_FOLDER']
    no_arquivo = gerar_shapefile_pacote(entrega_id)
    return send_file("{}{}".format(no_caminho, no_arquivo),
                     mimetype='application/zip',
                     attachment_filename=no_arquivo,
                     as_attachment=True)


@app.route("/atualizar-visoes-aviso-diario", methods=['POST'])
@login_required
def atualizar_visoes_aviso_diario():
    data = {
        'message': 'Visões atualizadas com sucesso. Pode gerar o novo Aviso Diário.',
        'status': 1,
        'error': ''
    }

    db2 = psycopg2.connect(app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
    conn = db2.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

    conn.execute(
        "SELECT COUNT(id) AS nu_total FROM funai.img_monitoramento_consolidado_a WHERE to_char(dt_cadastro,'YYYY-MM-DD') = to_char(NOW(),'YYYY-MM-DD')")
    result = conn.fetchone()

    if result.nu_total > 0:
        db.engine.execute(text(
            "SELECT funai.atualizar_visoes_diaria()").execution_options(autocommit=True))
    else:
        data[
            'message'] = "<h4>Ainda não foi efetuada a CARGA do dia! Efetue primeiro a CARGA e depois atualize as visões.</h4>"
        data['status'] = 0

    return jsonify(data)


@app.route("/alerta-urgente")
@login_required
def alerta_urgente():
    usuario_data = None
    usuario = VisaoUsuarioInterpreteMonitora.query.filter_by(
        id=session['usuario']['id']).first()

    if usuario is not None:
        usuario_data = usuario

    return render_template("alerta_urgente.html", data=VisaoAlertaUrgenteGerado.query.all(), usuario=usuario_data)

@app.route("/copiar-poligono-analista", methods=["POST"])
@login_required
def copiar_poligono_analista():
    no_tabela = request.form["no_tabela"]
    no_tabela_alerta = request.form["no_tabela_alerta"]
    monitoramento_id = str(request.form.getlist('monitoramento_id')).replace('[', '').replace(']', '').replace('u',
                                                                                                               '').replace(
        ', ', ',').replace("'", '')

    try:
        SQL = "no_estagio, " \
              "no_imagem, " \
              "nu_orbita, " \
              "nu_ponto, " \
              "dt_t_zero, " \
              "dt_t_um, " \
              "geom " \
              "FROM " \
              "{} " \
              "WHERE " \
              "id IN ({})".format(no_tabela, monitoramento_id)

        s = select([SQL])

        data = db.engine.execute(s).fetchall()

        if len(data) > 0:
            INSERT_ALERTA = "INSERT INTO {}(no_estagio, no_imagem, nu_orbita, nu_ponto, dt_t_zero, dt_t_um, geom)" \
                "SELECT no_estagio, no_imagem, nu_orbita, nu_ponto, dt_t_zero, dt_t_um, ST_Multi(geom)::geometry(MultiPolygon,4674) AS geom FROM {} WHERE id IN ({})".format(
                    no_tabela_alerta, no_tabela, monitoramento_id)

            db.engine.execute(
                text(INSERT_ALERTA).execution_options(autocommit=True))

            return jsonify({"status": "ok", "message": "ID(s) <strong>'{}'</strong>, foram copiado(s) com sucesso para tabela <strong>'{}'</strong>.".format(monitoramento_id, no_tabela_alerta)})
        else:
            return jsonify({"status": "aviso", "message": "ID(s) <strong>'{}'</strong>, informado(s) não foram encontrado(s) na tabela <strong>'{}'</strong>, e por isso, não podem ser copiado(s). Favor Verificar!".format(monitoramento_id, no_tabela)})
    except Exception as e:
        app.logger.exception(e)
        return jsonify({"status": "error", "message": "Ocorreu um erro ao copiados os dados."})


@app.route("/gerar-alerta-urgente-consolidado", methods=["POST"])
@login_required
def gerar_alerta_urgente_consolidado():
    no_tabela = request.form["no_tabela"]
    no_tabela_alerta = request.form["no_tabela_alerta"]

    try:
        INSERT_ALERTA_URGENTE_GERADO = "INSERT INTO funai.tb_alerta_urgente_gerado(tb_usuario_id, dt_envio) " \
                                       "SELECT {}::integer AS tb_usuario_id, to_char(NOW(),'YYYY-MM-DD')::date AS dt_envio".format(
                                           session['usuario']['id'])
        db.engine.execute(
            text(INSERT_ALERTA_URGENTE_GERADO).execution_options(autocommit=True))

        SQL = "MAX(id) AS id FROM funai.tb_alerta_urgente_gerado WHERE tb_usuario_id = {} AND to_char(dt_cadastro,'YYYY-MM-DD') = to_char(NOW(),'YYYY-MM-DD')".format(
            session['usuario']['id'])

        data = db.engine.execute(select([SQL])).fetchone()

        if len(data) > 0:
            INSERT_ALERTA_URGENTE_CONSOLIDADO = "INSERT INTO funai.img_alerta_urgente_consolidado_a(tb_alerta_urgente_gerado_id, tb_ciclo_monitoramento_id, nu_referencia, no_estagio, nu_mapa, no_imagem, nu_orbita, nu_ponto, dt_t_zero, dt_t_um, co_funai, no_ti, ds_cr, no_municipio, sg_uf, geom)" \
                                                "WITH info_ti AS ( " \
                                                "  SELECT " \
                                                "    c.id AS tb_ciclo_monitoramento_id, " \
                                                "    c.no_ciclo, " \
                                                "    a.no_estagio, " \
                                                "    a.nu_mapa, " \
                                                "    a.no_imagem, " \
                                                "    a.nu_orbita, " \
                                                "    a.nu_ponto, " \
                                                "    a.dt_t_zero, " \
                                                "    a.dt_t_um, " \
                                                "    t.co_funai, " \
                                                "    t.no_ti, " \
                                                "    t.ds_cr, " \
                                                "    a.geom  " \
                                                "  FROM  " \
                                                "    funai.lim_terra_indigena_a AS t, " \
                                                "    {} a " \
                                                "  INNER JOIN  " \
                                                "    funai.vm_ciclo_monitoramento AS c ON (c.dt_monitorada = a.dt_t_um) " \
                                                "  WHERE  " \
                                                "    ST_Intersects(a.geom,t.geom) " \
                                                "),  " \
                                                "info_ti_uf AS ( " \
                                                "SELECT " \
                                                "    t.tb_ciclo_monitoramento_id, " \
                                                "    t.no_ciclo, " \
                                                "    (SELECT COALESCE(MAX(nu_referencia)+1,1) FROM funai.img_alerta_urgente_consolidado_a)::integer AS      nu_referencia, " \
                                                "    t.no_estagio, " \
                                                "    t.nu_mapa, " \
                                                "    t.no_imagem, " \
                                                "    t.nu_orbita, " \
                                                "    t.nu_ponto, " \
                                                "    t.dt_t_zero, " \
                                                "    t.dt_t_um, " \
                                                "    t.co_funai, " \
                                                "    initcap(t.no_ti) AS no_ti, " \
                                                "    replace(initcap(t.ds_cr),'Cr','CR') AS ds_cr, " \
                                                "    array_to_string(array_agg(DISTINCT initcap(u.no_municipio)),', ') AS no_municipio,  " \
                                                "    u.sg_uf, " \
                                                "    t.geom  " \
                                                "  FROM " \
                                                "    info_ti AS t, " \
                                                "    cb.vw_lim_municipio_a_lim_unidade_federacao_a AS u " \
                                                "  WHERE " \
                                                "    ST_Intersects(t.geom,u.m_geom) " \
                                                "  GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,15,16 " \
                                                ") " \
                                                "SELECT " \
                                                "  {}::integer AS img_alerta_urgente_consolidado_a_id, " \
                                                "  t.tb_ciclo_monitoramento_id, " \
                                                "  t.nu_referencia, " \
                                                "  t.no_estagio, " \
                                                "  t.nu_mapa, " \
                                                "  t.no_imagem, " \
                                                "  t.nu_orbita, " \
                                                "  t.nu_ponto, " \
                                                "  t.dt_t_zero, " \
                                                "  t.dt_t_um, " \
                                                "  t.co_funai, " \
                                                "  t.no_ti, " \
                                                "  t.ds_cr, " \
                                                "  t.no_municipio,  " \
                                                "  t.sg_uf, " \
                                                "  t.geom " \
                                                "FROM " \
                                                "  info_ti_uf AS t".format(
                                                    no_tabela_alerta, data[0])

            db.engine.execute(
                text(INSERT_ALERTA_URGENTE_CONSOLIDADO).execution_options(autocommit=True))

            SQL = "COUNT(auc.id) AS total FROM funai.img_alerta_urgente_consolidado_a AS auc INNER JOIN funai.tb_alerta_urgente_gerado AS aug ON (aug.id = auc.tb_alerta_urgente_gerado_id) WHERE aug.tb_usuario_id = {} AND to_char(auc.dt_cadastro,'YYYY-MM-DD') = to_char(NOW(),'YYYY-MM-DD')".format(
                session['usuario']['id'])

            data = db.engine.execute(select([SQL])).fetchone()

            if len(data) > 0:
                db.engine.execute(
                    text("TRUNCATE {}".format(no_tabela_alerta)).execution_options(
                        autocommit=True))

        return jsonify({"status": "ok", "message": "Alerta Urgente gerado com sucesso. Favor os dados na tabela <strong>'funai.vw_alerta_urgente_consolidado_a'</strong>, no seu QGIS."})
    except Exception as e:
        app.logger.exception(e)
        return jsonify({"status": "error", "message": "Ocorreu um erro ao gerar o alerta urgente."})
