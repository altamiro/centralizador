/**
 * Created by altamiro on 24/08/16.
 */

var strMessage = 'Aguarde! Processando Novo Pacote da Auditoria Buffer...';

$(document).ready(function () {

    $('#tabela_entrega_buffer_ate2015').DataTable({
        "language": {
            "url": baseUrl + "static/js/datatables/pt_BR.json"
        },
        "pageLength": 25,
        "bPaginate": true,
        "bInfo": true,
        "bSort": false,
        "bFilter": true
    });

    if ($('#frmEntregaBuffer').length) {
        $('#frmEntregaBuffer').submit(enviarNovoPacote);
    } // end iF;

}); // end $(document).ready

function enviarNovoPacote() {
    var _filezip = $('#filezip');

    if (_filezip.val().match(/.zip|.rar/) == null) {
        bootbox.alert('Selecione um arquivo do tipo ZIP ou RAR.', function () {
            _filezip.focus();
        });
        return false;
    } // end iF;

    $('#frmEntregaBuffer').ajaxSubmit({
        url: baseUrl + 'auditoria-buffer-novo-pacote',
        type: 'post',
        dataType: 'json',
        success: function (result) {
            if (result.status > 0) {
                bootbox.alert("<span class='text-info strong'>" + result.message + "</span>", function () {
                    window.location.reload();
                });
            } else {
                msg = result.message;
                if (result.error != "") {
                    msg += "\nError: " + result.error;
                } // end iF;
                bootbox.alert("<span class='text-danger strong'>" + msg + "</span>");
            } // end iF;
        } // end success
    }); // end ajaxSubmit

} // end function enviarNovaEntrega

function excluirAuditoriaBuffer(entrega_id, nu_entrega) {
    bootbox.setLocale('br');
    bootbox.confirm("<span class='text-danger strong'>Tem certeza que deseja excluir todos os registros da entrega Nr. " + nu_entrega + " da Auditoria do Buffer?</span>",
        function (result) {
            if (result) {
                $.post(baseUrl + 'auditoria-buffer-excluir-pacote', {
                    entrega_id: entrega_id,
                    nu_entrega: nu_entrega
                }, function (result) {
                    if (result.status == 'error') {
                        bootbox.alert("<span class='text-danger strong'>" + result.message + "</span>");
                        return false;
                    } else {
                        bootbox.alert("<span class='text-info strong'>" + result.message + "</span>", function () {
                            window.location.reload();
                        });
                    } // end iF;
                }, 'json'); // end $.post
            }
        });

} // end function excluirAuditoriaBuffer

function gerarShapeFileAuditoriaBuffer(entrega_id) {
    $.get(baseUrl + 'auditoria-buffer-gerar-shp/' + entrega_id); // end $.post
} // end function gerarShapeFileAuditoriaBuffer