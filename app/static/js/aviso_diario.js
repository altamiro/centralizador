/**
 * Created by altamiro on 24/08/16.
 */

var strMessage = 'Aguarde! Processando consulta...';

$(document).ready(function () {
    $("#sl_ciclo_ano_atual_orbita").on("change", function (e) {
        $('#panel_ciclo_ano_atual').empty();
        if (e.added && $("#sl_ciclo_ano_atual_orbita").val() != null) {
            buscarDataTUmPorCiclo('sl_ciclo_ano_atual', 'sl_ciclo_ano_atual_orbita', 'sl_ciclo_ano_atual_orbita_dt_t_um', 'dv_ciclo_ano_atual_orbita_dt_t_um');
        } else if (e.removed && $("#sl_ciclo_ano_atual_orbita").val() != null) {
            buscarDataTUmPorCiclo('sl_ciclo_ano_atual', 'sl_ciclo_ano_atual_orbita', 'sl_ciclo_ano_atual_orbita_dt_t_um', 'dv_ciclo_ano_atual_orbita_dt_t_um');
        } else {
            $('#dv_ciclo_ano_atual_orbita_dt_t_um').removeClass('show').addClass('hide');
            $('#sl_ciclo_ano_atual_orbita_dt_t_um').select2("destroy").empty();
        } // end iF;

    }); // end iF #sl_orbita_dt_t_um

    $("#sl_ciclo_orbita").on("change", function (e) {
        $('#panel_ciclo').empty();
        if (e.added && $("#sl_ciclo_orbita").val() != null) {
            buscarDataTUmPorCiclo('sl_ciclo', 'sl_ciclo_orbita', 'sl_ciclo_orbita_dt_t_um', 'dv_ciclo_orbita_dt_t_um');
        } else if (e.removed && $("#sl_ciclo_orbita").val() != null) {
            buscarDataTUmPorCiclo('sl_ciclo', 'sl_ciclo_orbita', 'sl_ciclo_orbita_dt_t_um', 'dv_ciclo_orbita_dt_t_um');
        } else {
            $('#dv_ciclo_orbita_dt_t_um').removeClass('show').addClass('hide');
            $('#sl_ciclo_orbita_dt_t_um').select2("destroy").empty();
        } // end iF;

    }); // end iF #sl_orbita_dt_t_um

    $("#sl_ciclo_orbita_legado").on("change", function (e) {
        $('#panel_ciclo_legado').empty();
        if (e.added && $("#sl_ciclo_orbita_legado").val() != null) {
            buscarDataTUmPorCiclo('sl_ciclo_legado', 'sl_ciclo_orbita_legado', 'sl_ciclo_orbita_dt_t_um_legado', 'dv_ciclo_orbita_dt_t_um_legado');
        } else if (e.removed && $("#sl_ciclo_orbita_legado").val() != null) {
            buscarDataTUmPorCiclo('sl_ciclo_legado', 'sl_ciclo_orbita_legado', 'sl_ciclo_orbita_dt_t_um_legado', 'dv_ciclo_orbita_dt_t_um_legado');
        } else {
            $('#dv_ciclo_orbita_dt_t_um_legado').removeClass('show').addClass('hide');
            $('#sl_ciclo_orbita_dt_t_um_legado').select2("destroy").empty();
        } // end iF;

    }); // end iF #sl_orbita_dt_t_um_legado

    if ($('#frmCicloAnoAtual').length) {
        $('#frmCicloAnoAtual').submit(pesquisarCicloAnoAtual);
    } // end iF;

    if ($('#frmCiclo').length) {
        $('#frmCiclo').submit(pesquisarCiclo);
    } // end iF;

    if ($('#frmCicloLegado').length) {
        $('#frmCicloLegado').submit(pesquisarCicloLegado);
    } // end iF;

    if ($('#frmRelatorio').length) {
        $('#frmRelatorio').submit(pesquisarRelatorio);
    } // end iF;

    opcaoTipoRelatorio('td');

}); // end $(document).ready

function limparAbaRelatorio() {
    $("input[name='rd_relatorio_opcao_filtro']").each(function () {
        this.checked = false;
    })
    $('#dt_inicial').val('');
    $('#dt_final').val('');
    $('#panel_relatorio').empty();
} // end function limparAbaRelatorio

function limparAbaDiarioAnoAtual() {
    $("input[name='rd_ciclo_ano_atual_opcao_filtro']").each(function () {
        this.checked = false;
    })
    $('#sl_ciclo_ano_atual').val('');
    $('#sl_ciclo_ano_atual_orbita').select2("destroy").empty();
    $('#sl_ciclo_ano_atual_orbita_dt_t_um').select2("destroy").empty();
    $('#dv_ciclo_ano_atual_orbita').removeClass('show').addClass('hide');
    $('#dv_ciclo_ano_atual_orbita_dt_t_um').removeClass('show').addClass('hide');
    $('#panel_ciclo_ano_atual').empty();
} // end function limparAbaDiarioAnoAtual

function limparAbaDiario() {
    $("input[name='rd_ciclo_opcao_filtro']").each(function () {
        this.checked = false;
    })
    $('#sl_ciclo').val('');
    $('#sl_ciclo_orbita').select2("destroy").empty();
    $('#sl_ciclo_orbita_dt_t_um').select2("destroy").empty();
    $('#dv_ciclo_orbita').removeClass('show').addClass('hide');
    $('#dv_ciclo_orbita_dt_t_um').removeClass('show').addClass('hide');
    $('#panel_ciclo').empty();
} // end function limparAbaDiario

function limparAbaDiarioLegado() {
    $("input[name='rd_ciclo_opcao_filtro_legado']").each(function () {
        this.checked = false;
    })
    $('#sl_ciclo_legado').val('');
    $('#sl_ciclo_orbita_legado').select2("destroy").empty();
    $('#sl_ciclo_orbita_dt_t_um_legado').select2("destroy").empty();
    $('#dv_ciclo_orbita_legado').removeClass('show').addClass('hide');
    $('#dv_ciclo_orbita_dt_t_um_legado').removeClass('show').addClass('hide');
    $('#panel_ciclo_legado').empty();
} // end function limparAbaDiario

function limparImputDiv(abaID) {

    if (abaID == 'aba_diario_ano_atual') {
        limparAbaDiario();
        limparAbaDiarioLegado();
        limparAbaRelatorio();
        opcaoTipoRelatorio('td');
    } else if (abaID == 'aba_diario') {
        limparAbaDiarioAnoAtual();
        limparAbaDiarioLegado();
        limparAbaRelatorio();
        opcaoTipoRelatorio('td');
    } else if (abaID == 'aba_diario_legado') {
        limparAbaDiarioAnoAtual();
        limparAbaDiario();
        limparAbaRelatorio();
        opcaoTipoRelatorio('td');
    } else if (abaID == 'aba_relatorio') {
        limparAbaDiarioAnoAtual();
        limparAbaDiario();
        limparAbaDiarioLegado();        
        opcaoTipoRelatorio('td');
    } // end iF;
} // end function limparImputDiv

function opcaoTipoRelatorio(opcao) {
    if (opcao == 'ti') {
        $("#opcao_cr_sl1, #opcao_uf_sl1").select2('val', '').select2("destroy");
        $("#opcao_ti").removeClass('hide').addClass('show');
        $("#opcao_cr, #opcao_uf").removeClass('show').addClass('hide');
        $("#opcao_ti_sl1, #opcao_ti_sl2, #opcao_ti_sl3").select2();
    } else if (opcao == 'cr') {
        $("#opcao_ti_sl1, #opcao_ti_sl2, #opcao_ti_sl3, #opcao_uf_sl1").select2('val', '').select2("destroy");
        $("#opcao_cr").removeClass('hide').addClass('show');
        $("#opcao_ti, #opcao_uf").removeClass('show').addClass('hide');
        $("#opcao_cr_sl1").select2();
    } else if (opcao == 'uf') {
        $("#opcao_ti_sl1, #opcao_ti_sl2, #opcao_ti_sl3, #opcao_cr_sl1").select2('val', '').select2("destroy");
        $("#opcao_uf").removeClass('hide').addClass('show');
        $("#opcao_ti, #opcao_cr").removeClass('show').addClass('hide');
        $("#opcao_uf_sl1").select2();        
    } else if (opcao == 'td') {
        $("#opcao_ti_sl1, #opcao_ti_sl2, #opcao_ti_sl3, #opcao_cr_sl1, #opcao_uf_sl1").select2('val', '').select2("destroy");
        $("#opcao_ti, #opcao_cr, #opcao_uf").removeClass('show').addClass('hide');
    } // end iF;
} // end function opcao

function buscarNuOrbitaPorCiclo(obj, selectOrbita, selectDtTUm, divOrbita, divDtTUm) {
    var _this = $(obj);
    var _objSelectOrbita = $('#' + selectOrbita);
    var _objSelectDtTUm = $('#' + selectDtTUm);
    var _objDivOrbita = $('#' + divOrbita);
    var _objDivDtTUm = $('#' + divDtTUm);

    _objSelectOrbita.select2("destroy");
    _objSelectOrbita.empty();

    _objSelectDtTUm.select2("destroy");
    _objSelectDtTUm.empty();
    _objDivDtTUm.removeClass('show').addClass('hide');

    $('#panel_ciclo').empty();

    if (_this.val() > 0) {
        _objDivOrbita.removeClass('hide').addClass('show');
        $.ajax({
            url: baseUrl + 'buscar-orbita-ciclo',
            type: 'POST',
            dataType: 'json',
            data: {
                ciclo_id: _this.val()
            },
            success: function (result) {
                if (result.data.length > 0) {
                    $.each(result.data, function (i, data) {
                        var option = $("<option>").val(data['nu_orbita']).html(data['ds_descricao']).appendTo(_objSelectOrbita);
                    }); // end each;
                    _objSelectOrbita.select2();
                } // end iF;
            }
        });
    } else {
        _objDivOrbita.removeClass('show').addClass('hide');
    } // end iF;
} // end function buscarNuOrbitaPorCiclo

function buscarDataTUmPorCiclo(ciclo, orbita, select, div) {
    var _objCiclo = $('#' + ciclo);
    var _objOrbita = $('#' + orbita);
    var _objSelect = $('#' + select);
    var _objDiv = $('#' + div);

    _objSelect.select2("destroy");
    _objSelect.empty();

    if (_objOrbita.val().length > 0) {
        _objDiv.removeClass('hide').addClass('show');
        $.ajax({
            url: baseUrl + 'buscar-data-t-um-ciclo',
            type: 'POST',
            dataType: 'json',
            data: {
                ciclo_id: _objCiclo.val(),
                nu_orbita: _objOrbita.val()
            },
            success: function (result) {
                if (result.data.length > 0) {
                    $.each(result.data, function (i, data) {
                        var option = $("<option>").val(data['dt_t_um']).html(data['ds_descricao']).appendTo(_objSelect);
                    }); // end each;
                    _objSelect.select2();
                } // end iF;
            }
        });
    } else {
        _objDiv.removeClass('show').addClass('hide');
        $('#panel_ciclo').empty();
    } // end iF;
} // end function buscarDataTUmPorCiclo

function pesquisarCicloAnoAtual() {
    var _ciclo_id = $('#sl_ciclo_ano_atual');
    var _opcao_filtro = $("input[name='rd_ciclo_ano_atual_opcao_filtro']:checked");
    var _nu_orbita = $('#sl_ciclo_ano_atual_orbita');
    var _dt_t_um = $('#sl_ciclo_ano_atual_orbita_dt_t_um');
    var _dv_panel = $('#panel_ciclo_ano_atual');

    $.ajax({
        url: baseUrl + 'pesquisar-ciclo-fitro',
        type: 'POST',
        dataType: 'json',
        data: {
            ciclo_id: _ciclo_id.val(),
            tp_filtro: _opcao_filtro.val(),
            nu_orbita: _nu_orbita.val(),
            dt_t_um: _dt_t_um.val()
        },
        success: function (result) {
            if (result.data.length > 0) {
                if (_opcao_filtro.val() == 'ti') {
                    formatarDadosTerraIndigena(result.data, _dv_panel);
                } else if (_opcao_filtro.val() == 'cr') {
                    formatarDadosCoordenacaoRegional(result.data, _dv_panel);
                } else if (_opcao_filtro.val() == 'uf') {
                    formatarDadosUF(result.data, _dv_panel);
                } // end iF;
            } else {
                _html = '<div class="center strong" style="font-size: 16px; padding: 10px; color: #A52A2A;">';
                _html += 'Nenhum registro encontrado!';
                _html += '</div>';

                _dv_panel.empty().html(_html);
            } // end iF;
        }
    });
} // end function pesquisarCiclo

function pesquisarCiclo() {
    var _ciclo_id = $('#sl_ciclo');
    var _opcao_filtro = $("input[name='rd_ciclo_opcao_filtro']:checked");
    var _nu_orbita = $('#sl_ciclo_orbita');
    var _dt_t_um = $('#sl_ciclo_orbita_dt_t_um');
    var _dv_panel = $('#panel_ciclo');

    $.ajax({
        url: baseUrl + 'pesquisar-ciclo-fitro',
        type: 'POST',
        dataType: 'json',
        data: {
            ciclo_id: _ciclo_id.val(),
            tp_filtro: _opcao_filtro.val(),
            nu_orbita: _nu_orbita.val(),
            dt_t_um: _dt_t_um.val()
        },
        success: function (result) {
            if (result.data.length > 0) {
                if (_opcao_filtro.val() == 'ti') {
                    formatarDadosTerraIndigena(result.data, _dv_panel);
                } else if (_opcao_filtro.val() == 'cr') {
                    formatarDadosCoordenacaoRegional(result.data, _dv_panel);
                } else if (_opcao_filtro.val() == 'uf') {
                    formatarDadosUF(result.data, _dv_panel);
                } // end iF;
            } else {
                _html = '<div class="center strong" style="font-size: 16px; padding: 10px; color: #A52A2A;">';
                _html += 'Nenhum registro encontrado!';
                _html += '</div>';

                _dv_panel.empty().html(_html);
            } // end iF;
        }
    });
} // end function pesquisarCiclo

function pesquisarCicloLegado() {
    var _ciclo_id = $('#sl_ciclo_legado');
    var _opcao_filtro = $("input[name='rd_ciclo_opcao_filtro_legado']:checked");
    var _nu_orbita = $('#sl_ciclo_orbita_legado');
    var _dt_t_um = $('#sl_ciclo_orbita_dt_t_um_legado');
    var _dv_panel = $('#panel_ciclo_legado');

    $.ajax({
        url: baseUrl + 'pesquisar-ciclo-fitro',
        type: 'POST',
        dataType: 'json',
        data: {
            ciclo_id: _ciclo_id.val(),
            tp_filtro: _opcao_filtro.val(),
            nu_orbita: _nu_orbita.val(),
            dt_t_um: _dt_t_um.val()
        },
        success: function (result) {
            if (result.data.length > 0) {
                if (_opcao_filtro.val() == 'ti') {
                    formatarDadosTerraIndigena(result.data, _dv_panel);
                } else if (_opcao_filtro.val() == 'cr') {
                    formatarDadosCoordenacaoRegional(result.data, _dv_panel);
                } else if (_opcao_filtro.val() == 'uf') {
                    formatarDadosUF(result.data, _dv_panel);
                } // end iF;
            } else {
                _html = '<div class="center strong" style="font-size: 16px; padding: 10px; color: #A52A2A;">';
                _html += 'Nenhum registro encontrado!';
                _html += '</div>';

                _dv_panel.empty().html(_html);
            } // end iF;
        }
    });
} // end function pesquisarCicloLegado

function formatarDadosTerraIndigena(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_terra_indigena">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Código Funai</th>';
    _html += '        <th class="center">Terra Indígena</th>';
    _html += '        <th class="center">Ciclo</th>';
    _html += '        <th class="center">Orbita</th>';
    _html += '        <th class="center">Data da Imagem</th>';
    _html += '        <th class="right">CR</th>';
    _html += '        <th class="right">DG</th>';
    _html += '        <th class="right">DR</th>';
    _html += '        <th class="right">FF</th>';
    _html += '        <th class="right">Total</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        _html += '      <tr>';

        if (data[x].no_ciclo == null) {
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td class="right strong" colspan="5">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } else {
            _html += '        <td class="center">' + data[x].co_funai + '</td>';
            _html += '        <td class="left">' + data[x].no_ti + '</td>';
            _html += '        <td class="center">' + data[x].no_ciclo + '</td>';
            _html += '        <td class="center">' + data[x].nu_orbita + '</td>';
            _html += '        <td class="center">' + data[x].dt_t_um_formatada + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } // end iF

        _html += '      </tr>';

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);

} // end function formatarDadosTerraIndigena

function formatarDadosCoordenacaoRegional(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_coordenacao_regional">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Nome da CR</th>';
    _html += '        <th class="center">Ciclo</th>';
    _html += '        <th class="center">Orbita</th>';
    _html += '        <th class="center">Data da Imagem</th>';
    _html += '        <th class="right">CR</th>';
    _html += '        <th class="right">DG</th>';
    _html += '        <th class="right">DR</th>';
    _html += '        <th class="right">FF</th>';
    _html += '        <th class="right">Total</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        _html += '      <tr>';

        if (data[x].no_ciclo == null) {
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td class="right strong" colspan="4">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } else {
            _html += '        <td class="left">' + data[x].ds_cr + '</td>';
            _html += '        <td class="center">' + data[x].no_ciclo + '</td>';
            _html += '        <td class="center">' + data[x].nu_orbita + '</td>';
            _html += '        <td class="center">' + data[x].dt_t_um_formatada + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } // end iF

        _html += '      </tr>';

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);

} // end function formatarDadosCoordenacaoRegional

function formatarDadosUF(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_estado">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Estado</th>';
    _html += '        <th class="center">UF</th>';
    _html += '        <th class="center">Ciclo</th>';
    _html += '        <th class="center">Orbita</th>';
    _html += '        <th class="center">Data da Imagem</th>';
    _html += '        <th class="right">CR</th>';
    _html += '        <th class="right">DG</th>';
    _html += '        <th class="right">DR</th>';
    _html += '        <th class="right">FF</th>';
    _html += '        <th class="right">Total</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        _html += '      <tr>';

        if (data[x].no_ciclo == null) {
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td class="right strong" colspan="5">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } else {
            _html += '        <td class="left">' + data[x].no_uf + '</td>';
            _html += '        <td class="center">' + data[x].sg_uf + '</td>';
            _html += '        <td class="center">' + data[x].no_ciclo + '</td>';
            _html += '        <td class="center">' + data[x].nu_orbita + '</td>';
            _html += '        <td class="center">' + data[x].dt_t_um_formatada + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } // end iF

        _html += '      </tr>';

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);

} // end function formatarDadosUF

function pesquisarRelatorio() {
    var _opcao_filtro = $("input[name='rd_relatorio_opcao_filtro']:checked");
    var _dt_inicial = $('#dt_inicial');
    var _dt_final = $('#dt_final');
    var _dv_panel = $('#panel_relatorio');

    var _op_ti_flt_ti = 0;
    var _op_ti_flt_cr = 0;
    var _op_ti_flt_uf = 0;

    var _op_cr_flt_cr = 0;
    
    var _op_uf_flt_uf = 0;

    if (_opcao_filtro.val() == 'ti') {
        _op_ti_flt_ti = $('#opcao_ti_sl1').val();
        _op_ti_flt_cr = $('#opcao_ti_sl2').val();
        _op_ti_flt_uf = $('#opcao_ti_sl3').val();
    } else if (_opcao_filtro.val() == 'cr') {
        _op_cr_flt_cr = $('#opcao_cr_sl1').val();
    } else if (_opcao_filtro.val() == 'uf') {
        _op_uf_flt_uf = $('#opcao_uf_sl1').val();
    } // end iF;

    if (_dt_inicial.val().length == 0) {
        bootbox.alert('Informe uma Data Incial.', function () {
            _dt_inicial.focus();
        });
        return false;
    } // end iF;

    if (_dt_inicial.val().length < 10) {
        bootbox.alert('Informe uma Data Inicial valida.', function () {
            _dt_inicial.focus();
        });
        return false;
    } // end iF;

    if (_dt_final.val().length == 0) {
        bootbox.alert('Informe uma Data Final.', function () {
            _dt_final.focus();
        });
        return false;
    } // end iF;

    if (_dt_final.val().length < 10) {
        bootbox.alert('Informe uma Data Final valida.', function () {
            _dt_final.focus();
        });
        return false;
    } // end iF;

    $.ajax({
        url: baseUrl + 'pesquisar-relatorio-fitro',
        type: 'POST',
        dataType: 'json',
        data: {
            tp_filtro: _opcao_filtro.val(),
            dt_inicial: _dt_inicial.val(),
            dt_final: _dt_final.val(),
            op_ti_flt_ti: _op_ti_flt_ti,
            op_ti_flt_cr: _op_ti_flt_cr,
            op_ti_flt_uf: _op_ti_flt_uf,
            op_cr_flt_cr: _op_cr_flt_cr,
            op_uf_flt_uf: _op_uf_flt_uf
        },
        success: function (result) {
            if (result.data.length > 0) {
                if (_opcao_filtro.val() == 'ti') {
                    formatarDadosRelatorioTerraIndigena(result.data, _dv_panel);
                } else if (_opcao_filtro.val() == 'cr') {
                    formatarDadosRelatorioCoordenacaoRegional(result.data, _dv_panel);
                } else if (_opcao_filtro.val() == 'uf') {
                    formatarDadosRelatorioDadosUF(result.data, _dv_panel);
                } // end iF;
            } else {
                _html = '<div class="center strong" style="font-size: 16px; padding: 10px; color: #A52A2A;">';
                _html += 'Nenhum registro encontrado!';
                _html += '</div>';

                _dv_panel.empty().html(_html);
            } // end iF;
        }
    });
} // end function pesquisarRelatorio

function formatarDadosRelatorioTerraIndigena(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_relatorio_terra_indigena">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Código Funai</th>';
    _html += '        <th class="center">Terra Indígena</th>';
    _html += '        <th class="center">Estado</th>';
    _html += '        <th class="center">Coordenação Regional</th>';
    _html += '        <th class="right steelBlue">CR(ha)</th>';
    _html += '        <th class="right steelBlue">DG(ha)</th>';
    _html += '        <th class="right steelBlue">DR(ha)</th>';
    _html += '        <th class="right steelBlue">FF(ha)</th>';
    _html += '        <th class="right strong">Total(ha)</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        _html += '      <tr>';

        if (data[x].co_funai == null) {
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td class="right strong" colspan="4">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } else {
            _html += '        <td class="center">' + data[x].co_funai + '</td>';
            _html += '        <td class="left">' + data[x].no_ti + '</td>';
            _html += '        <td class="left">' + data[x].sg_uf + '</td>';
            _html += '        <td class="left">' + data[x].ds_cr + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } // end iF

        _html += '      </tr>';

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);

} // end function formatarDadosRelatorioTerraIndigena

function formatarDadosRelatorioCoordenacaoRegional(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_relatorio_coordenacao_regional">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Nome da CR</th>';
    _html += '        <th class="right steelBlue">CR(ha)</th>';
    _html += '        <th class="right steelBlue">DG(ha)</th>';
    _html += '        <th class="right steelBlue">DR(ha)</th>';
    _html += '        <th class="right steelBlue">FF(ha)</th>';
    _html += '        <th class="right strong">Total(ha)</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        _html += '      <tr>';

        if (data[x].ds_cr == null) {
            _html += '        <td class="right strong">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } else {
            _html += '        <td class="left">' + data[x].ds_cr + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } // end iF

        _html += '      </tr>';

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);

} // end function formatarDadosRelatorioCoordenacaoRegional

function formatarDadosRelatorioDadosUF(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_relatorio_estado">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Estado</th>';
    _html += '        <th class="center">UF</th>';
    _html += '        <th class="right steelBlue">CR(ha)</th>';
    _html += '        <th class="right steelBlue">DG(ha)</th>';
    _html += '        <th class="right steelBlue">DR(ha)</th>';
    _html += '        <th class="right steelBlue">FF(ha)</th>';
    _html += '        <th class="right strong">Total(ha)</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        _html += '      <tr>';

        if (data[x].no_uf == null) {
            _html += '        <td style="display: none" ></td>';
            _html += '        <td class="right strong" colspan="2">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } else {
            _html += '        <td class="left">' + data[x].no_uf + '</td>';
            _html += '        <td class="center">' + data[x].sg_uf + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dg_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].dr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ff_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].total_nu_area_ha) + '</td>';
        } // end iF

        _html += '      </tr>';

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);

} // end function formatarDadosRelatorioDadosUF
