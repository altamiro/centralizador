/**
 * Created by altamiro on 14/07/16.
 */

var strMessage = 'Aguarde! Validando usuário..';

$(document).ready(function () {
    if ($('#frmLogin').length) {
        $('#frmLogin').submit(autenticar);
    } // end iF;
});

function autenticar() {
    $.post(baseUrl + 'login', $('#frmLogin').serialize(), function (result) {
        if (result.status == 'error' || result.status == 'aviso') {
            bootbox.alert("<span class='text-danger strong'>" + result.message + "</span>");
            return false;
        } // end iF;
        window.location.href = baseUrl;
    }, 'json'); // end $.post
}