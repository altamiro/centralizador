/**
 * Created by altamiro on 12/16/16.
 */

/**
 * Abre uma janela popup
 *
 * @param STRING strUrl
 * @param STRING strWindowName
 * @param STRING strComplement
 * @return MIX
 */
function popup(strUrl, strWindowName, strComplement) {
    if (( strUrl == undefined ) || ( strWindowName == undefined )) return ( false );
    //if ( strComplement == undefined ) strComplement = "scrollbars=yes,resizable=yes,width=100%',height=460";
    Width = screen.availWidth;
    Height = screen.availHeight;
    strComplement = 'toolbar=no,location=no,directories=no,status=no,menubar=no,' +
        'scrollbars=1,resizable=no,copyhistory=1,width=' + Width + ',' +
        'height=' + Height + ',top=0,left=0', 'replace';
    var objWindow = window.open(strUrl, strWindowName, strComplement);
    objWindow.focus(objWindow.name);
    return ( objWindow );
}

/**
 * Abre uma janela popup modal
 *
 * @param STRING strUrl
 * @param STRING strWindowName
 * @param STRING strComplement
 * @param STRING strJsExecOnClose
 * @return MIX
 */
function popupModal(strUrl, strWindowName, strComplement, strJsExecOnClose) {
    var objWindow = popup(strUrl, strWindowName, strComplement);
    if (!objWindow) return ( false );
//  openBackground();
    checkPopupExists(objWindow, strJsExecOnClose);
    return ( true );
}

var objWindowPopupModal;
var intIntervalPopupModal;
var strJsExecOnClosePopupModal;
function checkPopupExists(objWindow, strJsExecOnClose) {
    if (( objWindow == undefined ) || ( !isObject(objWindow) )) return ( false );
    var intInterval = setInterval('checkPopupExistsInterval()', 1000);
    intIntervalPopupModal = intInterval;
    objWindowPopupModal = objWindow;
    strJsExecOnClosePopupModal = strJsExecOnClose;
    return ( true );
}

function checkPopupExistsInterval() {
    try {
        var strHref = objWindowPopupModal.location.href;
    }
    catch (err) {
        if (intIntervalPopupModal != undefined) {
            clearInterval(intIntervalPopupModal);
            if (strJsExecOnClosePopupModal != undefined) eval(strJsExecOnClosePopupModal);
            intIntervalPopupModal = null;
            objWindowPopupModal = null;
            strJsExecOnClosePopupModal = null;
        }
    }
    return ( true );
}

function isObject(mixElement) {
    return ( mixElement && typeof mixElement == 'object' ) || ( typeof mixElement == 'function' );
}
