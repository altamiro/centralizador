/**
 * Created by altamiro on 15/07/16.
 */

// Closure
(function () {

    /**
     * Decimal adjustment of a number.
     *
     * @param    {String}    type    The type of adjustment.
     * @param    {Number}    value    The number.
     * @param    {Integer}    exp        The exponent (the 10 logarithm of the adjustment base).
     * @returns    {Number}            The adjusted value.
     */
    function decimalAdjust(type, value, exp) {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    // Decimal round
    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    // Decimal floor
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    // Decimal ceil
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }

})();

function mascara(o, f) {
    v_obj = o
    v_fun = f
    setTimeout("execmascara()", 1);
} // end function mascara

function execmascara() {
    v_obj.value = v_fun(v_obj.value, v_obj)
} // end function execmascara

function leech(v, o) {
    v = v.replace(/o/gi, "0");
    v = v.replace(/i/gi, "1");
    v = v.replace(/z/gi, "2");
    v = v.replace(/e/gi, "3");
    v = v.replace(/a/gi, "4");
    v = v.replace(/s/gi, "5");
    v = v.replace(/t/gi, "7");
    return v;
} // end function leech

function soNumeros(v, o) {
    return v.replace(/\D/g, "");
} // end function soNumeros

function soNumerosSeparadoPorVirgula(v, o) {
    return v.replace(/[^0-9,]/g, "");
} // end functionsoNumerosSeparadoPorVirgula

function telefone(v, o) {
    v = v.replace(/\D/g, "");
    v = v.replace(/^(\d\d)(\d)/g, "($1) $2");
    v = v.replace(/(\d{4})(\d)/, "$1-$2");
    return v;
} // end function telefone

function cpf(v, o) {
    v = v.replace(/\D/g, "");
    v = v.replace(/(\d{3})(\d)/, "$1.$2");
    v = v.replace(/(\d{3})(\d)/, "$1.$2");

    v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
    return v;
} // end function cpf

function cep(v, o) {
    v = v.replace(/\D/g, "");
    v = v.replace(/^(\d{5})(\d)/, "$1-$2");
    return v;
} // end function cep

function data(v, o) {
    v = v.replace(/\D/g, "");
    v = v.replace(/^(\d{2})(\d)/, "$1/$2");
    v = v.replace(/(\d{2})(\d)/, "$1/$2");
    return v;
} // end function data

function mesano(v, o) {
    v = v.replace(/\D/g, "");
    v = v.replace(/^(\d{2})(\d)/, "$1/$2");
    return v;
} // end function mesano

function hora(v, o) {
    v = v.replace(/\D/g, "");
    v = v.replace(/^(\d{2})(\d)/, "$1:$2");
    return v;
} // end function hora

function cnpj(v, o) {
    v = v.replace(/\D/g, "");
    v = v.replace(/^(\d{2})(\d)/, "$1.$2");
    v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
    v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
    v = v.replace(/(\d{4})(\d)/, "$1-$2");
    return v;
} // end function cnpj

function cpfcnpj(v, o) {
    v = v.replace(/\D/g, "");
    if (v.length > 0 && v.length <= 11) {
        v = v.replace(/(\d{3})(\d)/, "$1.$2");
        v = v.replace(/(\d{3})(\d)/, "$1.$2");
        v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
        $(o).removeClass('cnpj');
        $(o).addClass('cpf');
    } else if (v.length > 11) {
        v = v.replace(/^(\d{2})(\d)/, "$1.$2");
        v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
        v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
        v = v.replace(/(\d{4})(\d)/, "$1-$2");
        $(o).removeClass('cpf');
        $(o).addClass('cnpj');
    } else {
        if ($(o).hasClass('cpf')) {
            $(o).removeClass('cpf');
        } else if ($(o).hasClass('cnpj')) {
            $(o).removeClass('cnpj');
        } // end IF;
    } // end iF;

    return v;
} // end function cpfcnpj

function romanos(v, o) {
    v = v.toUpperCase();
    v = v.replace(/[^IVXLCDM]/g, "");
    while (v.replace(/^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/, "") != "");
    v = v.replace(/.$/, "");
    return v;
} // end function romanos

function site(v, o) {
    //Esse sem comentarios para que vocï¿½ entenda sozinho ;-)
    v = v.replace(/^http:\/\/?/, "");
    dominio = v;
    caminho = "";
    if (v.indexOf("/") > -1)
        dominio = v.split("/")[0];
    caminho = v.replace(/[^\/]*/, "");
    dominio = dominio.replace(/[^\w\.\+-:@]/g, "");
    caminho = caminho.replace(/[^\w\d\+-@:\?&=%\(\)\.]/g, "");
    caminho = caminho.replace(/([\?&])=/, "$1");
    if (caminho != "") dominio = dominio.replace(/\.+$/, "");
    v = "http://" + dominio + caminho;
    return v;
} // end function site

function latlong(v, o) {
    v = v.replace(/\D/g, "");
    v = v.replace(/(\d{2})(\d)/, "$1º$2");
    v = v.replace(/(\d{2})(\d)/, "$1'$2");
    v = v.replace(/(\d{2})(\d)/, "$1,$2");
    return v;
} // end function latlong

function sidoc(v, o) {
    v = v.replace(/\D/g, "");
    v = v.replace(/(\d{5})(\d)/, "$1.$2");
    v = v.replace(/(\d{6})(\d)/, "$1.$2");

    v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
    return v;
} // end function sidoc