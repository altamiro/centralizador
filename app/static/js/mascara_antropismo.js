/**
 * Created by altamiro on 24/08/16.
 */

var strMessage = 'Aguarde! Processando consulta...';

$(document).ready(function () {
    if ($('#frmRelatorioMascara').length) {
        $('#frmRelatorioMascara').submit(pesquisarRelatorioMascara);
    } // end iF;
}); // end $(document).ready


function pesquisarRelatorioMascara() {
    var _opcao_filtro = $("input[name='rd_relatorio_opcao_filtro']:checked");
    var _dv_panel = $('#panel_mascara_relatorio');

    $.ajax({
        url: baseUrl + 'pesquisar-relatorio-mascara',
        type: 'POST',
        dataType: 'json',
        data: {
            tp_filtro: _opcao_filtro.val()
        },
        success: function (result) {
            if (result.data.length > 0) {
                if (_opcao_filtro.val() == 'ti') {
                    formatarDadosRelatorioMascaraTerraIndigena(result.data, _dv_panel);
                } else if (_opcao_filtro.val() == 'cr') {
                    formatarDadosRelatorioMascaraCoordenacaoRegional(result.data, _dv_panel);
                } else if (_opcao_filtro.val() == 'uf') {
                    formatarDadosRelatorioMascaraDadosUF(result.data, _dv_panel);
                } // end iF;
            } else {
                _html = '<div class="center strong" style="font-size: 16px; padding: 10px; color: #A52A2A;">';
                _html += 'Nenhum registro encontrado!';
                _html += '</div>';

                _dv_panel.empty().html(_html);
            } // end iF;
        }
    });
} // end function pesquisarRelatorioMascara

function formatarDadosRelatorioMascaraTerraIndigena(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_relatorio_mascara_terra_indigena">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Código Funai</th>';
    _html += '        <th class="center">Terra Indígena</th>';
    _html += '        <th class="right strong">Área Total TI(ha)</th>';
    _html += '        <th class="right strong">Área de Antropismo Consolidado(ha)</th>';
    _html += '        <th class="right strong">Área de Vegetação Natural(ha)</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        _html += '      <tr>';

        if (data[x].co_funai == null) {
            _html += '        <td style="display: none" ></td>';
            _html += '        <td class="right strong" colspan="2">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ti_total_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].mascara_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_vegetacao_natural_ha) + '</td>';
        } else {
            _html += '        <td class="center">' + data[x].co_funai + '</td>';
            _html += '        <td class="left">' + data[x].no_ti + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ti_total_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].mascara_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_vegetacao_natural_ha) + '</td>';
        } // end iF

        _html += '      </tr>';

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);

} // end function formatarDadosRelatorioMascaraTerraIndigena

function formatarDadosRelatorioMascaraCoordenacaoRegional(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_relatorio_mascara_coordenacao_regional">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Nome da CR</th>';
    _html += '        <th class="right strong">Área Total(ha)</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        _html += '      <tr>';

        if (data[x].ds_cr == null) {
            _html += '        <td class="right strong">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_ha) + '</td>';
        } else {
            _html += '        <td class="left">' + data[x].ds_cr + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_ha) + '</td>';
        } // end iF

        _html += '      </tr>';

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);

} // end function formatarDadosRelatorioMascaraCoordenacaoRegional

function formatarDadosRelatorioMascaraDadosUF(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_relatorio_mascara_estado">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Estado</th>';
    _html += '        <th class="center">UF</th>';
    _html += '        <th class="right strong">Área Total(ha)</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        _html += '      <tr>';

        if (data[x].no_uf == null) {
            _html += '        <td style="display: none" ></td>';
            _html += '        <td class="right strong" colspan="2">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_ha) + '</td>';
        } else {
            _html += '        <td class="left">' + data[x].no_uf + '</td>';
            _html += '        <td class="center">' + data[x].sg_uf + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_ha) + '</td>';
        } // end iF

        _html += '      </tr>';

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);

} // end function formatarDadosRelatorioMascaraDadosUF
