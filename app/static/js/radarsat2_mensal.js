/**
 * Created by altamiro on 24/08/16.
 */

var strMessage = 'Aguarde! Processando Entrega Mensal...';

$(document).ready(function () {

    $('#tabela_entrega_mensal_radarsat2').DataTable({
        "language": {
            "url": baseUrl + "static/js/datatables/pt_BR.json"
        },
        "pageLength": 25,
        "bPaginate": true,
        "bInfo": true,
        "bSort": false,
        "bFilter": true
    });

    if ($('#frmEntregaMensal').length) {
        $('#frmEntregaMensal').submit(enviarNovaEntrega);
    } // end iF;

}); // end $(document).ready

function enviarNovaEntrega() {
    var _filezip = $('#filezip');
    var _dt_entrega = $('#dt_entrega');

    if (_dt_entrega.val().length == 0) {
        bootbox.alert('Informe a Data da Entrega.', function () {
            _dt_entrega.focus();
        });
        return false;
    } // end iF;

    if (_dt_entrega.val().length < 10) {
        bootbox.alert('Informe a Data de Entrega valida.', function () {
            _dt_entrega.focus();
        });
        return false;
    } // end iF;

    if (_filezip.val().match(/.zip|.rar/) == null) {
        bootbox.alert('Selecione um arquivo do tipo ZIP ou RAR.', function () {
            _filezip.focus();
        });
        return false;
    } // end iF;

    $('#frmEntregaMensal').ajaxSubmit({
        url: baseUrl + 'nova-entrega-radarsat2-mensal',
        type: 'post',
        data: {
            dt_entrega: _dt_entrega.val()
        },
        dataType: 'json',
        success: function (result) {
            if (result.status > 0) {
                bootbox.alert("<span class='text-info strong'>" + result.message + "</span>", function () {
                    window.location.reload();
                });
            } else {
                msg = result.message;
                if (result.error != "") {
                    msg += "\nError: " + result.error;
                } // end iF;
                bootbox.alert("<span class='text-danger strong'>" + msg + "</span>");
            } // end iF;
        } // end success
    }); // end ajaxSubmit

} // end function enviarNovaEntrega