/**
 * Created by altamiro on 24/08/16.
 */

var strMessage = 'Aguarde! Salvando dados do aviso..';

$(document).ready(function () {

    $('.table-entrega-aviso-diario').DataTable({
        "language": {
            "url": baseUrl + "static/js/datatables/pt_BR.json"
        },
        "pageLength": 25,
        "bPaginate": true,
        "bInfo": true,
        "bSort": false,
        "bFilter": true
    });

    if ($('#frmAvisoDiario').length) {
        $('#frmAvisoDiario').submit(salvarNovoAviso);
    } // end iF;
}); // end $(document).ready

function limparImputDiv() {
    $('#tb_ciclo_monitoramento_id').val('');
    $('#st_situacao').val('');
    $('#nu_orbita').select2("destroy").empty();
    $('#dv_ciclo_orbita').removeClass('show').addClass('hide');
} // end function limparImputDiv

function buscarNuOrbitaPorCiclo(obj, selectOrbita, divOrbita) {
    var _this = $(obj);
    var _objSelectOrbita = $('#' + selectOrbita);
    var _objDivOrbita = $('#' + divOrbita);

    _objSelectOrbita.select2("destroy");
    _objSelectOrbita.empty();

    if (_this.val() > 0) {
        _objDivOrbita.removeClass('hide').addClass('show');
        $.ajax({
            url: baseUrl + 'buscar-orbita-ciclo',
            type: 'POST',
            dataType: 'json',
            data: {
                ciclo_id: _this.val()
            },
            success: function (result) {
                if (result.data.length > 0) {
                    $.each(result.data, function (i, data) {
                        var option = $("<option>").val(data['nu_orbita']).html(data['ds_descricao']).appendTo(_objSelectOrbita);
                    }); // end each;
                    _objSelectOrbita.select2();
                } // end iF;
            }
        });
    } else {
        _objDivOrbita.removeClass('show').addClass('hide');
    } // end iF;
} // end function buscarNuOrbitaPorCiclo

function salvarNovoAviso() {
    $.post(baseUrl + 'salvar-aviso-diario', $('#frmAvisoDiario').serialize(), function (result) {
        if (result.status == 'error' || result.status == 'aviso') {
            bootbox.alert("<span class='text-danger strong'>" + result.message + "</span>");
            return false;
        } // end iF;
        window.location.reload();
    }, 'json'); // end $.post
} // end function salvarNovoAviso

function excluirAviso(id) {
    bootbox.setLocale('br');
    bootbox.confirm("<span class='text-info strong'>Deseja excluir o registro?</span>",
        function (result) {
            if (result) {
                $.post(baseUrl + 'excluir-aviso-diario', {id: id}, function (result) {
                    if (result.status == 'error' || result.status == 'aviso') {
                        bootbox.alert("<span class='text-danger strong'>" + result.message + "</span>");
                        return false;
                    } // end iF;
                    window.location.reload();
                }, 'json'); // end $.post
            }
        });

} // end function excluirAviso