/**
 * Created by altamiro on 20/03/18.
 */

var strMessage = '';

$(document).ready(function () {

    btBr = [
        { 
            extend: "copyHtml5", 
            title: "Relatório - Alerta Urgente Consolidado Gerado" 
        }, 
        { 
            extend: "csvHtml5", 
            title: "Relatório - Alerta Urgente Consolidado Gerado" 
        }, 
        { // extend: 'excelHtml5',
            extend: "excelFlash", 
            title: "Relatório - Alerta Urgente Consolidado Gerado"
        }, 
        { 
            extend: "pdfHtml5",
            title: "Relatório - Alerta Urgente Consolidado Gerado"
        }, { 
            extend: "print", 
            title: "Relatório - Alerta Urgente Consolidado Gerado" 
        }
    ];

    if ($('.table-alerta-urgente-gerado').length) {
        $(".table-alerta-urgente-gerado").DataTable({
          language: {
            url: baseUrl + "static/js/datatables/pt_BR.json"
          },
          dom: "Bfrtip",
          pageLength: 25,
          bPaginate: true,
          bInfo: true,
          bSort: false,
          bFilter: true,
          buttons: btBr
        });
    } // end iF;

    if ($('#frmCopiarPoligono').length) {
        $('#frmCopiarPoligono').submit(copiarPoligono);
    } // end iF;

}); // end $(document).ready

function limparImputs() {
    $("#monitoramento_id").val("");
}
function copiarPoligono() {
    $.post(baseUrl + 'copiar-poligono-analista', $('#frmCopiarPoligono').serialize(), function (result) {
        if (result.status == 'error' || result.status == 'aviso') {
            bootbox.alert("<span class='text-danger'>" + result.message + "</span>");
            return false;
        } // end iF;

        bootbox.alert(
          "<span class='text-info'>" +
            result.message +
            "</span>",
          function() {
            window.location.reload();
          }
        );

    }, 'json'); // end $.post
} // end function copiarPoligono


function gerarAlertaUrgenteConsolidado() {

    bootbox.dialog({
        title: "<span class='text-info strong'>Deseja gerar o alerta urgente consolidado?</span>",
        message: "<h3 class='text-danger'>Antes de continuar esse procedimento, verifique se os dados da tabela <strong>" + $("#no_tabela_alerta").val() + "</strong>, estão ok.</h3>",
        buttons: {
            continuar: {
                label: 'Continuar',
                className: 'btn-success pull-left',
                callback: function () {
                    $.post(baseUrl + 'gerar-alerta-urgente-consolidado', $('#frmCopiarPoligono').serialize(), function(result) {
                        if (result.status == 'error' || result.status == 'aviso') {
                            bootbox.alert("<span class='text-danger'>" + result.message + "</span>");
                            return false;
                        } // end iF;

                        bootbox.alert("<span class='text-info'>" +result.message+ "</span>",function() {
                            window.location.reload();
                        }
                    );

                    }, 'json'); // end $.post
                }
            },
            cancelar: {
                label: 'Cancelar',
                className: 'btn-default'
            }
        }
    });

} // end function gerarAlertaUrgenteConsolidado