/**
 * Created by altamiro on 14/07/16.
 */

var strMessage = 'Aguarde! Salvando dados do usuário..';

$(document).ready(function () {
    if ($('#frmUsuario').length) {
        $('#frmUsuario').submit(salvar);
    } // end iF;
});

function salvar() {
    $.post(baseUrl + 'usuario/formulario', $('#frmUsuario').serialize(), function (result) {
        if (result.status == 'error' || result.status == 'aviso') {
            bootbox.alert("<span class='text-danger strong'>" + result.message + "</span>");
            return false;
        } // end iF;
        bootbox.alert("<span class='text-info'>" + result.message + "</span>", function () {
            window.location.href = result.url;
        });
    }, 'json'); // end $.post
}