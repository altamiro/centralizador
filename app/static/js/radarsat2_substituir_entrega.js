/**
 * Created by altamiro on 14/07/16.
 */

var strMessage = 'Aguarde! Processando Substituição da Entrega Mensal...';

$(document).ready(function () {
    if ($('#frmEntregaMensalRadarSat2').length) {
        $('#frmEntregaMensalRadarSat2').submit(enviarNovaEntrega);
    } // end iF;
});

function enviarNovaEntrega() {
    var _filezip = $('#filezip');
    var _dt_entrega = $('#dt_entrega');
    var _id = $("#id").val();

    if (_dt_entrega.val().length == 0) {
        bootbox.alert('Informe a Data da Entrega.', function () {
            _dt_entrega.focus();
        });
        return false;
    } // end iF;

    if (_dt_entrega.val().length < 10) {
        bootbox.alert('Informe a Data de Entrega valida.', function () {
            _dt_entrega.focus();
        });
        return false;
    } // end iF;

    if (_filezip.val().match(/.zip|.rar/) == null) {
        bootbox.alert('Selecione um arquivo do tipo ZIP ou RAR.', function () {
            _filezip.focus();
        });
        return false;
    } // end iF;

    $('#frmEntregaMensalRadarSat2').ajaxSubmit({
        url: baseUrl + 'radarsat2-substituir-entrega/' + _id,
        type: 'post',
        data: $('#frmEntregaMensalRadarSat2').serialize(),
        dataType: 'json',
        success: function (result) {
            if (result.status > 0) {
                bootbox.alert("<span class='text-info strong'>" + result.message + "</span>", function () {
                    window.location.href = baseUrl + 'radarsat2-mensal';
                });
            } else {
                msg = result.message;
                if (result.error != "") {
                    msg += "\nError: " + result.error;
                } // end iF;
                bootbox.alert("<span class='text-danger strong'>" + msg + "</span>");
            } // end iF;
        } // end success
    }); // end ajaxSubmit

} // end function enviarNovaEntrega