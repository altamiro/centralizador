/**
 * Created by altamiro on 14/07/16.
 */

var strMessage = 'Aguarde!';

var sHors = "0" + 0;
var sMins = "0" + 0;
var sSecs = -1;
var vGetSecs1;
var vGetSecs2;

function getSecs() {
    sSecs++;
    if (sSecs == 60) {
        sSecs = 0;
        sMins++;
        if (sMins <= 9) sMins = "0" + sMins;
    }
    if (sMins == 60) {
        sMins = "0" + 0;
        sHors++;
        if (sHors <= 9) sHors = "0" + sHors;
    }
    if (sSecs <= 9) sSecs = "0" + sSecs;
    $("#div_contador").html(sHors + ":" + sMins + ":" + sSecs);
    vGetSecs1 = setTimeout('getSecs()', 1000);
}

$(document).ready(function () {
    if ($('.date').length) {
        $('.date').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
    } // end iF;

    if ($('[data-toggle="popover"]').length) {
        $('[data-toggle="popover"]').popover()
    } // end iF;

});

$(document).ajaxStart(function () {
    $('#panel_ciclo').empty();
    $.blockUI({message: strMessage});

    if ($('#div_contador').length) {
        $("#div_contador").empty();
        vGetSecs2 = setTimeout('getSecs()', 1000);
    }

});

$(document).ajaxStop(function () {
    $.unblockUI();
    clearTimeout(vGetSecs1);
    clearTimeout(vGetSecs2);

    if ($('#div_contador').length) {
        $("#div_contador").empty();
    }

    var btButton = [];

    if ($('#tb_terra_indigena').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'CMR Landsat - Aviso Diario por Terra Indígena'
            },
            {
                extend: 'csvHtml5',
                title: 'CMR Landsat - Aviso Diario por Terra Indígena'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'CMR Landsat - Aviso Diario por Terra Indígena'
            },
            {
                extend: 'pdfHtml5',
                title: 'CMR Landsat - Aviso Diario por Terra Indígena'
            },
            {
                extend: 'print',
                title: 'CMR Landsat - Aviso Diario por Terra Indígena'
            }
        ]
    } else if ($('#tb_coordenacao_regional').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'CMR Landsat - Aviso Diario por Coordenação Regional'
            },
            {
                extend: 'csvHtml5',
                title: 'CMR Landsat - Aviso Diario por Coordenação Regional'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'CMR Landsat - Aviso Diario por Coordenação Regional'
            },
            {
                extend: 'pdfHtml5',
                title: 'CMR Landsat - Aviso Diario por Coordenação Regional'
            },
            {
                extend: 'print',
                title: 'CMR Landsat - Aviso Diario por Coordenação Regional'
            }
        ]
    } else if ($('#tb_estado').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'CMR Landsat - Aviso Diario por Estado'
            },
            {
                extend: 'csvHtml5',
                title: 'CMR Landsat - Aviso Diario por Estado'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'CMR Landsat - Aviso Diario por Estado'
            },
            {
                extend: 'pdfHtml5',
                title: 'CMR Landsat - Aviso Diario por Estado'
            },
            {
                extend: 'print',
                title: 'CMR Landsat - Aviso Diario por Estado'
            }
        ]
    } else if ($('#tb_deteccao_por_frame').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Frame'
            },
            {
                extend: 'csvHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Frame'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Frame'
            },
            {
                extend: 'pdfHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Frame'
            },
            {
                extend: 'print',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Frame'
            }
        ]
    } else if ($('#tb_deteccao_por_terra_indigena').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Terra Indígena'
            },
            {
                extend: 'csvHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Terra Indígena'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Terra Indígena'
            },
            {
                extend: 'pdfHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Terra Indígena'
            },
            {
                extend: 'print',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Terra Indígena'
            }
        ]
    } else if ($('#tb_deteccao_por_municipio').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Município'
            },
            {
                extend: 'csvHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Município'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Município'
            },
            {
                extend: 'pdfHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Município'
            },
            {
                extend: 'print',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por Município'
            }
        ]
    } else if ($('#tb_deteccao_por_terra_indigena_municipio').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por TIs(Belo Monte) / Município'
            },
            {
                extend: 'csvHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por TIs(Belo Monte) / Município'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por TIs(Belo Monte) / Município'
            },
            {
                extend: 'pdfHtml5',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por TIs(Belo Monte) / Município'
            },
            {
                extend: 'print',
                title: 'RadarSat2 - Entrega Mensal ' + $("#nu_entrega").val() + ' por TIs(Belo Monte) / Município'
            }
        ]
    } else if ($('#tb_deteccao_por_corte_raso_terra_indigena').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Corte Raso / TIs(Belo Monte)'
            },
            {
                extend: 'csvHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Corte Raso / TIs(Belo Monte)'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Corte Raso / TIs(Belo Monte)'
            },
            {
                extend: 'pdfHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Corte Raso / TIs(Belo Monte)'
            },
            {
                extend: 'print',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Corte Raso / TIs(Belo Monte)'
            }
        ]
    } else if ($('#tb_deteccao_por_corte_seletivo_terra_indigena').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Corte Seletivo / TIs(Belo Monte)'
            },
            {
                extend: 'csvHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Corte Seletivo / TIs(Belo Monte)'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Corte Seletivo / TIs(Belo Monte)'
            },
            {
                extend: 'pdfHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Corte Seletivo / TIs(Belo Monte)'
            },
            {
                extend: 'print',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Corte Seletivo / TIs(Belo Monte)'
            }
        ]
    } else if ($('#tb_deteccao_por_nova_infraestrutura_terra_indigena').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Nova Infraestrutura / TIs(Belo Monte)'
            },
            {
                extend: 'csvHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Nova Infraestrutura / TIs(Belo Monte)'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Nova Infraestrutura / TIs(Belo Monte)'
            },
            {
                extend: 'pdfHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Nova Infraestrutura / TIs(Belo Monte)'
            },
            {
                extend: 'print',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Nova Infraestrutura / TIs(Belo Monte)'
            }
        ]
    } else if ($('#tb_deteccao_por_nova_estrada_terra_indigena').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Nova Estrada / TIs(Belo Monte)'
            },
            {
                extend: 'csvHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Nova Estrada / TIs(Belo Monte)'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Nova Estrada / TIs(Belo Monte)'
            },
            {
                extend: 'pdfHtml5',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Nova Estrada / TIs(Belo Monte)'
            },
            {
                extend: 'print',
                title: 'RadarSat2 - Entrega Mensal Acumulado por Nova Estrada / TIs(Belo Monte)'
            }
        ]
    } else if ($('#tb_relatorio_terra_indigena').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'Relatorio - Aviso Diario por Terra Indígena - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                extend: 'csvHtml5',
                title: 'Relatorio - Aviso Diario por Terra Indígena - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'Relatorio - Aviso Diario por Terra Indígena - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                extend: 'pdfHtml5',
                title: 'Relatorio - Aviso Diario por Terra Indígena - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                extend: 'print',
                title: 'Relatorio - Aviso Diario por Terra Indígena - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            }
        ]
    } else if ($('#tb_relatorio_coordenacao_regional').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'Relatorio - Aviso Diario por Coordenação Regional - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                extend: 'csvHtml5',
                title: 'Relatorio - Aviso Diario por Coordenação Regional - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'Relatorio - Aviso Diario por Coordenação Regional - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                extend: 'pdfHtml5',
                title: 'Relatorio - Aviso Diario por Coordenação Regional - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                extend: 'print',
                title: 'Relatorio - Aviso Diario por Coordenação Regional - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            }
        ]
    } else if ($('#tb_relatorio_estado').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'Relatorio - Aviso Diario por Estado - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                extend: 'csvHtml5',
                title: 'Relatorio - Aviso Diario por Estado - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'Relatorio - Aviso Diario por Estado - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                extend: 'pdfHtml5',
                title: 'Relatorio - Aviso Diario por Estado - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            },
            {
                extend: 'print',
                title: 'Relatorio - Aviso Diario por Estado - ' + $("#dt_inicial").val().replace("/", "_").replace("/", "_") + '_a_' + $("#dt_final").val().replace("/", "_").replace("/", "_")
            }
        ]
    } else if ($('#tb_relatorio_mascara_terra_indigena').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'Relatorio - Máscara de Antropismo por Terra Indígena'
            },
            {
                extend: 'csvHtml5',
                title: 'Relatorio - Máscara de Antropismo por Terra Indígena'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'Relatorio - Máscara de Antropismo por Terra Indígena'
            },
            {
                extend: 'pdfHtml5',
                title: 'Relatorio - Máscara de Antropismo por Terra Indígena'
            },
            {
                extend: 'print',
                title: 'Relatorio - Máscara de Antropismo por Terra Indígena'
            }
        ]
    } else if ($('#tb_relatorio_mascara_coordenacao_regional').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'Relatorio - Máscara de Antropismo por Coordenação Regional'
            },
            {
                extend: 'csvHtml5',
                title: 'Relatorio - Máscara de Antropismo por Coordenação Regional'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'Relatorio - Máscara de Antropismo por Coordenação Regional'
            },
            {
                extend: 'pdfHtml5',
                title: 'Relatorio - Máscara de Antropismo por Coordenação Regional'
            },
            {
                extend: 'print',
                title: 'Relatorio - Máscara de Antropismo por Coordenação Regional'
            }
        ]
    } else if ($('#tb_relatorio_mascara_estado').length) {
        btButton = [
            {
                extend: 'copyHtml5',
                title: 'Relatorio - Máscara de Antropismo por Estado'
            },
            {
                extend: 'csvHtml5',
                title: 'Relatorio - Máscara de Antropismo por Estado'
            },
            {
                // extend: 'excelHtml5',
                extend: 'excelFlash',
                title: 'Relatorio - Máscara de Antropismo por Estado'
            },
            {
                extend: 'pdfHtml5',
                title: 'Relatorio - Máscara de Antropismo por Estado'
            },
            {
                extend: 'print',
                title: 'Relatorio - Máscara de Antropismo por Estado'
            }
        ]
    }
    // } else if ($("#tabela_alerta_urgente_gerado").length) {
    //     btButton = [
    //         { 
    //             extend: "copyHtml5", 
    //             title: "Relatório - Alerta Urgente Consolidado Gerado" 
    //         }, 
    //         { 
    //             extend: "csvHtml5", 
    //             title: "Relatório - Alerta Urgente Consolidado Gerado" 
    //         }, 
    //         { // extend: 'excelHtml5',
    //             extend: "excelFlash", 
    //             title: "Relatório - Alerta Urgente Consolidado Gerado"
    //         }, 
    //         { 
    //             extend: "pdfHtml5",
    //             title: "Relatório - Alerta Urgente Consolidado Gerado"
    //         }, { 
    //             extend: "print", 
    //             title: "Relatório - Alerta Urgente Consolidado Gerado" 
    //         }
    //     ];
    // } // end iF;

    if ($('.table-export').length) {
        $('.table-export').DataTable({
            dom: 'Bfrtip',
            "bPaginate": false,
            "bInfo": false,
            "bSort": false,
            "bFilter": false,
            buttons: btButton
        });
    } // end iF;

//'copy', 'csv', 'excel', 'pdf', 'print'
});

function formatarValor(mixed) {
    var int = parseInt(mixed.toFixed(2).toString().replace(/[^\d]+/g, ''));
    var tmp = int + '';
    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if (tmp.length > 6) {

        var num = mixed;
        var ret;

        var x = 0;

        if (num < 0) {
            num = Math.abs(num);
            x = 1;
        }  // end iF.


        if (isNaN(num)) {
            num = "0";
        }  // end iF.

        var cents = Math.floor((num * 100 + 0.5) % 100);

        num = Math.floor((num * 100 + 0.5) / 100).toString();

        if (cents < 10) {
            cents = "0" + cents;
        }  // end iF.


        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
            num = num.substring(0, num.length - (4 * i + 3)) + '.'
                + num.substring(num.length - (4 * i + 3));
            ret = num + ',' + cents;
        } // end for

        if (x == 1) {
            ret = ' - ' + ret
        }  // end iF.

        return ret;

    } else {
        if (tmp.length == 3) {
            tmp = '0' + tmp;
        } else if (tmp.length > 1) {
            tmp = tmp;
        } else {
            tmp = '0,00';
        } // end iF.
        return tmp;
    }  // end iF.
}


function monitoramentoConsolidado() {

    bootbox.dialog({
        title: "<span class='text-info strong'>Deseja atualizar os dados do monitoramento consolidado?</span>",
        message: "<h3 class='text-danger'>Antes de continuar esse procedimento, verifique se todos os interpretes já finalizaram seu monitoramento.</h3>",
        buttons: {
            continuar: {
                label: 'Continuar',
                className: 'btn-success pull-left',
                callback: function () {
                    $('#panel_ciclo').empty();
                    $("#sl_ciclo").val("");
                    $("#sl_ciclo_orbita, #sl_ciclo_orbita_dt_t_um").select2("val", "");
                    $("#dv_ciclo_orbita").removeClass('show').addClass('hide');
                    $("#dv_ciclo_orbita_dt_t_um").removeClass('show').addClass('hide');
                    atualizarMonitoramentoConsolidado();
                }
            },
            cancelar: {
                label: 'Cancelar',
                className: 'btn-default'
            }
        }
    });

} // end function monitoramentoConsolidado

function atualizarMonitoramentoConsolidado() {
    strMessage = "<div style='padding: 5px !important;'><p class='text-info strong'>Aguarde! Estamos efetuando a carga dos dados do Monitoramento Consolidado. <br/>Isso pode demorar um pouco..<br/><p><div id='div_contador' class='text-danger strong'>00:00:00</div></p></p></div>";

    $.ajax({
        url: baseUrl + 'atualizar-monitoramento-consolidado',
        type: 'POST',
        dataType: 'json',
        success: function (result) {
            var message = result.message;
            if (result.error != '') {
                message += "\n\n<span class='text-danger strong'>" + result.error + "</span>"
            } // end iF;
            bootbox.alert("<span class='text-info strong'>" + message + "</span>", function () {
                if (result.status > 0) {
                    window.location.reload();
                } // end iF;
            });
        }
    });

} // end function atualizarMonitoramentoConsolidado


function atualizarVisoesAvisoDiario() {
    strMessage = "<div style='padding: 5px !important;'><p class='text-info strong'>Aguarde! Estamos atualizando todas as visões usadas no Aviso Diário. <br/>Isso pode demorar um pouco..<br/><p><div id='div_contador' class='text-danger strong'>00:00:00</div></p></p></div>";

    $.ajax({
        url: baseUrl + 'atualizar-visoes-aviso-diario',
        type: 'POST',
        dataType: 'json',
        success: function (result) {
            var message = result.message;
            if (result.error != '') {
                message += "\n\n<span class='text-danger strong'>" + result.error + "</span>"
            } // end iF;
            bootbox.alert("<span class='text-info strong'>" + message + "</span>", function () {
                if (result.status > 0) {
                    window.location.reload();
                } // end iF;
            });
        }
    });

} // end function atualizarVisoesAvisoDiario