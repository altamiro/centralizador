/**
 * Created by altamiro on 24/08/16.
 */

var strMessage = 'Aguarde! Processando Entrega Trimestral...';

$(document).ready(function () {

    if ($('#frmEntregaTrimestral').length) {
        $('#frmEntregaTrimestral').submit(enviarNovaEntrega);
    } // end iF;

}); // end $(document).ready

function enviarNovaEntrega() {
    var _filezip = $('#filezip');
    var _dt_entrega = $('#dt_entrega');

    if (_dt_entrega.val().length == 0) {
        bootbox.alert('Informe a Data da Entrega.', function () {
            _dt_entrega.focus();
        });
        return false;
    } // end iF;

    if (_dt_entrega.val().length < 10) {
        bootbox.alert('Informe a Data de Entrega valida.', function () {
            _dt_entrega.focus();
        });
        return false;
    } // end iF;

    if (_filezip.val().match(/.zip|.rar/) == null) {
        bootbox.alert('Selecione um arquivo do tipo ZIP ou RAR.', function () {
            _filezip.focus();
        });
        return false;
    } // end iF;

    $('#frmEntregaTrimestral').ajaxSubmit({
        url: baseUrl + 'nova-entrega-radarsat2-trimestral',
        type: 'post',
        data: {
            dt_entrega: _dt_entrega.val()
        },
        dataType: 'json',
        success: function (result) {
            if (result.status > 0) {
                bootbox.alert("<span class='text-info strong'>" + result.message + "</span>", function () {
                    window.location.reload();
                });
            } else {
                msg = result.message;
                if (result.error != "") {
                    msg += "\nError: " + result.error;
                } // end iF;
                bootbox.alert("<span class='text-danger strong'>" + msg + "</span>");
            } // end iF;
        } // end success
    }); // end ajaxSubmit

} // end function enviarNovaEntrega