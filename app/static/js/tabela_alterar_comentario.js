/**
 * Created by altamiro on 24/08/16.
 */

var strMessage = 'Aguarde! Salvando dados do comentário da tabela..';

$(document).ready(function () {
    if ($('#frmTabela').length) {
        $('#frmTabela').submit(salvar);
    } // end iF;
});

function salvar() {
    $.post(baseUrl + 'tabela-alterar-comentario', $('#frmTabela').serialize(), function (result) {
        if (result.status == 'error' || result.status == 'aviso') {
            bootbox.alert("<span class='text-danger strong'>" + result.message + "</span>");
            return false;
        } // end iF;
        bootbox.alert("<span class='text-info'>" + result.message + "</span>", function () {
            window.location.href = baseUrl + 'tabela-informacao-atualizacao';
        });
    }, 'json'); // end $.post
}