/**
 * Created by altamiro on 14/07/16.
 */

var strMessage = 'Aguarde! Alterando dados do Aviso Diário..';

$(document).ready(function () {
    if ($('#frmEntregaAvisoDiario').length) {
        $('#frmEntregaAvisoDiario').submit(salvar);
    } // end iF;
});

function salvar() {
    var id = $("#id").val();
    $.post(baseUrl + 'aviso-diario/formulario/' + id, $('#frmEntregaAvisoDiario').serialize(), function (result) {
        if (result.status == 'error' || result.status == 'aviso') {
            bootbox.alert("<span class='text-danger strong'>" + result.message + "</span>");
            return false;
        } // end iF;
        bootbox.alert("<span class='text-info'>" + result.message + "</span>", function () {
            window.location.href = result.url;
        });
    }, 'json'); // end $.post
}