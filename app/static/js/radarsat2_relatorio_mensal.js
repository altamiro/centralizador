/**
 * Created by altamiro on 24/08/16.
 */

var strMessage = 'Aguarde! Processando Relatório Mensal...';

$(document).ready(function () {
    abaSelecionada('deteccao-por-frame');
}); // end $(document).ready

function abaSelecionada(abaID) {

    var nu_entrega = $("#nu_entrega").val();
    var _dv_panel = $("#div-" + abaID);

    $("#div-deteccao-por-frame, #div-deteccao-por-terra-indigena, #div-deteccao-por-municipio, #div-deteccao-por-terra-indigena-municipio, #div-deteccao-por-corte-raso-terra-indigena, #div-deteccao-por-corte-seletivo-terra-indigena, #div-deteccao-por-nova-infraestrutura-terra-indigena, #div-deteccao-por-nova-estrada-terra-indigena").empty();

    $.ajax({
        url: baseUrl + 'pesquisar-relatorio-mensal-radarsat2',
        type: 'POST',
        dataType: 'json',
        data: {
            nu_entrega: nu_entrega,
            ds_aba: abaID
        },
        success: function (result) {
            if (result.data.length > 0) {
                if (abaID == 'deteccao-por-frame') {
                    formatarDadosDeteccaoPorFrame(result.data, _dv_panel);
                } else if (abaID == 'deteccao-por-terra-indigena') {
                    formatarDadosDeteccaoPorTerraIndigena(result.data, _dv_panel);
                } else if (abaID == 'deteccao-por-municipio') {
                    formatarDadosDeteccaoPorMunicipio(result.data, _dv_panel);
                } else if (abaID == 'deteccao-por-terra-indigena-municipio') {
                    formatarDadosDeteccaoPorTerraIndigenaMunicipio(result.data, _dv_panel);
                } // end iF;
            } else {
                _html = '<div class="center strong" style="font-size: 16px; padding: 10px; color: #A52A2A;">';
                _html += 'Nenhum registro encontrado!';
                _html += '</div>';

                _dv_panel.empty().html(_html);
            } // end iF;
        }
    });

} // end function abaSelecionada

function formatarDadosDeteccaoPorFrame(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_deteccao_por_frame">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Número do Frame</th>';
    _html += '        <th class="center">Data da Imagem T0</th>';
    _html += '        <th class="center">Data da Imagem T1</th>';
    _html += '        <th class="center">Área do Frame Analisada(ha)</th>';
    _html += '        <th class="center">Corte Raso</th>';
    _html += '        <th class="center">Corte Seletivo</th>';
    _html += '        <th class="center">Nova Infra-estrutura</th>';
    _html += '        <th class="center">Nova Estrada</th>';
    _html += '        <th class="center">Seletivo Natural</th>';
    _html += '        <th class="center">Total</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        if (data[x].nu_entrega != null) {
            _html += '      <tr>';
            _html += '        <td class="center">' + data[x].nu_frame + '</td>';
            _html += '        <td class="center">' + data[x].dt_t_zero + '</td>';
            _html += '        <td class="center">' + data[x].dt_t_um + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].frame_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cs_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ni_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ne_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].sn_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_total) + '</td>';
            _html += '      </tr>';
        } // end iF

    } // end for;

    for (x = 0; x < data.length; x++) {

        if (data[x].nu_entrega == null) {
            _html += '      <tr>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td class="right strong" colspan="3">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].frame_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cs_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ni_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ne_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].sn_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_total) + '</td>';
            _html += '      </tr>';
        } // end iF

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);
} // end function formatarDadosDeteccaoPorFrame

function formatarDadosDeteccaoPorTerraIndigena(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_deteccao_por_terra_indigena">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Código Funai</th>';
    _html += '        <th class="center">Terra Indígena</th>';
    _html += '        <th class="center">Corte Raso</th>';
    _html += '        <th class="center">Corte Seletivo</th>';
    _html += '        <th class="center">Nova Infra-estrutura</th>';
    _html += '        <th class="center">Nova Estrada</th>';
    _html += '        <th class="center">Seletivo Natural</th>';
    _html += '        <th class="center">Total</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        if (data[x].nu_entrega != null) {
            _html += '      <tr>';
            _html += '        <td class="center">' + data[x].co_funai + '</td>';
            _html += '        <td class="left">' + data[x].no_ti + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cs_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ni_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ne_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].sn_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_total) + '</td>';
            _html += '      </tr>';
        } // end iF

    } // end for;

    for (x = 0; x < data.length; x++) {

        if (data[x].nu_entrega == null) {
            _html += '      <tr>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td class="right strong" colspan="2">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cs_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ni_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ne_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].sn_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_total) + '</td>';
            _html += '      </tr>';
        } // end iF

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);
} // end function formatarDadosDeteccaoPorTerraIndigena

function formatarDadosDeteccaoPorMunicipio(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_deteccao_por_municipio">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center">Geocódigo</th>';
    _html += '        <th class="center">Munícipio</th>';
    _html += '        <th class="center">Corte Raso</th>';
    _html += '        <th class="center">Corte Seletivo</th>';
    _html += '        <th class="center">Nova Infra-estrutura</th>';
    _html += '        <th class="center">Nova Estrada</th>';
    _html += '        <th class="center">Seletivo Natural</th>';
    _html += '        <th class="center">Total</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        if (data[x].nu_entrega != null) {
            _html += '      <tr>';
            _html += '        <td class="center">' + data[x].nu_geocodigo + '</td>';
            _html += '        <td class="left">' + data[x].no_municipio + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].cs_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ni_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].ne_nu_area_ha) + '</td>';
            _html += '        <td class="right">' + formatarValor(data[x].sn_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_total) + '</td>';
            _html += '      </tr>';
        } // end iF

    } // end for;

    for (x = 0; x < data.length; x++) {

        if (data[x].nu_entrega == null) {
            _html += '      <tr>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td class="right strong" colspan="2">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cs_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ni_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ne_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].sn_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_total) + '</td>';
            _html += '      </tr>';
        } // end iF

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);
} // end function formatarDadosDeteccaoPorMunicipio

function formatarDadosDeteccaoPorTerraIndigenaMunicipio(data, objDiv) {
    var _html = '';

    _html += '<table class="table table-bordered table-striped table-condensed table-export" id="tb_deteccao_por_terra_indigena_municipio">';
    _html += '    <thead>';
    _html += '    <tr>';
    _html += '        <th class="center vertical-center">Código Funai</th>';
    _html += '        <th class="center vertical-center">Nome da Terra Indígena</th>';
    _html += '        <th class="center vertical-center">Fase da Terra Indígena</th>';
    _html += '        <th class="center vertical-center">Geocódigo</th>';
    _html += '        <th class="center vertical-center">Nome do Município</th>';
    _html += '        <th class="center vertical-center">Área do Município (ha)</th>';
    _html += '        <th class="center vertical-center">Área da Terra Indígena (ha)</th>';
    _html += '        <th class="center vertical-center" width="8%">Percentual da área da TI no Município(%)</th>';
    _html += '        <th class="center vertical-center">Corte Raso</th>';
    _html += '        <th class="center vertical-center">Corte Seletivo</th>';
    _html += '        <th class="center vertical-center">Nova Infra-estrutura</th>';
    _html += '        <th class="center vertical-center">Nova Estrada</th>';
    _html += '        <th class="center vertical-center">Seletivo Natural</th>';
    _html += '        <th class="center vertical-center" width="5%">Total</th>';
    _html += '    </tr>';
    _html += '    </thead>';
    _html += '    <tbody>';

    for (x = 0; x < data.length; x++) {

        _html += '      <tr>';

        if (data[x].nu_entrega != null) {
            _html += '        <td class="center vertical-center">' + data[x].co_funai + '</td>';
            _html += '        <td class="left vertical-center">' + data[x].no_ti + '</td>';
            _html += '        <td class="left vertical-center">' + data[x].ds_fase_ti + '</td>';
            _html += '        <td class="center vertical-center">' + data[x].nu_geocodigo + '</td>';
            _html += '        <td class="left vertical-center">' + data[x].no_municipio + '</td>';
            _html += '        <td class="right vertical-center">' + formatarValor(data[x].nu_area_municipio_ha) + '</td>';
            _html += '        <td class="right vertical-center">' + formatarValor(data[x].nu_area_ti_no_municipio_ha) + '</td>';
            _html += '        <td class="right vertical-center">' + formatarValor(data[x].nu_percentual_ti_no_municipio) + '</td>';
            _html += '        <td class="right vertical-center">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right vertical-center">' + formatarValor(data[x].cs_nu_area_ha) + '</td>';
            _html += '        <td class="right vertical-center">' + formatarValor(data[x].ni_nu_area_ha) + '</td>';
            _html += '        <td class="right vertical-center">' + formatarValor(data[x].ne_nu_area_ha) + '</td>';
            _html += '        <td class="right vertical-center">' + formatarValor(data[x].sn_nu_area_ha) + '</td>';
            _html += '        <td class="right strong vertical-center">' + formatarValor(data[x].nu_area_total) + '</td>';
        } else {
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td style="display: none" ></td>';
            _html += '        <td class="right strong" colspan="5">Total</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_municipio_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_ti_no_municipio_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_percentual_ti_no_municipio) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cr_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].cs_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ni_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].ne_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].sn_nu_area_ha) + '</td>';
            _html += '        <td class="right strong">' + formatarValor(data[x].nu_area_total) + '</td>';
        } // end iF

        _html += '      </tr>';

    } // end for;

    _html += '    </tbody>';

    _html += '</table>';

    objDiv.empty().html(_html);
} // end function formatarDadosDeteccaoPorTerraIndigenaMunicipio