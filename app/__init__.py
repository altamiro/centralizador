# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

import logging

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail, Message
from flask_script import Manager

mail = Mail()

app = Flask(__name__)
app.config.from_object("app.settings.configuration.DevelopmentConfig")

# Configure Mail
mail.init_app(app)
message = Message

# Configure database
db = SQLAlchemy(app)

# Configure Manager
manager = Manager(app)

# Configure logging
handler = logging.FileHandler(app.config["LOGGING_LOCATION"], app.config["LOGGING_FILEMODE"])
formatter = logging.Formatter(app.config["LOGGING_FORMAT"], app.config["LOGGING_DATEFMT"])
handler.setLevel(app.config["LOGGING_LEVEL"])
handler.setFormatter(formatter)
app.logger.addHandler(handler)

from app import views, models
