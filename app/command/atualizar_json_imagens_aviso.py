# -*- encoding: utf-8 -*-
# Script que envia email dos serviços inativos ou falha
# Copyright 2017 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

from pprint import pprint
from app import app, manager, db
from datetime import datetime
from ..models import VisaoEntregaAvisoDiario, EntregaAvisoDiario
from ..settings.utils import *


def atualizar_json_imagens():
    data = VisaoEntregaAvisoDiario.query.all()
    try:
        if data:
            for r in data:
                entrega = EntregaAvisoDiario.query.get(r.id)
                entrega.js_imagens = json_aviso_diario_imagens(r.id)
                db.session.add(entrega)
                db.session.commit()
                app.logger.info('ok')
    except Exception as e:
        app.logger.exception("Erro: %s", e)

# def teste():
#     try:
#         entrega_id = 97
#         entrega = EntregaAvisoDiario.query.get(entrega_id)
#         entrega.js_terra_indigena = json_aviso_diario_terra_indigena(
#             entrega_id)
#         entrega.js_coordenacao_regional = json_aviso_diario_coordenacao_regional(
#             entrega_id)
#         entrega.js_estado = json_aviso_diario_estado(entrega_id)
#         entrega.js_imagens = json_aviso_diario_imagens(entrega_id)
#         db.session.add(entrega)
#         db.session.commit()
#         app.logger.info('ok')
#     except Exception as e:
#         app.logger.exception("Erro: %s", e)

@manager.command
def atualizar_json_imagens_aviso():
    atualizar_json_imagens()
    # teste()
