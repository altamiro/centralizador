# -*- encoding: utf-8 -*-
# Script que faz o download e publica dados do CR que tem no geoserver da FUNAI
# Copyright 2017 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>
import wget
import psycopg2
import psycopg2.extras
import gdal

from osgeo import ogr
from datetime import datetime, timedelta
from os import mkdir, listdir, system
from os.path import join, exists
from unipath import Path
from app import app, manager, mail, message

from app.settings.utils import descompactar

from .sql import *

db = psycopg2.connect(app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
conn = db.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

# pega o caminho do arquivo temporario
diretorio_temporario = join(app.config['UPLOAD_FOLDER'], 'coordenacao_regional')


def baixar_arquivo():
    url = app.config['COORDENACAO_REGIONAL_URL']

    if not exists(diretorio_temporario):
        mkdir(diretorio_temporario)

    try:
        local_filename = join(diretorio_temporario, app.config['COORDENACAO_REGIONAL_ARQUIVO'])

        file = wget.download(url, local_filename)

        descompactar(file, diretorio_temporario)

    except Exception as e:
        app.logger.exception("Error ao baixar o arquivo da coordenacao_regional: %s", e)


def modificar_tabela():
    conn.execute("BEGIN")
    app.logger.info("BEGIN")

    try:
        conn.execute(DELETE_LOC_COORDENACAO_REGIONAL)
        conn.execute(ALTER_SEQUENCE_LOC_COORDENACAO_REGIONAL)
        conn.execute("COMMIT")
        app.logger.info("COMMIT")
    except (Exception, ValueError, psycopg2.DataError) as e:
        conn.execute("ROLLBACK")
        app.logger.exception("ROLLBACK")
        app.logger.exception("__exception_delete_alter_loc_coordenacao_regional_p__")
        app.logger.exception("error: %s", e)


def atualizar_coordenacao_regional():
    conn.execute("BEGIN")
    app.logger.info("BEGIN")

    app.logger.info("__inicio__atualizar_tabela_loc_coordenacao_regional_p__")

    # lista todos os arquivos *.shp
    for _shp_arquivo in sorted(listdir(diretorio_temporario)):

        if _shp_arquivo.lower().endswith('.shp'):
            arquivo_caminho_completo = join(diretorio_temporario, _shp_arquivo)
            gdal.SetConfigOption("SHAPE_ENCODING", "LATIN1")
            shapefile = ogr.Open(arquivo_caminho_completo)
            layer = shapefile.GetLayer()
            for feature in layer:
                s_co_cr = int(feature.GetField('undadm_cod'))
                s_no_cr = feature.GetField('undadm_nom')
                s_no_abreviado = feature.GetField('undadm_no0')
                s_sg_cr = str(feature.GetField('undadm_sig')).replace(" ", "")
                s_st_situacao = ['Inativo', 'Ativo'][feature.GetField('undadm_sit') == 'A']
                s_ds_email = feature.GetField('undadm_ema')
                s_no_municipio = feature.GetField('mun_nome')
                conn.execute(SELECT_LIM_UNIDADE_FEDERACAO, [feature.GetField('mun_uf_sig')])
                r = conn.fetchone()
                s_no_uf = r.no_uf
                s_no_regiao = r.no_regiao
                s_sg_uf = feature.GetField('mun_uf_sig')
                s_ds_telefone = feature.GetField('undadm_num')
                s_geom = feature.GetGeometryRef().ExportToWkt()

                try:

                    conn.execute(INSERT_LOC_COORDENACAO_REGIONAL,
                                 [s_co_cr, s_no_cr, s_no_abreviado, s_sg_cr, s_st_situacao, s_ds_email, s_no_regiao,
                                  s_no_municipio, s_no_uf, s_sg_uf, s_ds_telefone, s_geom])

                    app.logger.info("INSERT: no_cr: %s - no_abreviado: %s - sg_cr: %s", s_no_cr, s_no_abreviado,
                                    s_sg_cr)

                except (Exception, ValueError, psycopg2.DataError) as e:
                    conn.execute("ROLLBACK")
                    app.logger.exception("ROLLBACK")
                    app.logger.debug(
                        "s_co_cr: %s \n s_no_cr: %s \n s_no_abreviado: %s \n s_sg_cr: %s",
                        s_co_cr, s_no_cr, s_no_abreviado, s_sg_cr)
                    app.logger.exception("__exception_insert_loc_coordenacao_regional_p__")
                    app.logger.exception("error: %s", e)

    conn.execute("COMMIT")
    conn.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY aplicacao.vwm_tabela_informacao_atualizacao")
    app.logger.info("COMMIT")
    app.logger.info("__fim__atualizar_tabela_loc_coordenacao_regional_p__")


# funcao que envia email depois que finaliza
def disparar_email():
    conn.execute(SELECT_INFORMACAO_ATUALIZACAO_COORDENACAO_REGIONAL)
    r = conn.fetchone()

    if len(r) > 1:
        html = "<div style='font-family: Arial, Helvetica, sans-serif !important;font-size:12px !important;'>"
        html += "Informação da sincronização - Coordenação Regional(FUNAI).<br /><br />"
        html += "<table border='1' cellpadding='2' cellspacing='2' width='600px'>"
        html += "  <thead>"
        html += "    <tr>"
        html += "      <th align='center'>Data</th>"
        html += "      <th align='center'>Horário</th>"
        html += "      <th align='center'>Total</th>"
        html += "    </tr>"
        html += "  </thead>"
        html += "  <tbody>"

        html += "    <tr>"
        html += "      <td align='center'>%s</td>" % str(r.dt_atualizacao)
        html += "      <td align='center'>%s</td>" % str(r.hr_atualizacao)
        html += "      <td align='center' style='font-weight: bold !important;'>%s</td>" % int(r.qtd_registro)
        html += "    </tr>"

        html += "  </tbody>"
        html += "</table>"
        html += "<br/>"
        html += "<br/>"
        html += "<span style='font-size:11px !important;color:#808080 !important;'>"
        html += "Atenciosamente,"
        html += "<br/>"
        html += "<br/>"
        html += "Altamiro Rodrigues"
        html += "<br/>"
        html += "Administrador de Banco de Dados"
        html += "<br/>"
        html += "altamiro.rodrigues@hexgis.com"
        html += "<br/>"
        html += "<span style='font-weight: bold !important;'><span style='color:#e56605 !important;'>HEX</span> <span style='color:#2a3d51 !important;'>- Tecnologias Geoespaciais</span></span>"
        html += "</span>"
        html += "</div>"

        msg = message(
            "[HEX/FUNAI] Coordenação Regional - {}".format(datetime.now().strftime('%d/%m/%Y - %H:%M:%S')),
            recipients=[app.config['EMAIL_GABRIEL'], app.config['EMAIL_MAIRA']],
            sender=app.config['EMAIL_SENDER'],
            reply_to=app.config['EMAIL_GABRIEL'],
            html=html
        )

        try:
            mail.send(msg)
        except Exception as e:
            app.logger.exception("Erro ao enviar email: %s", e)


@manager.command
def coordenacao_regional():
    if exists(diretorio_temporario):
        Path(diretorio_temporario).rmtree()

    if not exists(diretorio_temporario):
        mkdir(diretorio_temporario)

    baixar_arquivo()
    modificar_tabela()  # executa modificacoes nas tabelas de terra indigena e belo monte
    atualizar_coordenacao_regional()
    disparar_email()

    if exists(diretorio_temporario):
        Path(diretorio_temporario).rmtree()

    # fecha a conexao do cursor
    conn.close()
    # fecha a conexao do banco
    db.close()
