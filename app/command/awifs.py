# -*- encoding: utf-8 -*-
# Script que faz o download e publica dados da Terra Indigenas
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>
import wget
import psycopg2
import psycopg2.extras
import gdal

from osgeo import ogr
from datetime import datetime, timedelta
from os import mkdir, listdir, system
from os.path import join, exists
from unipath import Path
from app import app, manager, mail, message

from app.settings.utils import descompactar

from .sql import *

db = psycopg2.connect(app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
conn = db.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

# pega o caminho do arquivo temporario
diretorio_temporario = join(app.config['UPLOAD_FOLDER'], 'awifs')

def baixar_arquivo(data):
    url = "http://terrabrasilis.info/deterb/wms?request=GetFeature&service=wfs&version=1.1.0&outputformat=SHAPE-ZIP&typename=DETER-B:deter_5_days_old&srsName=EPSG:4674&CQL_FILTER=date='{}' AND sensor='AWiFS'".format(data)

    filename = "awifs_{}.zip".format(str(data).replace("-","_"))
    if not exists(diretorio_temporario):
        mkdir(diretorio_temporario)

    # try:
    local_filename = join(diretorio_temporario, filename)

    file = wget.download(url, local_filename)
    app.logger.debug(wget.get_console_width())

    #     descompactar(file, diretorio_temporario)
    #
    # except Exception as e:
    #     app.logger.exception("Error ao baixar o arquivo da awifs: %s", e)

@manager.command
def awifs2():
    if exists(diretorio_temporario):
        Path(diretorio_temporario).rmtree()

    if not exists(diretorio_temporario):
        mkdir(diretorio_temporario)

    conn.execute("SELECT generate_series('2017-08-29', to_char(NOW(),'YYYY-MM-DD')::date, '1 day')::date AS dt_monitorada LIMIT 1")
    resultSet = conn.fetchall()

    if len(resultSet) > 0:
        for r in resultSet:
            baixar_arquivo(r.dt_monitorada)
