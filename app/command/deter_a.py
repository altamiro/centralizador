# -*- encoding: utf-8 -*-
# Script que faz o download e publica dados do Alerta MODIS - "DETER A"
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>
import requests
import psycopg2
import psycopg2.extras
import gdal

from osgeo import ogr
from bs4 import BeautifulSoup
from requests.auth import HTTPBasicAuth
from datetime import datetime, timedelta
from clint.textui import progress
from os import mkdir, listdir, system
from os.path import join, exists
from unipath import Path
from app import app, manager, mail, message

from app.settings.utils import descompactar

from .sql import *

db = psycopg2.connect(app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
conn = db.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

descricao_mes = {
    1: 'Janeiro',
    2: 'Fevereiro',
    3: 'Marco',
    4: 'Abril',
    5: 'Maio',
    6: 'Junho',
    7: 'Julho',
    8: 'Agosto',
    9: 'Setembro',
    10: 'Outubro',
    11: 'Novembro',
    12: 'Dezembro'
}

lista_mes_retorna_numero = {
    'janeiro': '01',
    'fevereiro': '02',
    'marco': '03',
    'abril': '04',
    'maio': '05',
    'junho': '06',
    'julho': '07',
    'agosto': '08',
    'setembro': '09',
    'outubro': '10',
    'novembro': '11',
    'dezembro': '12'
}

lista_mes_en = {
    'Jan': 1,
    'Feb': 2,
    'Mar': 3,
    'Apr': 4,
    'May': 5,
    'Jun': 6,
    'Jul': 7,
    'Aug': 8,
    'Sep': 9,
    'Oct': 10,
    'Nov': 11,
    'Dec': 12
}

# pega o caminho do arquivo temporario
diretorio_temporario = join(app.config['UPLOAD_FOLDER'], 'deter')

target_url = app.config['DETER_URL']

# define a data de 8 dias anterior, pegando a data atual como base
two_days_ago = datetime.now() - timedelta(days=3)


def listar_arquivos_zip_servidor():
    soup = BeautifulSoup(
        requests.get(target_url, auth=HTTPBasicAuth(app.config['DETER_USERNAME'], app.config['DETER_PASSWORD'])).text,
        'html.parser')
    hr = soup.find('hr')
    for line in str(hr).split('\n'):
        if line.startswith('<img'):
            line = BeautifulSoup(line, 'html.parser')
            if line.img['src'] == '/icons/compressed.gif':
                record = {}
                line_x = line.img.getText().strip().split(' ')

                indexes_length = 0
                indexes_year = 0
                indexes_hm = 0
                for i, x in enumerate(line_x):
                    if x.endswith('M') or x.endswith('k'):
                        indexes_length = i
                    if x.endswith(two_days_ago.strftime('%Y')):
                        indexes_year = i
                    if x.endswith(':', 1, 3):
                        indexes_hm = i

                day, month, year = line_x[indexes_year].split('-')
                hour, minute = line_x[indexes_hm].split(':')
                filedate = datetime(int(year), int(lista_mes_en[month]), int(day), int(hour), int(minute))
                record['date_file'] = filedate.strftime('%d/%m/%Y %H:%M')
                record['date_base'] = two_days_ago.strftime('%d/%m/%Y %H:%M')
                record['url'] = "%s%s" % (target_url, line_x[0])
                record['length'] = line_x[indexes_length]
                name, data, shp = line_x[0].split('_')
                record['file_path_name'] = data
                # verifica se a data do arquivo e' maior ou igual a data da variavel two_days_ago
                if filedate >= two_days_ago:
                    yield record


def baixar_arquivo(record):
    try:
        if record['url'].endswith('.zip'):

            app.logger.info('Baixando arquivo %s - %s', record['url'], record['length'])

            local_filename = diretorio_temporario + '/' + record['url'].split('/')[-1]
            r = requests.get(record['url'], stream=True,
                             auth=HTTPBasicAuth(app.config['DETER_USERNAME'], app.config['DETER_PASSWORD']))
            with open(local_filename, 'wb') as f:
                total_length = int(r.headers.get('content-length'))
                for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length / 1024) + 1):
                    if chunk:
                        f.write(chunk)
                        f.flush()

            app.logger.info('=================== INICIO DESCOMPACTADO ====================')
            # lista todos os arquivos *.zip
            for _zip_arquivo in listdir(diretorio_temporario):

                if _zip_arquivo.lower().endswith('.zip'):
                    arquivo_caminho_completo = diretorio_temporario + '/' + _zip_arquivo
                    descompactar(arquivo_caminho_completo, diretorio_temporario)
                    app.logger.info("Arquivo %s descompactado", _zip_arquivo)
                    Path(arquivo_caminho_completo).remove()

            app.logger.info('=================== DESCOMPACTADO SUCESSO ===================')

            for arquivo in listdir(diretorio_temporario):
                if arquivo.lower().find("indicio") > 0:
                    arquivo_caminho_completo = diretorio_temporario + '/' + arquivo
                    Path(arquivo_caminho_completo).remove()
                elif arquivo.lower().find("nuvem") > 0:
                    arquivo_caminho_completo = diretorio_temporario + '/' + arquivo
                    Path(arquivo_caminho_completo).remove()
                elif arquivo.lower().endswith(".txt") > 0:
                    arquivo_caminho_completo = diretorio_temporario + '/' + arquivo
                    Path(arquivo_caminho_completo).remove()

    except Exception as e:
        app.logger.exception("Error ao baixar o arquivo awifs: %s \n error: %s", record['url'], e)


def apagar_todos_dados_caso_tenha_erro(dt_arquivo):
    conn.execute(DELETE_IMG_ALERTA_DETER_A, [app.config['DETER_TIPO'], dt_arquivo])
    conn.execute(DELETE_DETER_DOWNLOAD_DETER_A_B, [app.config['DETER_TIPO'], dt_arquivo])


def download_deter_a_b_id(dt_arquivo):
    conn.execute(QUERY_DETER_BUSCAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA,
                 [app.config['DETER_TIPO'], dt_arquivo])
    nu_id = conn.fetchone().id
    return nu_id


def inserir_novo_download_deter_a_b(dt_arquivo):
    try:
        conn.execute(INSERT_DETER_DOWNLOAD_DETER_A_B, [app.config['DETER_TIPO'], dt_arquivo])
        db.commit()

        return download_deter_a_b_id(dt_arquivo)
    except Exception as e:
        db.rollback()
        app.logger.exception("Erro ao inserir na tabela tb_download_deter_a_b: %s", e)


def atualizar_download_deter_a_b(dt_arquivo):
    try:
        conn.execute(UPDATE_DETER_DOWNLOAD_DETER_A_B, [app.config['DETER_TIPO'], dt_arquivo])
        db.commit()
    except Exception as e:
        db.rollback()
        app.logger.exception("Erro ao efetuar atualizacao na tabela tb_download_deter_a_b: %s", e)


def buscar_download_deter_a_b_id(dt_arquivo):
    conn.execute(QUERY_DETER_CONTAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA,
                 [app.config['DETER_TIPO'], dt_arquivo])
    total = conn.fetchone().total

    if total > 0:
        return download_deter_a_b_id(dt_arquivo)
    else:
        return inserir_novo_download_deter_a_b(dt_arquivo)


# funcao que processa os dado do awifs deter b
def processar_dados_deter_a():
    # lista todos os arquivos *.shp
    for arquivo in sorted(listdir(diretorio_temporario)):

        if arquivo.lower().endswith('.shp') and arquivo.lower().find("alerta") > 0:
            arquivo_caminho_completo = diretorio_temporario + '/' + arquivo
            gdal.SetConfigOption("SHAPE_ENCODING", "LATIN1")
            shapefile = ogr.Open(arquivo_caminho_completo)
            layer = shapefile.GetLayer()

            conn.execute("BEGIN")
            app.logger.info("BEGIN")
            _boo_success = False

            try:

                for feature in layer:
                    geom = feature.GetGeometryRef().ExportToWkt()
                    _data = feature.GetField('view_date').replace('/', '').replace('-', '')
                    v_no_tipo = app.config['DETER_TIPO']
                    v_no_classe = feature.GetField('class_name')
                    i_nu_cena = int(feature.GetField('scene_id'))

                    d_dt_deteccao = "%s-%s-%s" % (_data[:4], _data[4:6], _data[6:8])

                    conn.execute(QUERY_DETER_VERIFICAR_DATA_FINALIZADA, [d_dt_deteccao])
                    rs = conn.fetchone()

                    if rs.total > 0:
                        apagar_todos_dados_caso_tenha_erro(d_dt_deteccao)

                    v_ds_mes = descricao_mes[int(_data[4:6])]
                    i_nu_ano = int(_data[:4])
                    v_no_municipio = ''
                    c_sg_uf = ''

                    # executa uma query para pegar o municipio e uf baseado no cruzamento do dado
                    conn.execute(QUERY_CRUZAMENTO_UF_MUNICIPIO, [geom])
                    rs = conn.fetchone()
                    if rs:
                        v_no_municipio = rs.no_municipio
                        c_sg_uf = rs.sg_uf

                    tb_download_deter_a_b_id = buscar_download_deter_a_b_id(d_dt_deteccao)
                    conn.execute(INSERT_IMG_DETER_AWIFS_A,
                                 [tb_download_deter_a_b_id, v_no_tipo, v_no_classe, i_nu_cena, v_ds_mes, i_nu_ano,
                                  v_no_municipio, c_sg_uf, d_dt_deteccao, geom])

                    app.logger.info(
                        "INSERT: tb_download_deter_a_b_id: %s \n v_no_tipo: %s \n v_no_classe: %s \n i_nu_cena: %s \n v_ds_mes: %s \n i_nu_ano: %s \n v_no_municipio: %s \n c_sg_uf: %s \n d_dt_deteccao: %s",
                        tb_download_deter_a_b_id, v_no_tipo, v_no_classe, i_nu_cena, v_ds_mes, i_nu_ano, v_no_municipio,
                        c_sg_uf, d_dt_deteccao)

                    _boo_success = True

            except (Exception, ValueError, psycopg2.DataError) as e:
                conn.execute("ROLLBACK")
                app.logger.info("ROLLBACK")
                apagar_todos_dados_caso_tenha_erro(d_dt_deteccao)
                _boo_success = False
                app.logger.exception('=================== OCORREU UM ERRO AO INSERIR ARQUIVO %s ====================',
                                     arquivo)
                app.logger.exception("Erro ao inserir na tabela img_alerta_deter_a: %s", e)
                app.logger.exception(
                    "tb_download_deter_a_b_id: %s \n v_no_tipo: %s \n v_no_classe: %s \n i_nu_cena: %s \n v_ds_mes: %s \n i_nu_ano: %s \n v_no_municipio: %s \n c_sg_uf: %s \n d_dt_deteccao: %s",
                    tb_download_deter_a_b_id, v_no_tipo, v_no_classe, i_nu_cena, v_ds_mes, i_nu_ano, v_no_municipio,
                    c_sg_uf, d_dt_deteccao)

            if _boo_success:
                conn.execute(UPDATE_DETER_DOWNLOAD_DETER_A_B)
                conn.execute("COMMIT")
                app.logger.info("COMMIT")
                app.logger.info("Os dados do arquivo %s, foi processado com sucesso.", arquivo)


# funcao que envia email depois que finaliza
def disparar_email():
    conn.execute(QUERY_TODOS_DOWNLOAD_DETER_A_B, [app.config['DETER_TIPO']])
    resultSet = conn.fetchall()

    if len(resultSet) > 0:
        html = "<div style='font-family: Arial, Helvetica, sans-serif !important;font-size:12px !important;'>"
        html += "Informação da sincronização dos dados do DETER-A 'Alerta MODIS'.<br /><br />"
        html += "<table border='1' cellpadding='2' cellspacing='2' width='600px'>"
        html += "  <thead>"
        html += "    <tr>"
        html += "      <th align='center'>Descricao</th>"
        html += "      <th align='center'>Data do Shape</th>"
        html += "      <th align='center'>Quantidade Importada</th>"
        html += "    </tr>"
        html += "  </thead>"
        html += "  <tbody>"

        for rs in resultSet:
            html += "    <tr>"
            html += "      <td align='center'>%s</td>" % rs.no_tipo
            html += "      <td align='center'>%s</td>" % rs.dt_shape
            html += "      <td align='center'>%s</td>" % rs.qt_total_registro
            html += "    </tr>"

        html += "  </tbody>"
        html += "</table>"
        html += "<br/>"
        html += "<br/>"
        html += "<span style='font-size:11px !important;color:#808080 !important;'>"
        html += "Atenciosamente,"
        html += "<br/>"
        html += "<br/>"
        html += "Altamiro Rodrigues"
        html += "<br/>"
        html += "Administrador de Banco de Dados"
        html += "<br/>"
        html += "altamiro.rodrigues@hexgis.com"
        html += "<br/>"
        html += "<span style='font-weight: bold !important;'><span style='color:#e56605 !important;'>HEX</span> <span style='color:#2a3d51 !important;'>- Tecnologias Geoespaciais</span></span>"
        html += "</span>"
        html += "</div>"

        msg = message(
            "[HEX/FUNAI] DETER-A(MODIS) - Sincronizacao dos dados - {}".format(
                datetime.now().strftime('%d/%m/%Y - %H:%M:%S')),
            recipients=[app.config['EMAIL_GABRIEL'], app.config['EMAIL_MAIRA']],
            sender=app.config['EMAIL_SENDER'],
            reply_to=app.config['EMAIL_GABRIEL'],
            html=html
        )

        try:
            mail.send(msg)
        except Exception as e:
            app.logger.exception("Erro ao enviar email: %s", e)


@manager.command
def deter():
    if exists(diretorio_temporario):
        Path(diretorio_temporario).rmtree()

    if not exists(diretorio_temporario):
        mkdir(diretorio_temporario)

    for record in listar_arquivos_zip_servidor():
        baixar_arquivo(record)

    processar_dados_deter_a()
    conn.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY aplicacao.vwm_tabela_informacao_atualizacao")
    conn.execute("VACUUM FULL aplicacao.vwm_tabela_informacao_atualizacao")
    conn.execute("VACUUM ANALYZE aplicacao.vwm_tabela_informacao_atualizacao")
    disparar_email()

    if exists(diretorio_temporario):
        Path(diretorio_temporario).rmtree()

    # fecha a conexao do cursor
    conn.close()
    # fecha a conexao do banco
    db.close()
