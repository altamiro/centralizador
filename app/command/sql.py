# -*- encoding: utf-8 -*-
# arquivo onde tem todas variaveis de SQL
# Copyright 2016 Altamiro Rodrigues

# verificar se existe registro com o tipo e data informado
QUERY_CONTAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA = "SELECT COUNT(id) AS total FROM funai.tb_download_deter_a_b WHERE LOWER(no_tipo) = LOWER(TRIM(%s)) AND dt_arquivo = %s"

# busca as informacoes do download de acordo com o tipo e data informado
QUERY_BUSCAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA = "SELECT id, no_tipo, dt_arquivo, st_finalizado, qt_total_registro FROM funai.tb_download_deter_a_b WHERE LOWER(no_tipo) = LOWER(TRIM(%s)) AND dt_arquivo = %s"

# insert da tabela tb_download_deter_a_b
INSERT_DOWNLOAD_DETER_A_B = "INSERT INTO funai.tb_download_deter_a_b(no_tipo, dt_arquivo) VALUES (TRIM(%s),%s)"

# update da tabela tb_download_deter_a_b
UPDATE_DOWNLOAD_DETER_A_B = "UPDATE funai.tb_download_deter_a_b AS d SET st_finalizado='t', dt_atualizacao=NOW(), qt_total_registro=(SELECT COUNT(id) AS qt_total_registro FROM funai.img_alerta_awifs_a WHERE tb_download_deter_a_b_id = d.id) WHERE LOWER(d.no_tipo) = LOWER(TRIM(%s)) AND d.dt_arquivo = %s"

# insert da tabela img_alerta_awifs_a
INSERT_IMG_ALERTA_AWIFS_A = "INSERT INTO funai.img_alerta_awifs_a(tb_download_deter_a_b_id, no_tipo, nu_lote, no_classe, st_quadrante, nu_orbita_ponto, ds_mes, nu_ano, no_municipio, sg_uf, no_uc, dt_deteccao, geom) VALUES (%s, TRIM(%s), %s, TRIM(%s), %s, %s, TRIM(%s), %s, TRIM(%s), TRIM(%s), TRIM(%s), %s, ST_Multi(ST_Setsrid(ST_Buffer(ST_Geometryfromtext(%s),0),4674)))"

# update da tabela tb_download_deter_a_b do tipo awifs
UPDATE_DOWNLOAD_DETER_A_B_AWIFS = "UPDATE funai.tb_download_deter_a_b AS d SET qt_total_registro=(SELECT COUNT(id) AS qt_total_registro FROM funai.img_alerta_awifs_a AS a WHERE a.dt_deteccao = d.dt_arquivo), dt_atualizacao=NOW(), st_finalizado = 't' WHERE d.no_tipo = 'Alerta AWiFS' AND st_finalizado = 'f' AND to_char(dt_cadastro,'YYYY-MM-DD') = to_char(NOW(),'YYYY-MM-DD')"

# select que faz o cruzamento do dados de acordo com a geometria informada
QUERY_CRUZAMENTO_UF_MUNICIPIO = "SELECT um.no_municipio, um.no_uf, um.sg_uf FROM cb.vw_lim_municipio_a_lim_unidade_federacao_a AS um WHERE ST_IsValid(um.m_geom) AND ST_Intersects(ST_Multi(ST_Setsrid(ST_Buffer(ST_Geometryfromtext(%s),0),4674)), um.m_geom)"

# select que pega todos dados que foi feito download do dia de acordo com tipo
QUERY_TODOS_DOWNLOAD_DETER_A_B = "SELECT no_tipo, to_char(dt_arquivo,'DD/MM/YYYY') AS dt_shape, nu_lote, qt_total_registro FROM funai.tb_download_deter_a_b WHERE LOWER(no_tipo) = LOWER(TRIM(%s)) AND st_finalizado = 't' AND to_char(dt_cadastro,'YYYY-MM-DD') = to_char(now(),'YYYY-MM-DD') ORDER BY dt_arquivo DESC"

# delete da tabela img_alerta_awifs_a
DELETE_IMG_ALERTA_AWIFS_A = "DELETE FROM funai.img_alerta_awifs_a WHERE tb_download_deter_a_b_id = (SELECT id FROM funai.tb_download_deter_a_b WHERE LOWER(no_tipo) = LOWER(TRIM(%s)) AND dt_arquivo = %s)"

# delete da tabela tb_download_deter_a_b
DELETE_DOWNLOAD_DETER_A_B = "DELETE FROM funai.tb_download_deter_a_b WHERE LOWER(no_tipo) = LOWER(TRIM(%s)) AND dt_arquivo = %s"

# delete da tabela lim_terra_indigena_a
DELETE_LIM_TERRA_INDIGENA = "DELETE FROM funai.lim_terra_indigena_a"

# alterar a sequence da tabela lim_terra_indigena_a
ALTER_SEQUENCE_LIM_TERRA_INDIGENA = "ALTER SEQUENCE funai.lim_terra_indigena_a_id_seq RESTART WITH 1"

# inserir dados na tabela lim_terra_indigena_a
INSERT_LIM_TERRA_INDIGENA = "INSERT INTO funai.lim_terra_indigena_a(no_ti, no_grupo_etnico, ds_fase_ti, ds_modalidade, ds_reestudo_ti, ds_cr, no_municipio, sg_uf, co_funai, nu_area_ha, geom, st_faixa_fronteira, dt_em_estudo, ds_portaria_em_estudo, dt_delimitada, ds_despacho_delimitada, dt_declarada, ds_portaria_declarada, dt_homologada, ds_decreto_homologada, dt_regularizada, ds_matricula_regularizada, ds_doc_resumo_em_estudo, ds_doc_resumo_delimitada, ds_doc_resumo_declarada, ds_doc_resumo_homologada, ds_doc_resumo_regularizada, co_cr) VALUES ( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, ST_Multi(ST_Setsrid(ST_Geometryfromtext(%s), 4674)), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

# delete da tabela lim_belo_monte_terra_indigena_a
DELETE_LIM_BELO_MONTE_TERRA_INDIGENA = "DELETE FROM funai.lim_belo_monte_terra_indigena_a"

# inserir os dados na tabela lim_belo_monte_terra_indigena_a
INSERT_BELO_MONTE_TERRA_INDIGENA = "INSERT INTO funai.lim_belo_monte_terra_indigena_a(co_funai, no_ti, no_grupo_etnico, ds_fase_ti, ds_modalidade, ds_reestudo_ti, ds_cr, no_municipio, sg_uf, nu_area_ha, geom) SELECT co_funai, no_ti, no_grupo_etnico, ds_fase_ti, ds_modalidade, ds_reestudo_ti, ds_cr, no_municipio, sg_uf, nu_area_ha, ST_Multi(ST_Setsrid(ST_Buffer(geom,0.0),4674)) FROM funai.lim_terra_indigena_a WHERE co_funai IN (3002,3201,3801,7601,21501,23201,32602,38902,46201,50601,60001,72601,62001)"

# select que pega as informacaoes da atualizacao da terra indigena
SELECT_INFORMACAO_ATUALIZACAO_TERRA_INDIGENA = "SELECT dt_atualizacao, ds_observacao, nu_total FROM aplicacao.vw_atualizacao_terra_indigena"

# verificar se existe registro com o tipo e data informado
QUERY_DETER_CONTAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA = "SELECT COUNT(id) AS total FROM funai.tb_download_deter_a_b WHERE LOWER(no_tipo) = LOWER(TRIM(%s)) AND dt_arquivo = %s"

# busca as informacoes do download de acordo com o tipo e data informado
QUERY_DETER_BUSCAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA = "SELECT id, no_tipo, dt_arquivo, st_finalizado, qt_total_registro FROM funai.tb_download_deter_a_b WHERE LOWER(no_tipo) = LOWER(TRIM(%s)) AND dt_arquivo = %s"

# delete da tabela img_alerta_deter_a
DELETE_IMG_ALERTA_DETER_A = "DELETE FROM funai.img_alerta_deter_a WHERE tb_download_deter_a_b_id = (SELECT id FROM funai.tb_download_deter_a_b WHERE LOWER(no_tipo) = LOWER(TRIM(%s)) AND dt_arquivo = %s)"

# delete da tabela tb_download_deter_a_b
DELETE_DETER_DOWNLOAD_DETER_A_B = "DELETE FROM funai.tb_download_deter_a_b WHERE LOWER(no_tipo) = LOWER(TRIM(%s)) AND dt_arquivo = %s"

# insert da tabela tb_download_deter_a_b
INSERT_DETER_DOWNLOAD_DETER_A_B = "INSERT INTO funai.tb_download_deter_a_b(no_tipo, dt_arquivo) VALUES (TRIM(%s),%s)"

# update da tabela tb_download_deter_a_b do tipo deter
UPDATE_DETER_DOWNLOAD_DETER_A_B = "UPDATE funai.tb_download_deter_a_b AS d SET qt_total_registro=(SELECT COUNT(id) AS qt_total_registro FROM funai.img_alerta_deter_a AS a WHERE a.dt_deteccao = d.dt_arquivo), dt_atualizacao=NOW(), st_finalizado = 't' WHERE d.no_tipo = 'Alerta DETER' AND st_finalizado = 'f' AND to_char(dt_cadastro,'YYYY-MM-DD') = to_char(NOW(),'YYYY-MM-DD')"

# insert da tabela img_alerta_deter_a
INSERT_IMG_DETER_AWIFS_A = "INSERT INTO funai.img_alerta_deter_a(tb_download_deter_a_b_id, no_tipo, no_classe, nu_cena, ds_mes, nu_ano, no_municipio, sg_uf, dt_deteccao, geom) VALUES (%s, TRIM(%s), TRIM(%s), %s, TRIM(%s), %s, TRIM(%s), TRIM(%s), %s, ST_Multi(ST_Setsrid(ST_Buffer(ST_Geometryfromtext(%s),0),4674)))"

QUERY_DETER_VERIFICAR_DATA_FINALIZADA = "SELECT COUNT(id)::INTEGER AS total FROM funai.tb_download_deter_a_b WHERE no_tipo = 'Alerta DETER' AND st_finalizado = 't' AND dt_arquivo = %s"

QUERY_AWIFS_VERIFICAR_DATA_FINALIZADA = "SELECT COUNT(id)::INTEGER AS total FROM funai.tb_download_deter_a_b WHERE no_tipo = 'Alerta AWiFS' AND st_finalizado = 't' AND dt_arquivo = %s"

# delete da tabela loc_coordenacao_regional_p
DELETE_LOC_COORDENACAO_REGIONAL = "DELETE FROM funai.loc_coordenacao_regional_p"

# alterar a sequence da tabela loc_coordenacao_regional_p
ALTER_SEQUENCE_LOC_COORDENACAO_REGIONAL = "ALTER SEQUENCE funai.loc_coordenacao_regional_p_id_seq RESTART WITH 1"

# insert da tabela loc_coordenacao_regional_p
INSERT_LOC_COORDENACAO_REGIONAL = "INSERT INTO funai.loc_coordenacao_regional_p(co_cr, no_cr, no_abreviado, sg_cr, st_situacao, ds_email, no_regiao, no_municipio, no_uf, sg_uf, ds_telefone, geom) VALUES (%s, TRIM(%s), TRIM(%s), TRIM(%s), TRIM(%s), TRIM(%s), TRIM(%s), TRIM(%s), TRIM(%s), TRIM(%s), TRIM(%s), ST_Setsrid(ST_Geometryfromtext(%s),4674))"

# select que pega as informacaoes da atualizacao da coordenacao regional
SELECT_INFORMACAO_ATUALIZACAO_COORDENACAO_REGIONAL = "SELECT dt_atualizacao, hr_atualizacao, qtd_registro FROM aplicacao.vwm_tabela_informacao_atualizacao WHERE no_tabela = 'loc_coordenacao_regional_p'"

# busca o nome da unidade da federacao de acordo com a UF
SELECT_LIM_UNIDADE_FEDERACAO = "SELECT CASE WHEN u.sg_uf IN ('MS','MT','TO') THEN 'Centro-Oeste' WHEN u.sg_uf IN ('AL','BA','CE','MA') THEN 'Nordeste' WHEN u.sg_uf IN ('AC','AM','AP','PA','RO','RR') THEN 'Norte' WHEN u.sg_uf IN ('MG','SP') THEN 'Sudeste' WHEN u.sg_uf IN ('RS','SC') THEN 'Sul' END::varchar(12) AS no_regiao, u.no_uf FROM cb.lim_unidade_federacao_a AS u WHERE LOWER(TRIM(u.sg_uf)) = LOWER(TRIM(%s))"

# delete da tabela loc_aldeia_indigena_p
DELETE_LOC_ALDEIA_INDIGENA = "DELETE FROM funai.loc_aldeia_indigena_p"

# select que pega as informacaoes da atualizacao da aldeia indigena
SELECT_INFORMACAO_ATUALIZACAO_ALDEIA_INDIGENA = "SELECT dt_atualizacao, hr_atualizacao, qtd_registro FROM aplicacao.vwm_tabela_informacao_atualizacao WHERE no_tabela = 'loc_aldeia_indigena_p'"

# insert da tabela loc_aldeia_indigena_p
INSERT_LOC_ALDEIA_INDIGENA = "INSERT INTO funai.loc_aldeia_indigena_p(no_aldeia, terra_indigena_co_funai, municipio_nu_geocodigo, ds_cr, no_municipio, no_uf, geom) VALUES (TRIM(%s), %s, %s, TRIM(%s), TRIM(%s), TRIM(%s), ST_Setsrid(ST_Geometryfromtext(%s),4674))"

# delete da tabela loc_terra_indigena_estudo_p
DELETE_LIM_TERRA_INDIGENA_ESTUDO = "DELETE FROM funai.loc_terra_indigena_estudo_p"

# alterar a sequence da tabela loc_terra_indigena_estudo_p
ALTER_SEQUENCE_LIM_TERRA_INDIGENA_ESTUDO = "ALTER SEQUENCE funai.loc_terra_indigena_estudo_p_id_seq RESTART WITH 1"

# inserir dados na tabela loc_terra_indigena_estudo_p
INSERT_LIM_TERRA_INDIGENA_ESTUDO = "INSERT INTO funai.loc_terra_indigena_estudo_p(co_funai, no_ti, no_grupo_etnico, ds_fase_ti, ds_modalidade, ds_reestudo_ti, no_municipio, sg_uf, st_faixa_fronteira, dt_em_estudo, ds_portaria_em_estudo, dt_delimitada, ds_despacho_delimitada, dt_declarada, ds_portaria_declarada, dt_homologada, ds_decreto_homologada, dt_regularizada, ds_matricula_regularizada, ds_doc_resumo_em_estudo, ds_doc_resumo_delimitada, ds_doc_resumo_declarada, ds_doc_resumo_homologada, ds_doc_resumo_regularizada, nu_area_ha, geom) VALUES ( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, ST_Setsrid(ST_PointFromText(%s), 4674))"


# select que pega as informacaoes da atualizacao da terra indigena estudo
SELECT_INFORMACAO_ATUALIZACAO_TERRA_INDIGENA_ESTUDO = "SELECT dt_atualizacao, ds_observacao, nu_total FROM aplicacao.vw_atualizacao_terra_indigena_estudo"
