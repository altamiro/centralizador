# -*- encoding: utf-8 -*-
# Script que faz o download e publica dados da Terra Indigenas
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>
import wget
import psycopg2
import psycopg2.extras
import gdal

from osgeo import ogr
from datetime import datetime, timedelta
from os import mkdir, listdir, system
from os.path import join, exists
from unipath import Path
from app import app, manager, mail, message

from app.settings.utils import descompactar

from .sql import *

db = psycopg2.connect(app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
conn = db.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

# pega o caminho do arquivo temporario
diretorio_temporario = join(
    app.config['UPLOAD_FOLDER'], 'terra_indigena_estudo')


def baixar_arquivo():
    url = app.config['TERRA_INDIGENA_ESTUDO_URL']

    if not exists(diretorio_temporario):
        mkdir(diretorio_temporario)

    try:
        local_filename = join(diretorio_temporario,
                              app.config['TERRA_INDIGENA_ESTUDO_ARQUIVO'])

        file = wget.download(url, local_filename)

        descompactar(file, diretorio_temporario)

    except Exception as e:
        app.logger.exception("Error ao baixar o arquivo da terra indigena: %s", e)


def modificar_tabela():
    conn.execute("BEGIN")
    app.logger.info("BEGIN")

    try:
        conn.execute(DELETE_LIM_TERRA_INDIGENA_ESTUDO)
        conn.execute(ALTER_SEQUENCE_LIM_TERRA_INDIGENA_ESTUDO)
        conn.execute("COMMIT")
        app.logger.info("COMMIT")
    except (Exception, ValueError, psycopg2.DataError) as e:
        conn.execute("ROLLBACK")
        app.logger.exception("ROLLBACK")
        app.logger.exception("__exception_delete_alter_lim_terra_indigena_estudo_p__")
        app.logger.exception("error: %s", e)


def atualizar_terra_indigena_estudo():
    conn.execute("BEGIN")
    app.logger.info("BEGIN")

    app.logger.info("__inicio__atualizar_tabela_terra_indigena__")

    # lista todos os arquivos *.shp
    for _shp_arquivo in sorted(listdir(diretorio_temporario)):

        if _shp_arquivo.lower().endswith('.shp'):
            arquivo_caminho_completo = join(diretorio_temporario, _shp_arquivo)
            gdal.SetConfigOption("SHAPE_ENCODING", "LATIN1")
            shapefile = ogr.Open(arquivo_caminho_completo)
            layer = shapefile.GetLayer()
            for feature in layer:
                s_no_ti = feature.GetField('terrai_nom')
                s_no_grupo_etnico = feature.GetField('etnia_nome')
                s_ds_fase_ti = feature.GetField('fase_ti')
                s_ds_modalidade = feature.GetField('modalidade')
                s_ds_reestudo_ti = feature.GetField('reestudo_t')
                s_no_municipio = feature.GetField('municipio_')
                s_sg_uf = feature.GetField('uf_sigla')
                s_co_funai = feature.GetField('terrai_cod')
                s_nu_area_ha = feature.GetField('superficie')
                s_geom = feature.GetGeometryRef().ExportToWkt()
                s_st_faixa_fronteira = feature.GetField('faixa_fron')
                s_dt_em_estudo = feature.GetField('data_em_es')
                s_ds_portaria_em_estudo = feature.GetField('tit_em_est')
                s_dt_delimitada = feature.GetField('data_delim')
                s_ds_despacho_delimitada = feature.GetField('tit_delimi')
                s_dt_declarada = feature.GetField('data_decla')
                s_ds_portaria_declarada = feature.GetField('tit_declar')
                s_dt_homologada = feature.GetField('data_homol')
                s_ds_decreto_homologada = feature.GetField('tit_homolo')
                s_dt_regularizada = feature.GetField('data_regul')
                s_ds_matricula_regularizada = feature.GetField('tit_regula')
                s_ds_doc_resumo_em_estudo = feature.GetField('res_em_est')
                s_ds_doc_resumo_delimitada = feature.GetField('res_delimi')
                s_ds_doc_resumo_declarada = feature.GetField('res_declar')
                s_ds_doc_resumo_homologada = feature.GetField('res_homolo')
                s_ds_doc_resumo_regularizada = feature.GetField('res_regula')

                try:

                    conn.execute(INSERT_LIM_TERRA_INDIGENA_ESTUDO,
                                 [s_co_funai, s_no_ti, s_no_grupo_etnico, s_ds_fase_ti, s_ds_modalidade, s_ds_reestudo_ti, s_no_municipio, s_sg_uf, s_st_faixa_fronteira, s_dt_em_estudo, s_ds_portaria_em_estudo, s_dt_delimitada, s_ds_despacho_delimitada, s_dt_declarada, s_ds_portaria_declarada, s_dt_homologada, s_ds_decreto_homologada, s_dt_regularizada, s_ds_matricula_regularizada, s_ds_doc_resumo_em_estudo, s_ds_doc_resumo_delimitada, s_ds_doc_resumo_declarada, s_ds_doc_resumo_homologada, s_ds_doc_resumo_regularizada, s_nu_area_ha, s_geom])

                    app.logger.info(
                        "INSERT: no_ti: %s - s_no_grupo_etnico: %s - s_ds_fase_ti: %s - s_ds_modalidade: %s - ",
                        s_no_ti, s_no_grupo_etnico, s_ds_fase_ti, s_ds_modalidade)

                except (Exception, ValueError, psycopg2.DataError) as e:
                    conn.execute("ROLLBACK")
                    app.logger.exception("ROLLBACK")
                    app.logger.debug(
                        "s_no_ti: %s \n s_no_grupo_etnico: %s \n s_ds_fase_ti: %s \n s_ds_modalidade: %s \n s_ds_reestudo_ti: %s \n s_no_municipio: %s \n s_sg_uf: %s \n s_co_funai: %s \n s_nu_area_ha: %s",
                        s_no_ti, s_no_grupo_etnico, s_ds_fase_ti, s_ds_modalidade, s_ds_reestudo_ti, 
                        s_no_municipio, s_sg_uf, s_co_funai, s_nu_area_ha)
                    app.logger.exception("__exception_insert_lim_terra_indigena_estudo_p__")
                    app.logger.exception("error: %s", e)

    conn.execute("COMMIT")
    conn.execute(
        "REFRESH MATERIALIZED VIEW CONCURRENTLY aplicacao.vwm_tabela_informacao_atualizacao")
    app.logger.info("COMMIT")
    app.logger.info("__fim__atualizar_tabela_terra_indigena__")

# funcao que envia email depois que finaliza
def disparar_email():
    conn.execute(SELECT_INFORMACAO_ATUALIZACAO_TERRA_INDIGENA_ESTUDO)
    resultSet = conn.fetchall()

    if len(resultSet) > 1:
        html = "<div style='font-family: Arial, Helvetica, sans-serif !important;font-size:12px !important;'>"
        html += "Informação da sincronização - Terra Indígena em Estudo(FUNAI).<br /><br />"
        html += "<table border='1' cellpadding='2' cellspacing='2' width='600px'>"
        html += "  <thead>"
        html += "    <tr>"
        html += "      <th align='center'>Data da Atualização</th>"
        html += "      <th align='center'>Descrição</th>"
        html += "      <th align='center'>Total</th>"
        html += "    </tr>"
        html += "  </thead>"
        html += "  <tbody>"

        for rs in resultSet:

            dt_atualizacao = rs.dt_atualizacao
            ds_observacao = rs.ds_observacao
            strong = ''
            align = 'center'

            if dt_atualizacao is None:
                dt_atualizacao = ''

            if ds_observacao == 'Total':
                strong = " style='font-weight: bold !important;'"
                align = "right"

            html += "    <tr>"
            html += "      <td align='center'>%s</td>" % dt_atualizacao
            html += "      <td align='%s'%s>%s</td>" % (align, strong, ds_observacao)
            html += "      <td align='center' style='font-weight: bold !important;'>%s</td>" % rs.nu_total
            html += "    </tr>"

        html += "  </tbody>"
        html += "</table>"
        html += "<br/>"
        html += "<br/>"
        html += "<span style='font-size:11px !important;color:#808080 !important;'>"
        html += "Atenciosamente,"
        html += "<br/>"
        html += "<br/>"
        html += "Altamiro Rodrigues"
        html += "<br/>"
        html += "Administrador de Banco de Dados"
        html += "<br/>"
        html += "altamiro.rodrigues@hexgis.com"
        html += "<br/>"
        html += "<span style='font-weight: bold !important;'><span style='color:#e56605 !important;'>HEX</span> <span style='color:#2a3d51 !important;'>- Tecnologias Geoespaciais</span></span>"
        html += "</span>"
        html += "</div>"

        msg = message(
            "[HEX/FUNAI] Terra Indígena em Estudo - Sincronizacao dos dados - {}".format(
                datetime.now().strftime('%d/%m/%Y - %H:%M:%S')),
            recipients=[app.config['EMAIL_GABRIEL'], app.config['EMAIL_MAIRA']],
            sender=app.config['EMAIL_SENDER'],
            reply_to=app.config['EMAIL_GABRIEL'],
            html=html
        )

        try:
            mail.send(msg)
        except Exception as e:
            app.logger.exception("Erro ao enviar email: %s", e)


@manager.command
def terra_indigena_estudo():
    if exists(diretorio_temporario):
        Path(diretorio_temporario).rmtree()

    if not exists(diretorio_temporario):
        mkdir(diretorio_temporario)

    baixar_arquivo()
    modificar_tabela()  # executa modificacoes nas tabelas de terra indigena e belo monte
    atualizar_terra_indigena_estudo()
    disparar_email()

    if exists(diretorio_temporario):
        Path(diretorio_temporario).rmtree()

    # fecha a conexao do cursor
    conn.close()
    # fecha a conexao do banco
    db.close()
