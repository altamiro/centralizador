# -*- encoding: utf-8 -*-
# Script que faz o download e publica dados do Alerta AWiFS - "DETER B"
# Copyright 2017 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

import requests
import psycopg2
import psycopg2.extras
import gdal
import re

from osgeo import ogr
from pprint import pprint
from datetime import datetime, timedelta
from clint.textui import progress
from os import mkdir, listdir, system, rename
from os.path import join, exists, splitext
from unipath import Path

from app import app, manager, mail, message
from app.settings.utils import descompactar

from .sql import *

descricao_mes = {
    1: 'Janeiro',
    2: 'Fevereiro',
    3: 'Marco',
    4: 'Abril',
    5: 'Maio',
    6: 'Junho',
    7: 'Julho',
    8: 'Agosto',
    9: 'Setembro',
    10: 'Outubro',
    11: 'Novembro',
    12: 'Dezembro'
}

lista_mes_retorna_numero = {
    'janeiro': '01',
    'fevereiro': '02',
    'marco': '03',
    'abril': '04',
    'maio': '05',
    'junho': '06',
    'julho': '07',
    'agosto': '08',
    'setembro': '09',
    'outubro': '10',
    'novembro': '11',
    'dezembro': '12'
}


class Awifs(object):

    def __init__(self):
        self.no_url_zip = "http://terrabrasilis.info/deterb/wms?request=GetFeature&service=wfs&version=1.1.0&outputFormat=SHAPE-ZIP&typename=DETER-B:deter_5_days_old&srsName=EPSG:4674&CQL_FILTER=date='{}' AND sensor='AWiFS'"
        self.no_url_json = "http://terrabrasilis.info/deterb/wms?request=GetFeature&service=wfs&version=1.1.0&outputFormat=application/json&typename=DETER-B:deter_5_days_old&srsName=EPSG:4674&CQL_FILTER=date='{}' AND sensor='AWiFS'"
        self.diretorio = join(app.config['UPLOAD_FOLDER'], 'awifs')
        self.arquivos = []
        self.deteccao = []
        self.db = psycopg2.connect(app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
        self.conn = self.db.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

    def criar_pasta(self):
        if exists(self.diretorio):
            Path(self.diretorio).rmtree()

        if not exists(self.diretorio):
            mkdir(self.diretorio)

    def excluir_pasta(self):
        if exists(self.diretorio):
            Path(self.diretorio).rmtree()

    def refresh_view(self):
        app.logger.info(
            "Inicio Refresh aplicacao.vwm_tabela_informacao_atualizacao")
        self.conn.execute(
            "REFRESH MATERIALIZED VIEW CONCURRENTLY aplicacao.vwm_tabela_informacao_atualizacao")
        app.logger.info(
            "Fim Refresh aplicacao.vwm_tabela_informacao_atualizacao")

    def close(self):
        # fecha a conexao do cursor
        self.conn.close()
        # fecha a conexao do banco
        self.db.close()

    def apagar_todos_dados_caso_tenha_erro(self, dt_arquivo):
        self.conn.execute(DELETE_IMG_ALERTA_AWIFS_A, [
            app.config['AWIFS_TIPO'], dt_arquivo])
        self.conn.execute(DELETE_DOWNLOAD_DETER_A_B, [
            app.config['AWIFS_TIPO'], dt_arquivo])

    def download_deter_a_b_id(self, dt_arquivo):
        self.conn.execute(QUERY_BUSCAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA,
                          [app.config['AWIFS_TIPO'], dt_arquivo])
        nu_id = self.conn.fetchone().id
        return nu_id

    def inserir_novo_download_deter_a_b(self, dt_arquivo):
        try:
            self.conn.execute(INSERT_DOWNLOAD_DETER_A_B, [
                app.config['AWIFS_TIPO'], dt_arquivo])
            self.db.commit()

            return self.download_deter_a_b_id(dt_arquivo)
        except Exception as e:
            self.db.rollback()
            app.logger.exception(
                "Erro ao inserir na tabela tb_download_deter_a_b: %s", e)

    def atualizar_download_deter_a_b(self, dt_arquivo):
        try:
            self.conn.execute(UPDATE_DOWNLOAD_DETER_A_B_AWIFS, [
                app.config['AWIFS_TIPO'], dt_arquivo])
            self.db.commit()
        except Exception as e:
            self.db.rollback()
            app.logger.exception(
                "Erro ao efetuar atualizacao na tabela tb_download_deter_a_b: %s", e)

    def buscar_download_deter_a_b_id(self, dt_arquivo):
        self.conn.execute(QUERY_CONTAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA,
                          [app.config['AWIFS_TIPO'], dt_arquivo])
        total = self.conn.fetchone().total

        if total > 0:
            return self.download_deter_a_b_id(dt_arquivo)
        else:
            return self.inserir_novo_download_deter_a_b(dt_arquivo)

    def getDataInfo(self):
        self.conn.execute(
            "SELECT dt_deteccao FROM aplicacao.vm_data_download_awifs")
        resultSet = self.conn.fetchall()

        if len(resultSet) > 0:
            for r in resultSet:
                dt_deteccao = r.dt_deteccao.strftime("%Y-%m-%d")
                no_url = self.no_url_json.format(dt_deteccao)
                r = self.getDownloadUrl(no_url)
                if r.status_code == 200 and int(r.json()['totalFeatures']) > 0:
                    self.deteccao.append(dt_deteccao)

    def getDownloadUrl(self, url):
        return requests.get(url, stream=True)

    def download(self):
        try:
            self.getDataInfo()
            if len(self.deteccao) > 0:
                for dt in self.deteccao:
                    dt_deteccao = dt
                    no_arquivo = "{}.zip".format(str(dt_deteccao).replace('-', '_'))
                    no_url = self.no_url_zip.format(dt_deteccao)
                    r = self.getDownloadUrl(no_url)
                    if r.status_code == 200:
                        with open(self.diretorio + '/' + no_arquivo, 'wb') as f:
                            for chunk in r.iter_content(1024):
                                if chunk:
                                    f.write(chunk)
                                    f.flush()
                            arquivo_caminho_completo = self.diretorio + '/' + no_arquivo
                            descompactar(arquivo_caminho_completo, self.diretorio)
                            Path(arquivo_caminho_completo).remove()
                            Path("{}/{}".format(self.diretorio, 'wfsrequest.txt')).remove()
                            for file in listdir(self.diretorio):
                                if file.lower().startswith('deter'):
                                    filename, file_extension = splitext(file)
                                    novo_no_arquivo = "{}/{}{}".format(
                                        self.diretorio, dt_deteccao, file_extension)
                                    rename("{}/{}".format(self.diretorio, file), novo_no_arquivo)
        except Exception as e:
            app.logger.exception("Error ao baixar o arquivo")
            pass

    # funcao que processa os dado do awifs deter b
    def processar_dados_awifs_deter_b(self):
        # lista todos os arquivos *.shp
        for arquivo in sorted(listdir(self.diretorio)):
            if arquivo.lower().endswith('.shp'):
                arquivo_caminho_completo = self.diretorio + '/' + arquivo
                gdal.SetConfigOption("SHAPE_ENCODING", "LATIN1")
                shapefile = ogr.Open(arquivo_caminho_completo)
                layer = shapefile.GetLayer()

                self.conn.execute("BEGIN")
                app.logger.info("BEGIN")
                _boo_success = False

                try:

                    for feature in layer:
                        _data = feature.GetField('date').replace('/', '')
                        v_no_tipo = app.config['AWIFS_TIPO']
                        i_nu_lote = feature.GetField('lot')
                        v_no_classe = feature.GetField('classname')
                        c_st_quadrante = feature.GetField('quadrant')
                        i_nu_orbita_ponto = feature.GetField('orbitpoint')
                        v_ds_mes = descricao_mes[int(_data[4:6])]
                        i_nu_ano = int(_data[:4])
                        v_no_municipio = feature.GetField('county')
                        c_sg_uf = feature.GetField('uf')
                        v_uc = feature.GetField('uc')
                        d_dt_deteccao = "{}-{}-{}".format(_data[:4], _data[4:6], _data[6:8])
                        geom = feature.GetGeometryRef().ExportToWkt()

                        self.conn.execute(QUERY_AWIFS_VERIFICAR_DATA_FINALIZADA, [d_dt_deteccao])
                        rs = self.conn.fetchone()

                        if rs.total == 0:
                            tb_download_deter_a_b_id = self.buscar_download_deter_a_b_id(d_dt_deteccao)
                            self.conn.execute(INSERT_IMG_ALERTA_AWIFS_A,
                                              [tb_download_deter_a_b_id, v_no_tipo, i_nu_lote, v_no_classe,
                                               c_st_quadrante,
                                               i_nu_orbita_ponto, v_ds_mes, i_nu_ano, v_no_municipio, c_sg_uf, v_uc,
                                               d_dt_deteccao, geom])

                            app.logger.info(
                                "INSERT: tb_download_deter_a_b_id: %s - v_no_tipo: %s - i_nu_lote: %s - v_no_classe: %s - c_st_quadrante: %s - i_nu_orbita_ponto: %s - v_ds_mes: %s - i_nu_ano: %s - v_no_municipio: %s - c_sg_uf: %s - v_uc: %s - d_dt_deteccao: %s",
                                tb_download_deter_a_b_id, v_no_tipo, i_nu_lote, v_no_classe, c_st_quadrante,
                                i_nu_orbita_ponto, v_ds_mes, i_nu_ano, v_no_municipio, c_sg_uf, v_uc, d_dt_deteccao)

                        _boo_success = True

                except (Exception, ValueError, psycopg2.DataError) as e:
                    self.conn.execute("ROLLBACK")
                    app.logger.info("ROLLBACK")
                    self.apagar_todos_dados_caso_tenha_erro(d_dt_deteccao)
                    _boo_success = False
                    app.logger.exception(
                        '=================== OCORREU UM ERRO AO INSERIR ARQUIVO %s ====================',
                        arquivo)
                    app.logger.exception(
                        "Erro ao inserir na tabela img_alerta_awifs_a: %s", e)
                    app.logger.exception(
                        "tb_download_deter_a_b_id: %s \n v_no_tipo: %s \n i_nu_lote: %s \n v_no_classe: %s \n c_st_quadrante: %s \n i_nu_orbita_ponto: %s \n v_ds_mes: %s \n i_nu_ano: %s \n v_no_municipio: %s \n c_sg_uf: %s \n v_uc: %s \n d_dt_deteccao: %s",
                        tb_download_deter_a_b_id, v_no_tipo, i_nu_lote, v_no_classe, c_st_quadrante, i_nu_orbita_ponto,
                        v_ds_mes, i_nu_ano, v_no_municipio, c_sg_uf, v_uc, d_dt_deteccao)

                if _boo_success:
                    self.conn.execute(UPDATE_DOWNLOAD_DETER_A_B_AWIFS)
                    self.conn.execute("COMMIT")
                    app.logger.info("COMMIT")
                    app.logger.info(
                        "Os dados do arquivo %s, foi processado com sucesso.", arquivo)

    # funcao que envia email depois que finaliza
    def disparar_email(self):
        self.conn.execute(QUERY_TODOS_DOWNLOAD_DETER_A_B,
                          [app.config['AWIFS_TIPO']])
        resultSet = self.conn.fetchall()

        if len(resultSet) > 0:
            html = "<div style='font-family: Arial, Helvetica, sans-serif !important;font-size:12px !important;'>"
            html += "Informação da sincronização dos dados do DETER-B 'Alerta AWiFS'.<br /><br />"
            html += "<table border='1' cellpadding='2' cellspacing='2' width='600px'>"
            html += "  <thead>"
            html += "    <tr>"
            html += "      <th align='center'>Descricao</th>"
            html += "      <th align='center'>Data do Shape</th>"
            html += "      <th align='center'>Quantidade Importada</th>"
            html += "    </tr>"
            html += "  </thead>"
            html += "  <tbody>"

            for rs in resultSet:
                html += "    <tr>"
                html += "      <td align='center'>%s</td>" % rs.no_tipo
                html += "      <td align='center'>%s</td>" % rs.dt_shape
                html += "      <td align='center'>%s</td>" % rs.qt_total_registro
                html += "    </tr>"

            html += "  </tbody>"
            html += "</table>"
            html += "<br/>"
            html += "<br/>"
            html += "<span style='font-size:11px !important;color:#808080 !important;'>"
            html += "Atenciosamente,"
            html += "<br/>"
            html += "<br/>"
            html += "Altamiro Rodrigues"
            html += "<br/>"
            html += "Administrador de Banco de Dados"
            html += "<br/>"
            html += "altamiro.rodrigues@hexgis.com"
            html += "<br/>"
            html += "<span style='font-weight: bold !important;'><span style='color:#e56605 !important;'>HEX</span> <span style='color:#2a3d51 !important;'>- Tecnologias Geoespaciais</span></span>"
            html += "</span>"
            html += "</div>"

            msg = message(
                "[HEX/FUNAI] DETER-B(AWiFS) - Sincronizacao dos dados - {}".format(
                    datetime.now().strftime('%d/%m/%Y - %H:%M:%S')),
                recipients=[app.config['EMAIL_GABRIEL'], app.config['EMAIL_MAIRA']],
                sender=app.config['EMAIL_SENDER'],
                reply_to=app.config['EMAIL_GABRIEL'],
                html=html
            )

            try:
                mail.send(msg)
            except Exception as e:
                app.logger.exception("Erro ao enviar email: %s", e)


@manager.command
def novo_awifs_deter_b():
    awifs = Awifs()
    awifs.criar_pasta()
    awifs.download()
    awifs.processar_dados_awifs_deter_b()
    awifs.excluir_pasta()
    awifs.refresh_view()
    awifs.disparar_email()
    awifs.close()
