# -*- encoding: utf-8 -*-
# Script que faz sincronização da tabela "img_catalogo_landsat_a" do skynet para tabela "img_catalogo_landsat_a" do banco de dados do CMR FUNAI
# Copyright 2017 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

from sqlalchemy.sql import text

from app import app, manager, db

@manager.command
def atualizar_catalogo_landsat():
    db.engine.execute(text("SELECT funai.atualizar_catalogo_landsat()").execution_options(autocommit=True))