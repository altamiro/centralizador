# -*- encoding: utf-8 -*-
# Script que faz o download e publica dados do Alerta MODIS - "DETER A"
# Copyright 2017 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

import requests
import psycopg2
import psycopg2.extras
import gdal
import re

from osgeo import ogr
from pprint import pprint
from datetime import datetime, timedelta
from clint.textui import progress
from os import mkdir, listdir, system
from os.path import join, exists
from unipath import Path

from app import app, manager, mail, message
from app.settings.utils import descompactar

from .sql import *

descricao_mes = {
    1: 'Janeiro',
    2: 'Fevereiro',
    3: 'Marco',
    4: 'Abril',
    5: 'Maio',
    6: 'Junho',
    7: 'Julho',
    8: 'Agosto',
    9: 'Setembro',
    10: 'Outubro',
    11: 'Novembro',
    12: 'Dezembro'
}

lista_mes_retorna_numero = {
    'janeiro': '01',
    'fevereiro': '02',
    'marco': '03',
    'abril': '04',
    'maio': '05',
    'junho': '06',
    'julho': '07',
    'agosto': '08',
    'setembro': '09',
    'outubro': '10',
    'novembro': '11',
    'dezembro': '12'
}

lista_mes_en = {
    'Jan': 1,
    'Feb': 2,
    'Mar': 3,
    'Apr': 4,
    'May': 5,
    'Jun': 6,
    'Jul': 7,
    'Aug': 8,
    'Sep': 9,
    'Oct': 10,
    'Nov': 11,
    'Dec': 12
}

class DeterA(object):
  
  def __init__(self, data):
    self.data = data
    self.no_url = app.config['DETER_URL']
    self.diretorio = join(app.config['UPLOAD_FOLDER'], 'deter')
    self.arquivos = []
    self.db = psycopg2.connect(app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
    self.conn = self.db.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
    # define a data de 8 dias anterior, pegando a data atual como base
    self.two_days_ago = datetime.now() - timedelta(days=3)

  def getDownloadUrl(self, url):
    return requests.get(url, stream=True, auth=requests.auth.HTTPBasicAuth(app.config['DETER_USERNAME'], app.config['DETER_PASSWORD']))

  def getURL(self, url):
    pat = re.compile(
        "<.*?>|&nbsp;|&amp;|Apache/1.3.41 Server at www.obt.inpe.br Port 81| |Index of /deter/xtmp|", re.DOTALL | re.M)
    r = requests.get(url, auth=requests.auth.HTTPBasicAuth(
        app.config['DETER_USERNAME'], app.config['DETER_PASSWORD']))
    data = pat.sub("", r.text)
    l = data.split("\n")
    g = [r for r in l if len(r) > 0]
    del g[:4]
    return g

  def getInfo(self):
    for r in self.data:
      url = "{}{}".format(self.no_url, r)
      for data in self.getURL(url):
        if data.find('_shp.zip') > 0:
          no_arquivo, dt_base_arquivo = data.split(".zip")
          no_arquivo = "{}.zip".format(no_arquivo)
          dia, mes, ano = dt_base_arquivo[:11].split("-")
          dt_base_arquivo = datetime(int(ano), int(
              lista_mes_en[mes]), int(dia))
          if dt_base_arquivo >= self.two_days_ago and len(no_arquivo) > 20:
            nu_ano = no_arquivo[6:10]
            nu_mes = no_arquivo[10:12]
            nu_dia = no_arquivo[12:14]
            dt_arquivo = datetime(int(nu_ano), int(
                nu_mes), int(nu_dia)).strftime("%Y-%m-%d")
            self.arquivos.append({
              'url': url,
              'arquivo': no_arquivo,
              'deteccao': dt_arquivo
            })

  def download(self):
    try:
      self.getInfo()
      for arq in self.arquivos:
        r = self.getDownloadUrl("{}{}".format(arq['url'], arq['arquivo']))
        with open(self.diretorio + '/' + arq['arquivo'], 'wb') as f:
            total_length = int(r.headers.get('content-length'))
            for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length / 1024) + 1):
                if chunk:
                    f.write(chunk)
                    f.flush()

      app.logger.info(
          '=================== INICIO DESCOMPACTADO ====================')
      # lista todos os arquivos *.zip
      for _zip_arquivo in listdir(self.diretorio):

          if _zip_arquivo.lower().endswith('.zip'):
              arquivo_caminho_completo = self.diretorio + '/' + _zip_arquivo
              descompactar(arquivo_caminho_completo,
                            self.diretorio)
              app.logger.info("Arquivo %s descompactado", _zip_arquivo)
              Path(arquivo_caminho_completo).remove()

      app.logger.info(
          '=================== DESCOMPACTADO SUCESSO ===================')

      for arquivo in listdir(self.diretorio):
          if arquivo.lower().find("indicio") > 0:
              arquivo_caminho_completo = self.diretorio + '/' + arquivo
              Path(arquivo_caminho_completo).remove()
          elif arquivo.lower().find("nuvem") > 0:
              arquivo_caminho_completo = self.diretorio + '/' + arquivo
              Path(arquivo_caminho_completo).remove()
          elif arquivo.lower().endswith(".txt") > 0:
              arquivo_caminho_completo = self.diretorio + '/' + arquivo
              Path(arquivo_caminho_completo).remove()
    except Exception as e:
        app.logger.exception("Error ao baixar o arquivo")
        pass

  def apagar_todos_dados_caso_tenha_erro(self, dt_arquivo):
    self.conn.execute(DELETE_IMG_ALERTA_DETER_A, [
                 app.config['DETER_TIPO'], dt_arquivo])
    self.conn.execute(DELETE_DETER_DOWNLOAD_DETER_A_B, [
                 app.config['DETER_TIPO'], dt_arquivo])


  def download_deter_a_b_id(self, dt_arquivo):
      self.conn.execute(QUERY_DETER_BUSCAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA,
                  [app.config['DETER_TIPO'], dt_arquivo])
      nu_id = self.conn.fetchone().id
      return nu_id


  def inserir_novo_download_deter_a_b(self, dt_arquivo):
      try:
          self.conn.execute(INSERT_DETER_DOWNLOAD_DETER_A_B, [
                      app.config['DETER_TIPO'], dt_arquivo])
          self.db.commit()

          return self.download_deter_a_b_id(dt_arquivo)
      except Exception as e:
          self.db.rollback()
          app.logger.exception(
              "Erro ao inserir na tabela tb_download_deter_a_b: %s", e)


  def atualizar_download_deter_a_b(self, dt_arquivo):
      try:
          self.conn.execute(UPDATE_DETER_DOWNLOAD_DETER_A_B, [
                      app.config['DETER_TIPO'], dt_arquivo])
          self.db.commit()
      except Exception as e:
          self.db.rollback()
          app.logger.exception(
              "Erro ao efetuar atualizacao na tabela tb_download_deter_a_b: %s", e)


  def buscar_download_deter_a_b_id(self, dt_arquivo):
      self.conn.execute(QUERY_DETER_CONTAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA,
                  [app.config['DETER_TIPO'], dt_arquivo])
      total = self.conn.fetchone().total

      if total > 0:
          return self.download_deter_a_b_id(dt_arquivo)
      else:
          return self.inserir_novo_download_deter_a_b(dt_arquivo)


  # funcao que processa os dado do awifs deter b
  def processar_dados_deter_a(self):
      # lista todos os arquivos *.shp
      for arquivo in sorted(listdir(self.diretorio)):

          if arquivo.lower().endswith('.shp') and arquivo.lower().find("alerta") > 0:
              arquivo_caminho_completo = self.diretorio + '/' + arquivo
              gdal.SetConfigOption("SHAPE_ENCODING", "LATIN1")
              shapefile = ogr.Open(arquivo_caminho_completo)
              layer = shapefile.GetLayer()

              self.conn.execute("BEGIN")
              app.logger.info("BEGIN")
              _boo_success = False

              try:

                  for feature in layer:
                      geom = feature.GetGeometryRef().ExportToWkt()
                      _data = feature.GetField('view_date').replace(
                          '/', '').replace('-', '')
                      v_no_tipo = app.config['DETER_TIPO']
                      v_no_classe = feature.GetField('class_name')
                      i_nu_cena = int(feature.GetField('scene_id'))

                      d_dt_deteccao = "%s-%s-%s" % (_data[:4],
                                                    _data[4:6], _data[6:8])

                      self.conn.execute(
                          QUERY_DETER_VERIFICAR_DATA_FINALIZADA, [d_dt_deteccao])
                      rs = self.conn.fetchone()

                      if rs.total > 0:
                          self.apagar_todos_dados_caso_tenha_erro(d_dt_deteccao)

                      v_ds_mes = descricao_mes[int(_data[4:6])]
                      i_nu_ano = int(_data[:4])
                      v_no_municipio = ''
                      c_sg_uf = ''

                      # executa uma query para pegar o municipio e uf baseado no cruzamento do dado
                      self.conn.execute(QUERY_CRUZAMENTO_UF_MUNICIPIO, [geom])
                      rs = self.conn.fetchone()
                      if rs:
                          v_no_municipio = rs.no_municipio
                          c_sg_uf = rs.sg_uf

                      tb_download_deter_a_b_id = self.buscar_download_deter_a_b_id(
                          d_dt_deteccao)
                      self.conn.execute(INSERT_IMG_DETER_AWIFS_A,
                                  [tb_download_deter_a_b_id, v_no_tipo, v_no_classe, i_nu_cena, v_ds_mes, i_nu_ano,
                                    v_no_municipio, c_sg_uf, d_dt_deteccao, geom])

                      app.logger.info(
                          "INSERT: tb_download_deter_a_b_id: %s \n v_no_tipo: %s \n v_no_classe: %s \n i_nu_cena: %s \n v_ds_mes: %s \n i_nu_ano: %s \n v_no_municipio: %s \n c_sg_uf: %s \n d_dt_deteccao: %s",
                          tb_download_deter_a_b_id, v_no_tipo, v_no_classe, i_nu_cena, v_ds_mes, i_nu_ano, v_no_municipio,
                          c_sg_uf, d_dt_deteccao)

                      _boo_success = True

              except (Exception, ValueError, psycopg2.DataError) as e:
                  self.conn.execute("ROLLBACK")
                  app.logger.info("ROLLBACK")
                  self.apagar_todos_dados_caso_tenha_erro(d_dt_deteccao)
                  _boo_success = False
                  app.logger.exception('=================== OCORREU UM ERRO AO INSERIR ARQUIVO %s ====================',
                                      arquivo)
                  app.logger.exception(
                      "Erro ao inserir na tabela img_alerta_deter_a: %s", e)
                  app.logger.exception(
                      "tb_download_deter_a_b_id: %s \n v_no_tipo: %s \n v_no_classe: %s \n i_nu_cena: %s \n v_ds_mes: %s \n i_nu_ano: %s \n v_no_municipio: %s \n c_sg_uf: %s \n d_dt_deteccao: %s",
                      tb_download_deter_a_b_id, v_no_tipo, v_no_classe, i_nu_cena, v_ds_mes, i_nu_ano, v_no_municipio,
                      c_sg_uf, d_dt_deteccao)

              if _boo_success:
                  self.conn.execute(UPDATE_DETER_DOWNLOAD_DETER_A_B)
                  self.conn.execute("COMMIT")
                  app.logger.info("COMMIT")
                  app.logger.info(
                      "Os dados do arquivo %s, foi processado com sucesso.", arquivo)


  # funcao que envia email depois que finaliza
  def disparar_email(self):
      self.conn.execute(QUERY_TODOS_DOWNLOAD_DETER_A_B, [app.config['DETER_TIPO']])
      resultSet = self.conn.fetchall()

      if len(resultSet) > 0:
          html = "<div style='font-family: Arial, Helvetica, sans-serif !important;font-size:12px !important;'>"
          html += "Informação da sincronização dos dados do DETER-A 'Alerta MODIS'.<br /><br />"
          html += "<table border='1' cellpadding='2' cellspacing='2' width='600px'>"
          html += "  <thead>"
          html += "    <tr>"
          html += "      <th align='center'>Descricao</th>"
          html += "      <th align='center'>Data do Shape</th>"
          html += "      <th align='center'>Quantidade Importada</th>"
          html += "    </tr>"
          html += "  </thead>"
          html += "  <tbody>"

          for rs in resultSet:
              html += "    <tr>"
              html += "      <td align='center'>%s</td>" % rs.no_tipo
              html += "      <td align='center'>%s</td>" % rs.dt_shape
              html += "      <td align='center'>%s</td>" % rs.qt_total_registro
              html += "    </tr>"

          html += "  </tbody>"
          html += "</table>"
          html += "<br/>"
          html += "<br/>"
          html += "<span style='font-size:11px !important;color:#808080 !important;'>"
          html += "Atenciosamente,"
          html += "<br/>"
          html += "<br/>"
          html += "Altamiro Rodrigues"
          html += "<br/>"
          html += "Administrador de Banco de Dados"
          html += "<br/>"
          html += "altamiro.rodrigues@hexgis.com"
          html += "<br/>"
          html += "<span style='font-weight: bold !important;'><span style='color:#e56605 !important;'>HEX</span> <span style='color:#2a3d51 !important;'>- Tecnologias Geoespaciais</span></span>"
          html += "</span>"
          html += "</div>"

          msg = message(
              "[HEX/FUNAI] DETER-A(MODIS) - Sincronizacao dos dados - {}".format(
                  datetime.now().strftime('%d/%m/%Y - %H:%M:%S')),
              recipients=[app.config['EMAIL_GABRIEL'], app.config['EMAIL_MAIRA']],
              sender=app.config['EMAIL_SENDER'],
              reply_to=app.config['EMAIL_GABRIEL'],
              html=html
          )

          try:
              mail.send(msg)
          except Exception as e:
              app.logger.exception("Erro ao enviar email: %s", e)

  def criar_pasta(self):
    if exists(self.diretorio):
        Path(self.diretorio).rmtree()

    if not exists(self.diretorio):
        mkdir(self.diretorio)

  def excluir_pasta(self):
    if exists(self.diretorio):
        Path(self.diretorio).rmtree()

  def refresh_view(self):
    app.logger.info(
        "Inicio Refresh aplicacao.vwm_tabela_informacao_atualizacao")
    self.conn.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY aplicacao.vwm_tabela_informacao_atualizacao")
    app.logger.info(
        "Fim Refresh aplicacao.vwm_tabela_informacao_atualizacao")

  def close(self):
    # fecha a conexao do cursor
    self.conn.close()
    # fecha a conexao do banco
    self.db.close()
      
@manager.command
def novo_deter_a():
  data = []
  nu_ano = datetime.now().strftime('%Y')
  nu_mes = int(datetime.now().strftime('%m'))
  if nu_mes == 1:
    nu_mes = str(12).rjust(2, '0')
  else:
    nu_mes = str(nu_mes - 1).rjust(2, '0')

  nu_mes_ano = "{}/{}{}/".format(int(nu_ano),nu_ano, nu_mes)

  data.append("{}/".format(nu_ano))
  data.append(nu_mes_ano)

  deter = DeterA(data)
  deter.criar_pasta()
  deter.download()
  deter.processar_dados_deter_a()  
  deter.excluir_pasta()
  deter.refresh_view()
  deter.disparar_email()
  deter.close()
