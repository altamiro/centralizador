# -*- encoding: utf-8 -*-
# Script que faz o download e publica dados do Alerta AWiFS - "DETER B"
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>
import requests
import psycopg2
import psycopg2.extras
import gdal

from osgeo import ogr
from bs4 import BeautifulSoup
from requests_ntlm import HttpNtlmAuth
from datetime import datetime, timedelta
from clint.textui import progress
from os import mkdir, listdir
from os.path import join, exists
from unipath import Path
from app import app, manager, mail, message

from app.settings.utils import descompactar

from .sql import *

db = psycopg2.connect(app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
conn = db.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2467.2 Safari/537.36'
}

descricao_mes = {
    1: 'Janeiro',
    2: 'Fevereiro',
    3: 'Marco',
    4: 'Abril',
    5: 'Maio',
    6: 'Junho',
    7: 'Julho',
    8: 'Agosto',
    9: 'Setembro',
    10: 'Outubro',
    11: 'Novembro',
    12: 'Dezembro'
}

lista_mes_retorna_numero = {
    'janeiro': '01',
    'fevereiro': '02',
    'marco': '03',
    'abril': '04',
    'maio': '05',
    'junho': '06',
    'julho': '07',
    'agosto': '08',
    'setembro': '09',
    'outubro': '10',
    'novembro': '11',
    'dezembro': '12'
}

# pega o caminho do arquivo temporario
diretorio_temporario = join(app.config['UPLOAD_FOLDER'], 'awifs')

target_url = "%s%s" % (app.config['AWIFS_URL'], datetime.now().strftime("%Y"))

# define a data de 8 dias anterior, pegando a data atual como base
two_days_ago = datetime.now() - timedelta(days=8)


def autenticar(url):
    soup = BeautifulSoup(requests.get(url, headers=headers, auth=HttpNtlmAuth(app.config['AWIFS_USERNAME'],
                                                                              app.config['AWIFS_PASSWORD'])).text,
                         'html.parser')
    return soup


def listar_arquivos_zip_servidor():
    soup = autenticar(target_url)
    br = soup.find('br')
    for line in str(br).split('</a><br>'):
        line = BeautifulSoup(line, 'html.parser')
        line_x = line.getText().replace('<dir>', '').replace('PM         ', ' ').replace('AM         ',
                                                                                         ' ').strip().replace('  ',
                                                                                                              '|').replace(
            ' ', '|').split('|')
        month, day, year = line_x[0].split('/')
        hour, minute = line_x[1].split(':')
        path_name = line_x[2]
        path_date = datetime(int(year), int(month), int(day), int(hour), int(minute))

        # # verifica se a data do arquivo e' maior ou igual a data da variavel two_days_ago
        if path_date >= two_days_ago:
            dir_url = "%s/%s" % (target_url, path_name)
            soup = autenticar(dir_url)
            br = soup.find('br')
            for line in str(br).split('</a><br>'):
                line = BeautifulSoup(line, 'html.parser')
                line_x = line.getText().replace('<dir>', '').replace('PM         ', ' ').replace('AM         ',
                                                                                                 ' ').strip().replace(
                    '  ', '|').replace(' ', '|').split('|')
                dir_url = "%s/%s/%s" % (target_url, path_name, line_x[2])
                path_date_name = line_x[2]
                soup = autenticar(dir_url)
                br = soup.find('br')
                for line in str(br).split('</a><br>'):
                    record = {}
                    line = BeautifulSoup(line, 'html.parser')
                    line_x = line.getText().replace('<dir>', '').replace('PM       ', ' ').replace('AM       ',
                                                                                                   ' ').strip().replace(
                        '  ', '|').replace(' ', '|').split('|')
                    dir_url = "%s/%s/%s/%s" % (target_url, path_name, path_date_name, line_x[3])
                    soup = autenticar(dir_url)
                    br = soup.find('br')
                    for line in str(br).split('</a><br>'):
                        record = {}
                        line = BeautifulSoup(line, 'html.parser')
                        line_x = line.getText().replace('<dir>', '').replace('PM       ', ' ').replace('AM       ',
                                                                                                       ' ').strip().replace(
                            '  ', '|').replace(' ', '|').split('|')
                        indexes = 0;
                        for i, x in enumerate(line_x):
                            if x.endswith('.zip'):
                                indexes = i

                        if line_x[indexes].endswith('.zip'):
                            record['url'] = "%s/%s" % (dir_url, line_x[indexes])
                            yield record


def baixar_arquivo(record):
    try:
        if record['url'].endswith('.zip'):

            app.logger.info('Baixando arquivo %s', record['url'])
            local_filename = diretorio_temporario + '/' + record['url'].split('/')[-1]
            r = requests.get(record['url'], stream=True, headers=headers,
                             auth=HttpNtlmAuth(app.config['AWIFS_USERNAME'], app.config['AWIFS_PASSWORD']))
            with open(local_filename, 'wb') as f:
                total_length = int(r.headers.get('content-length'))
                for chunk in progress.bar(r.iter_content(chunk_size=1024),
                                          expected_size=(total_length / 1024) + 1):
                    if chunk:
                        f.write(chunk)
                        f.flush()

            app.logger.info('=================== INICIO DESCOMPACTADO ====================')
            # lista todos os arquivos *.zip
            for _zip_arquivo in listdir(diretorio_temporario):

                if _zip_arquivo.lower().endswith('.zip'):
                    arquivo_caminho_completo = diretorio_temporario + '/' + _zip_arquivo
                    descompactar(arquivo_caminho_completo, diretorio_temporario)
                    app.logger.info("Arquivo %s descompactado", _zip_arquivo)
                    Path(arquivo_caminho_completo).remove()

            app.logger.info('=================== DESCOMPACTADO SUCESSO ===================')

    except Exception as e:
        app.logger.exception("Error ao baixar o arquivo awifs: %s \n error: %s", record['url'], e)


def apagar_todos_dados_caso_tenha_erro(nu_lote, dt_arquivo):
    conn.execute(DELETE_IMG_ALERTA_AWIFS_A, [app.config['AWIFS_TIPO'], nu_lote, dt_arquivo])
    conn.execute(DELETE_DOWNLOAD_DETER_A_B, [app.config['AWIFS_TIPO'], nu_lote, dt_arquivo])


def download_deter_a_b_id(nu_lote, dt_arquivo):
    conn.execute(QUERY_BUSCAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA,
                 [app.config['AWIFS_TIPO'], nu_lote, dt_arquivo])
    nu_id = conn.fetchone().id
    return nu_id


def inserir_novo_download_deter_a_b(nu_lote, dt_arquivo):
    try:
        conn.execute(INSERT_DOWNLOAD_DETER_A_B, [app.config['AWIFS_TIPO'], dt_arquivo, nu_lote])
        db.commit()

        return download_deter_a_b_id(nu_lote, dt_arquivo)
    except Exception as e:
        db.rollback()
        app.logger.exception("Erro ao inserir na tabela tb_download_deter_a_b: %s", e)


def atualizar_download_deter_a_b(nu_lote, dt_arquivo):
    try:
        conn.execute(UPDATE_DOWNLOAD_DETER_A_B, [app.config['AWIFS_TIPO'], nu_lote, dt_arquivo])
        db.commit()
    except Exception as e:
        db.rollback()
        app.logger.exception("Erro ao efetuar atualizacao na tabela tb_download_deter_a_b: %s", e)


def buscar_download_deter_a_b_id(nu_lote, dt_arquivo):
    conn.execute(QUERY_CONTAR_DOWNLOAD_DETER_A_B_POR_TIPO_E_DATA,
                 [app.config['AWIFS_TIPO'], nu_lote, dt_arquivo])
    total = conn.fetchone().total

    if total > 0:
        return download_deter_a_b_id(nu_lote, dt_arquivo)
    else:
        return inserir_novo_download_deter_a_b(nu_lote, dt_arquivo)


# funcao que processa os dado do awifs deter b
def processar_dados_awifs_deter_b():
    # lista todos os arquivos *.shp
    for arquivo in sorted(listdir(diretorio_temporario)):

        if arquivo.lower().endswith('.shp'):
            arquivo_caminho_completo = diretorio_temporario + '/' + arquivo
            gdal.SetConfigOption("SHAPE_ENCODING", "LATIN1")
            shapefile = ogr.Open(arquivo_caminho_completo)
            layer = shapefile.GetLayer()

            conn.execute("BEGIN")
            app.logger.info("BEGIN")
            _boo_success = False

            try:

                for feature in layer:
                    geom = feature.GetGeometryRef().ExportToWkt()
                    _data = feature.GetField('data').replace('/', '')
                    v_no_tipo = app.config['AWIFS_TIPO']
                    v_no_classe = feature.GetField('class_name')
                    c_st_quadrante = feature.GetField('quadrante')
                    i_nu_orbita_ponto = feature.GetField('orbiponto')
                    v_uc = ''

                    if feature.IsFieldSet('uc'):
                        v_uc = feature.GetField('uc')

                    v_ds_mes = descricao_mes[int(_data[4:6])]
                    i_nu_ano = int(_data[:4])
                    v_no_municipio = ''
                    c_sg_uf = ''

                    # executa uma query para pegar o municipio e uf baseado no cruzamento do dado
                    conn.execute(QUERY_CRUZAMENTO_UF_MUNICIPIO, [geom])
                    rs = conn.fetchone()
                    if rs:
                        v_no_municipio = rs.no_municipio
                        c_sg_uf = rs.sg_uf

                    i_nu_lote = feature.GetField('lote')
                    d_dt_deteccao = "%s-%s-%s" % (_data[:4], _data[4:6], _data[6:8])

                    conn.execute(QUERY_AWIFS_VERIFICAR_DATA_FINALIZADA, [i_nu_lote, d_dt_deteccao])
                    rs = conn.fetchone()

                    if rs.total == 0:
                        tb_download_deter_a_b_id = buscar_download_deter_a_b_id(i_nu_lote, d_dt_deteccao)
                        conn.execute(INSERT_IMG_ALERTA_AWIFS_A,
                                     [tb_download_deter_a_b_id, v_no_tipo, i_nu_lote, v_no_classe, c_st_quadrante,
                                      i_nu_orbita_ponto, v_ds_mes, i_nu_ano, v_no_municipio, c_sg_uf, v_uc,
                                      d_dt_deteccao, geom])

                        app.logger.info(
                            "INSERT: tb_download_deter_a_b_id: %s - v_no_tipo: %s - i_nu_lote: %s - v_no_classe: %s - c_st_quadrante: %s - i_nu_orbita_ponto: %s - v_ds_mes: %s - i_nu_ano: %s - v_no_municipio: %s - c_sg_uf: %s - v_uc: %s - d_dt_deteccao: %s",
                            tb_download_deter_a_b_id, v_no_tipo, i_nu_lote, v_no_classe, c_st_quadrante,
                            i_nu_orbita_ponto, v_ds_mes, i_nu_ano, v_no_municipio, c_sg_uf, v_uc, d_dt_deteccao)

                    _boo_success = True

            except (Exception, ValueError, psycopg2.DataError) as e:
                conn.execute("ROLLBACK")
                app.logger.info("ROLLBACK")
                apagar_todos_dados_caso_tenha_erro(i_nu_lote, d_dt_deteccao)
                _boo_success = False
                app.logger.exception('=================== OCORREU UM ERRO AO INSERIR ARQUIVO %s ====================',
                                     arquivo)
                app.logger.exception("Erro ao inserir na tabela img_alerta_awifs_a: %s", e)
                app.logger.exception(
                    "tb_download_deter_a_b_id: %s \n v_no_tipo: %s \n i_nu_lote: %s \n v_no_classe: %s \n c_st_quadrante: %s \n i_nu_orbita_ponto: %s \n v_ds_mes: %s \n i_nu_ano: %s \n v_no_municipio: %s \n c_sg_uf: %s \n v_uc: %s \n d_dt_deteccao: %s",
                    tb_download_deter_a_b_id, v_no_tipo, i_nu_lote, v_no_classe, c_st_quadrante, i_nu_orbita_ponto,
                    v_ds_mes, i_nu_ano, v_no_municipio, c_sg_uf, v_uc, d_dt_deteccao)

            if _boo_success:
                conn.execute(UPDATE_DOWNLOAD_DETER_A_B_AWIFS)
                conn.execute("COMMIT")
                app.logger.info("COMMIT")
                app.logger.info("Os dados do arquivo %s, foi processado com sucesso.", arquivo)


# funcao que envia email depois que finaliza
def disparar_email():
    conn.execute(QUERY_TODOS_DOWNLOAD_DETER_A_B, [app.config['AWIFS_TIPO']])
    resultSet = conn.fetchall()

    if len(resultSet) > 0:
        html = "<div style='font-family: Arial, Helvetica, sans-serif !important;font-size:12px !important;'>"
        html += "Informação da sincronização dos dados do DETER-B 'Alerta AWiFS'.<br /><br />"
        html += "<table border='1' cellpadding='2' cellspacing='2' width='600px'>"
        html += "  <thead>"
        html += "    <tr>"
        html += "      <th align='center'>Descricao</th>"
        html += "      <th align='center'>Data do Shape</th>"
        html += "      <th align='center'>Lote</th>"
        html += "      <th align='center'>Quantidade Importada</th>"
        html += "    </tr>"
        html += "  </thead>"
        html += "  <tbody>"

        for rs in resultSet:
            html += "    <tr>"
            html += "      <td align='center'>%s</td>" % rs.no_tipo
            html += "      <td align='center'>%s</td>" % rs.dt_shape
            html += "      <td align='center'>%s</td>" % rs.nu_lote
            html += "      <td align='center'>%s</td>" % rs.qt_total_registro
            html += "    </tr>"

        html += "  </tbody>"
        html += "</table>"
        html += "<br/>"
        html += "<br/>"
        html += "<span style='font-size:11px !important;color:#808080 !important;'>"
        html += "Atenciosamente,"
        html += "<br/>"
        html += "<br/>"
        html += "Altamiro Rodrigues"
        html += "<br/>"
        html += "Administrador de Banco de Dados"
        html += "<br/>"
        html += "altamiro.rodrigues@hexgis.com"
        html += "<br/>"
        html += "<span style='font-weight: bold !important;'><span style='color:#e56605 !important;'>HEX</span> <span style='color:#2a3d51 !important;'>- Tecnologias Geoespaciais</span></span>"
        html += "</span>"
        html += "</div>"

        msg = message(
            "[HEX/FUNAI] DETER-B(AWiFS) - Sincronizacao dos dados - {}".format(
                datetime.now().strftime('%d/%m/%Y - %H:%M:%S')),
            recipients=[app.config['EMAIL_GABRIEL'], app.config['EMAIL_MAIRA']],
            sender=app.config['EMAIL_SENDER'],
            reply_to=app.config['EMAIL_GABRIEL'],
            html=html
        )

        try:
            mail.send(msg)
        except Exception as e:
            app.logger.exception("Erro ao enviar email: %s", e)


@manager.command
def awifs():
    if exists(diretorio_temporario):
        Path(diretorio_temporario).rmtree()

    if not exists(diretorio_temporario):
        mkdir(diretorio_temporario)

    for record in listar_arquivos_zip_servidor():
        baixar_arquivo(record)

    processar_dados_awifs_deter_b()
    conn.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY aplicacao.vwm_tabela_informacao_atualizacao")
    conn.execute("VACUUM FULL aplicacao.vwm_tabela_informacao_atualizacao")
    conn.execute("VACUUM ANALYZE aplicacao.vwm_tabela_informacao_atualizacao")
    disparar_email()

    if exists(diretorio_temporario):
        Path(diretorio_temporario).rmtree()

    # fecha a conexao do cursor
    conn.close()
    # fecha a conexao do banco
    db.close()
