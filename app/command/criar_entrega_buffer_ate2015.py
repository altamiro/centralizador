# -*- encoding: utf-8 -*-
# Script que faz a migração de tabela
# Copyright 2017 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

import psycopg2
import psycopg2.extras

from datetime import datetime, timedelta
from app import app, manager, db

from ..models import *


def criar_entrega():
    SQL = "SELECT " \
          "(row_number() OVER (ORDER BY to_char(m.dt_cadastro,'YYYY-MM-DD')::date ASC))::integer AS nu_entrega, " \
          "to_char(m.dt_cadastro,'YYYY-MM-DD')::date AS dt_entrega, " \
          "COUNT(DISTINCT m.id) AS qt_total_registro, " \
          "array_agg(DISTINCT m.id) AS ids " \
          "FROM " \
          "funai.img_mascara_antropismo_consolidado_buffer_ate2015_a AS m " \
          "GROUP BY " \
          "to_char(m.dt_cadastro,'YYYY-MM-DD')::date " \
          "ORDER BY " \
          "to_char(m.dt_cadastro,'YYYY-MM-DD')::date ASC"

    result = db.engine.execute(SQL).fetchall()

    try:
        if len(result) > 0:
            for r in result:
                tb_usuario_id = 1
                nu_entrega = r.nu_entrega
                dt_entrega = r.dt_entrega.strftime("%Y-%m-%d")
                qt_total_registro = r.qt_total_registro

                entregaBufferAte2015 = TbEntregaBufferAte2015(tb_usuario_id=tb_usuario_id, nu_entrega=nu_entrega,
                                                              dt_entrega=dt_entrega, st_finalizado='t',
                                                              qt_total_registro=qt_total_registro)

                db.session.add(entregaBufferAte2015)
                db.session.commit()

                if len(r.ids) > 0:
                    for i in r.ids:
                        usuarioEntregaBufferAte2015 = TbUsuarioEntregaBufferAte2015(
                            tb_entrega_id=entregaBufferAte2015.id, img_mascara_buffer_ate2015_id=i)
                        db.session.add(usuarioEntregaBufferAte2015)
                        db.session.commit()

                app.logger.debug(
                    "OK! id: %s - tb_usuario_id: %s - nu_entrega: %s - dt_entrega: %s - qt_total_registro: %s",
                    entregaBufferAte2015.id, tb_usuario_id, nu_entrega, dt_entrega, qt_total_registro)

    except (Exception, ValueError) as e:
        db.session.rollback()
        app.logger.error("ROLLBACK")
        app.logger.error("__error_insert_tb_entrega_buffer_ate2015__")
        app.logger.exception("error: %s", e)


@manager.command
def criar_entrega_buffer_ate2015():
    criar_entrega()
