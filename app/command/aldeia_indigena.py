# -*- encoding: utf-8 -*-
# Script que faz o download e publica dados do CR que tem no geoserver da FUNAI
# Copyright 2017 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>
import wget
import psycopg2
import psycopg2.extras
import gdal

from osgeo import ogr
from datetime import datetime, timedelta
from os import mkdir, listdir, system
from os.path import join, exists
from unipath import Path
from app import app, manager, mail, message

from app.settings.utils import descompactar

from .sql import *

db = psycopg2.connect(app.config['PSYCOPG2_DATABASE_URI'])  # psycopg2
conn = db.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)

# pega o caminho do arquivo temporario
diretorio_temporario = join(app.config['UPLOAD_FOLDER'], 'aldeia_indigena')


def baixar_arquivo():
    url = app.config['ALDEIA_INDIGENA_URL']

    if not exists(diretorio_temporario):
        mkdir(diretorio_temporario)

    try:
        local_filename = join(diretorio_temporario, app.config['ALDEIA_INDIGENA_ARQUIVO'])

        file = wget.download(url, local_filename)

        descompactar(file, diretorio_temporario)

    except Exception as e:
        app.logger.exception("Error ao baixar o arquivo da aldeia_indigena: %s", e)


def modificar_tabela():
    conn.execute("BEGIN")
    app.logger.info("BEGIN")

    try:
        conn.execute(DELETE_LOC_ALDEIA_INDIGENA)
        conn.execute("COMMIT")
        app.logger.info("COMMIT")
    except (Exception, ValueError, psycopg2.DataError) as e:
        conn.execute("ROLLBACK")
        app.logger.exception("ROLLBACK")
        app.logger.exception("__exception_delete_alter_loc_aldeia_indigena_p__")
        app.logger.exception("error: %s", e)


def atualizar_aldeia_indigena():
    conn.execute("BEGIN")
    app.logger.info("BEGIN")

    app.logger.info("__inicio__atualizar_tabela_loc_aldeia_indigena_p__")

    # lista todos os arquivos *.shp
    for _shp_arquivo in sorted(listdir(diretorio_temporario)):

        if _shp_arquivo.lower().endswith('.shp'):
            arquivo_caminho_completo = join(diretorio_temporario, _shp_arquivo)
            gdal.SetConfigOption("SHAPE_ENCODING", "LATIN1")
            shapefile = ogr.Open(arquivo_caminho_completo)
            layer = shapefile.GetLayer()
            for feature in layer:
                s_no_aldeia = feature.GetField('nome_aldei')
                s_terra_indigena_co_funai = None
                if feature.GetField('cod_ti') is not None:
                    s_terra_indigena_co_funai = int(feature.GetField('cod_ti'))
                s_municipio_nu_geocodigo = int(feature.GetField('cod_munici'))
                s_ds_cr = feature.GetField('nome_cr')
                s_no_municipio = feature.GetField('nommunic')
                s_no_uf = feature.GetField('nomuf')
                s_geom = feature.GetGeometryRef().ExportToWkt()

                try:

                    conn.execute(INSERT_LOC_ALDEIA_INDIGENA,
                                 [s_no_aldeia, s_terra_indigena_co_funai, s_municipio_nu_geocodigo, s_ds_cr,
                                  s_no_municipio, s_no_uf, s_geom])

                    app.logger.info("INSERT: s_no_aldeia: %s - s_terra_indigena_co_funai: %s - s_ds_cr: %s",
                                    s_no_aldeia, s_terra_indigena_co_funai, s_ds_cr)

                except (Exception, ValueError, psycopg2.DataError) as e:
                    conn.execute("ROLLBACK")
                    app.logger.exception("ROLLBACK")
                    app.logger.debug(
                        "s_no_aldeia: %s \n s_terra_indigena_co_funai: %s \n s_ds_cr: %s \n s_no_municipio: %s",
                        s_no_aldeia, s_terra_indigena_co_funai, s_ds_cr, s_no_municipio)
                    app.logger.exception("__exception_insert_loc_aldeia_indigena_p__")
                    app.logger.exception("error: %s", e)

    conn.execute("COMMIT")
    conn.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY aplicacao.vwm_tabela_informacao_atualizacao")
    app.logger.info("COMMIT")
    app.logger.info("__fim__atualizar_tabela_loc_aldeia_indigena_p__")


# funcao que envia email depois que finaliza
def disparar_email():
    conn.execute(SELECT_INFORMACAO_ATUALIZACAO_ALDEIA_INDIGENA)
    r = conn.fetchone()

    if len(r) > 1:
        html = "<div style='font-family: Arial, Helvetica, sans-serif !important;font-size:12px !important;'>"
        html += "Informação da sincronização - Aldeia Indigena(FUNAI).<br /><br />"
        html += "<table border='1' cellpadding='2' cellspacing='2' width='600px'>"
        html += "  <thead>"
        html += "    <tr>"
        html += "      <th align='center'>Data</th>"
        html += "      <th align='center'>Horário</th>"
        html += "      <th align='center'>Total</th>"
        html += "    </tr>"
        html += "  </thead>"
        html += "  <tbody>"

        html += "    <tr>"
        html += "      <td align='center'>%s</td>" % str(r.dt_atualizacao)
        html += "      <td align='center'>%s</td>" % str(r.hr_atualizacao)
        html += "      <td align='center' style='font-weight: bold !important;'>%s</td>" % int(r.qtd_registro)
        html += "    </tr>"

        html += "  </tbody>"
        html += "</table>"
        html += "<br/>"
        html += "<br/>"
        html += "<span style='font-size:11px !important;color:#808080 !important;'>"
        html += "Atenciosamente,"
        html += "<br/>"
        html += "<br/>"
        html += "Altamiro Rodrigues"
        html += "<br/>"
        html += "Administrador de Banco de Dados"
        html += "<br/>"
        html += "altamiro.rodrigues@hexgis.com"
        html += "<br/>"
        html += "<span style='font-weight: bold !important;'><span style='color:#e56605 !important;'>HEX</span> <span style='color:#2a3d51 !important;'>- Tecnologias Geoespaciais</span></span>"
        html += "</span>"
        html += "</div>"

        msg = message(
            "[HEX/FUNAI] Aldeia Indigena - {}".format(datetime.now().strftime('%d/%m/%Y - %H:%M:%S')),
            recipients=[app.config['EMAIL_GABRIEL'], app.config['EMAIL_MAIRA']],
            sender=app.config['EMAIL_SENDER'],
            reply_to=app.config['EMAIL_GABRIEL'],
            html=html
        )

        try:
            mail.send(msg)
        except Exception as e:
            app.logger.exception("Erro ao enviar email: %s", e)


@manager.command
def aldeia_indigena():
    if exists(diretorio_temporario):
        Path(diretorio_temporario).rmtree()

    if not exists(diretorio_temporario):
        mkdir(diretorio_temporario)

    baixar_arquivo()
    modificar_tabela()  # executa modificacoes nas tabelas de terra indigena e belo monte
    atualizar_aldeia_indigena()
    disparar_email()

    if exists(diretorio_temporario):
        Path(diretorio_temporario).rmtree()

    # fecha a conexao do cursor
    conn.close()
    # fecha a conexao do banco
    db.close()
