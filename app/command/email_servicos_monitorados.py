# -*- encoding: utf-8 -*-
# Script que envia email dos serviços inativos ou falha
# Copyright 2017 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

from pprint import pprint
from app import app, manager, mail, message
from datetime import datetime
from ..models import VisaoServidorServicoMonitorado


# funcao que envia email depois que finaliza
def disparar_email():


    result = VisaoServidorServicoMonitorado().to_status_notin_active()

    if result:
        html = "<div style='font-family: Arial, Helvetica, sans-serif !important;font-size:12px !important;'>"
        html += "Informação sobre serviços monitorados.<br /><br />"
        html += "<table border='1' cellpadding='2' cellspacing='2' width='100%'>"
        html += "  <thead>"
        html += "    <tr>"
        html += "      <th align='center'>Servidor</th>"
        html += "      <th align='center'>Nr. IP</th>"
        html += "      <th align='center'>Serviço</th>"
        html += "      <th align='center'>Situação</th>"
        html += "      <th align='center'>Dt. Serviço</th>"
        html += "      <th align='center'>Em Atividade</th>"
        html += "    </tr>"
        html += "  </thead>"
        html += "  <tbody>"

        for data in result:
            r = data.to_json()

            html += "    <tr>"
            html += "      <td align='left'>%s</td>" % r['no_servidor']
            html += "      <td align='center'>%s</td>" % r['nu_ip']
            html += "      <td align='left'>%s</td>" % r['no_servico']
            html += "      <td align='left'>%s - %s</td>" % (r['ds_status'], r['ds_situacao'])
            html += "      <td align='center'>%s</td>" % r['dt_cadastro'].strftime('%d/%m/%Y - %H:%M:%S')
            html += "      <td align='left'>%s %s %s %s %s %s</td>" % (
            r['nu_ano'], r['nu_mes'], r['nu_dia'], r['nu_hora'], r['nu_minuto'], r['nu_segundo'])
            html += "    </tr>"

        html += "  </tbody>"
        html += "</table>"

        html += "<br/>"
        html += "<br/>"
        html += "<br/>"
        html += "<span style='font-size:11px !important;color:#808080 !important;'>"
        html += "Atenciosamente,"
        html += "<br/>"
        html += "<br/>"
        html += "Altamiro Rodrigues"
        html += "<br/>"
        html += "Administrador de Banco de Dados"
        html += "<br/>"
        html += "altamiro.rodrigues@hexgis.com"
        html += "<br/>"
        html += "<span style='font-weight: bold !important;'><span style='color:#e56605 !important;'>HEX</span> <span style='color:#2a3d51 !important;'>- Tecnologias Geoespaciais</span></span>"
        html += "</span>"
        html += "</div>"

        msg = message(
            "[HEX/FUNAI] Serviço(s) de Monitorado(s) - {}".format(datetime.now().strftime('%d/%m/%Y - %H:%M:%S')),
            recipients=[app.config['EMAIL_GABRIEL'], app.config['EMAIL_MAIRA']],
            sender=app.config['EMAIL_SENDER'],
            reply_to=app.config['EMAIL_GABRIEL'],
            html=html
        )

        try:
            mail.send(msg)
        except Exception as e:
            app.logger.exception("Erro ao enviar email: %s", e)


@manager.command
def email_servicos_monitorados():
    disparar_email()
