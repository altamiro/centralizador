# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

from flask import session
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.sql import text
from app import db


def getSessaoUsuario(attr):
    return dict(session["usuario"]).get(attr)


class Usuario(db.Model):
    __tablename__ = "tb_usuario"
    __table_args__ = {"schema": "aplicacao"}

    id = db.Column(db.Integer, primary_key=True)
    no_usuario = db.Column(db.String(50))
    ds_email = db.Column(db.String(50), unique=True)
    ds_senha = db.Column(db.String(128))
    st_analista = db.Column(db.Boolean)
    st_administrador = db.Column(db.Boolean)
    st_ativo = db.Column(db.Boolean)
    dt_atualizacao = db.Column(db.DateTime)
    dt_cadastro = db.Column(db.DateTime)

    def __init__(self, no_usuario, ds_email, ds_senha, st_analista, st_administrador):
        self.no_usuario = no_usuario
        self.ds_email = ds_email
        self.password = ds_senha
        self.st_analista = st_analista
        self.st_administrador = st_administrador
        self.st_ativo = True
        self.dt_cadastro = "now()"

    @property
    def password(self):
        raise AttributeError("senha não é um atributo legível")

    @password.setter
    def password(self, password):
        self.ds_senha = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.ds_senha, password)

    def __repr__(self):
        return "{id: %d, no_usuario: %s, ds_email: %s, st_analista: %s, st_administrador: %s, st_ativo: %s}" % (
            self.id, self.no_usuario, self.ds_email, self.st_analista, self.st_administrador, self.st_ativo)


class TemplateRelatorio(db.Model):
    __tablename__ = "tb_configurar_template_relatorio"
    __table_args__ = {"schema": "aplicacao"}

    no_chave = db.Column(db.String(255), primary_key=True)
    ds_valor = db.Column(db.Text)
    tp_relatorio = db.Column(db.Integer)
    tb_usuario_id = db.Column(db.Integer)
    dt_atualizacao = db.Column(db.DateTime)
    dt_cadastro = db.Column(db.DateTime)

    def __init__(self, no_chave, ds_valor, tp_relatorio, usuario_id):
        self.no_chave = no_chave
        self.ds_valor = ds_valor
        self.tp_relatorio = tp_relatorio
        self.tb_usuario_id = usuario_id
        self.dt_cadastro = "now()"

    def __repr__(self):
        return "{no_chave: %s, ds_valor: %s, tp_relatorio: %s, dt_atualizacao: %s, dt_cadastro: %s}" % (
            self.no_chave, self.ds_valor, self.tp_relatorio, self.dt_atualizacao, self.dt_cadastro)


class EntregaAvisoDiario(db.Model):
    __tablename__ = "tb_entrega_aviso_diario"
    __table_args__ = {"schema": "aplicacao"}

    id = db.Column(db.Integer, primary_key=True)
    tb_usuario_id = db.Column(db.Integer)
    tb_ciclo_monitoramento_id = db.Column(db.Integer)
    nu_aviso = db.Column(db.Integer)
    nu_orbita = db.Column(db.String(50))
    st_situacao = db.Column(db.String(9))
    dt_disponibilizado_banco = db.Column(db.DateTime)
    js_terra_indigena = db.Column(JSONB)
    js_coordenacao_regional = db.Column(JSONB)
    js_estado = db.Column(JSONB)
    js_imagens = db.Column(JSONB)
    dt_cadastro = db.Column(db.DateTime)

    def __init__(self, usuario_id, ciclo_monitoramento_id, nu_aviso, nu_orbita, st_situacao):
        self.tb_usuario_id = usuario_id
        self.tb_ciclo_monitoramento_id = ciclo_monitoramento_id
        self.nu_aviso = nu_aviso
        self.nu_orbita = nu_orbita
        self.st_situacao = st_situacao
        self.dt_disponibilizado_banco = "now()"
        self.dt_cadastro = "now()"

    def __repr__(self):
        return "{usuario: %s, ciclo: %s, nu_orbita: %s, situacao: %s, dt_disponibilizado: %s, dt_cadastro: %s}" % (
            self.tb_usuario_id, self.tb_ciclo_monitoramento_id, self.nu_orbita, self.st_situacao,
            self.dt_disponibilizado_banco, self.dt_cadastro)


class VisaoEntregaAvisoDiario(db.Model):
    __tablename__ = "vw_entrega_aviso_diario"
    __table_args__ = {"schema": "aplicacao"}

    id = db.Column(db.Integer, primary_key=True)
    ciclo_id = db.Column(db.Integer)
    no_referencia = db.Column(db.String(10))
    no_usuario = db.Column(db.String(50))
    no_ciclo = db.Column(db.String(50))
    no_ciclo_descricao = db.Column(db.String(80))
    nu_orbita = db.Column(db.String(50))
    nu_orbita_aspas = db.Column(db.String(50))
    st_situacao = db.Column(db.String(9))
    dt_disponibilizado = db.Column(db.String(22))
    dt_cadastro = db.Column(db.String(21))
    no_arquivo = db.Column(db.Text)
    js_terra_indigena = db.Column(JSONB)
    js_coordenacao_regional = db.Column(JSONB)
    js_estado = db.Column(JSONB)
    js_imagens = db.Column(JSONB)
    nu_ano = db.Column(db.Integer)

    def __repr__(self):
        return '<%s - %s - %s - %s - %s - %s - %s>' % (
            self.id, self.ciclo_id, self.no_referencia, self.no_usuario, self.no_ciclo, self.nu_orbita,
            self.st_situacao)


class VisaoMonitoramentoDataTUm(db.Model):
    __tablename__ = 'vw_monitoramento_consolidado_data_t_um'
    __table_args__ = {'schema': 'funai'}

    id = db.Column(db.Integer, primary_key=True)
    ciclo_id = db.Column(db.Integer)
    dt_t_um = db.Column(db.Date)
    nu_orbita = db.Column(db.String(3))
    boo_status = db.Column(db.Integer)
    ds_descricao = db.Column(db.Text)

    def __repr__(self):
        return '<%s - %s - %s - %s - %s - %s>' % (
            self.id, self.ciclo_id, self.dt_t_um, self.nu_orbita, self.boo_status, self.ds_descricao)


class VisaoCicloMonitoramentoConsolidado(db.Model):
    __tablename__ = 'vw_ciclo_monitoramento_consolidado'
    __table_args__ = {'schema': 'funai'}

    id = db.Column(db.Integer, primary_key=True)
    no_ciclo = db.Column(db.String(50))

    def __repr__(self):
        return '<%s - %s>' % (self.id, self.no_ciclo)


class VisaoMonitoramentoNuOrbita(db.Model):
    __tablename__ = 'vw_monitoramento_consolidado_nu_orbita'
    __table_args__ = {'schema': 'funai'}

    id = db.Column(db.Integer, primary_key=True)
    ciclo_id = db.Column(db.Integer)
    nu_orbita = db.Column(db.String(3))
    boo_status = db.Column(db.Integer)
    ds_descricao = db.Column(db.Text)

    def __repr__(self):
        return '<%s - %s - %s - %s - %s>' % (self.id, self.ciclo_id, self.nu_orbita, self.boo_status, self.ds_descricao)


class VisaoMonitoramentoDiarioPorOrbita(db.Model):
    __tablename__ = 'vwm_monitora_aviso_diario_por_orbita'
    __table_args__ = {'schema': 'funai'}

    id = db.Column(db.Integer, primary_key=True)
    nu_orbita = db.Column(db.String(3))
    ciclo_id = db.Column(db.Integer)
    no_ciclo = db.Column(db.String(50))
    dt_t_um_formatada = db.Column(db.String(10))
    dt_t_um = db.Column(db.Date)
    cr_nu_area_ha = db.Column(db.Float)
    dg_nu_area_ha = db.Column(db.Float)
    dr_nu_area_ha = db.Column(db.Float)
    ff_nu_area_ha = db.Column(db.Float)
    total_nu_area_ha = db.Column(db.Float)

    def __repr__(self):
        return '<%s - %s - %s - %s - %s - %s - %s - %s - %s - %s - %s>' % (
            self.id, self.nu_orbita, self.ciclo_id, self.no_ciclo, self.dt_t_um_formatada, self.dt_t_um,
            self.cr_nu_area_ha,
            self.dg_nu_area_ha, self.dr_nu_area_ha, self.ff_nu_area_ha, self.total_nu_area_ha)


class VisaoMonitoramentoDiarioPorTerraIndigena(db.Model):
    __tablename__ = 'vwm_monitora_aviso_diario_por_terra_indigena'
    __table_args__ = {'schema': 'funai'}

    id = db.Column(db.Integer, primary_key=True)
    co_funai = db.Column(db.Integer)
    no_ti = db.Column(db.String(100))
    ds_cr = db.Column(db.String(100))
    id_cr = db.Column(db.Text)
    ciclo_id = db.Column(db.Integer)
    no_ciclo = db.Column(db.String(50))
    nu_orbita = db.Column(db.String(3))
    dt_t_um_formatada = db.Column(db.String(10))
    dt_t_um = db.Column(db.Date)
    cr_nu_area_ha = db.Column(db.Float)
    dg_nu_area_ha = db.Column(db.Float)
    dr_nu_area_ha = db.Column(db.Float)
    ff_nu_area_ha = db.Column(db.Float)
    total_nu_area_ha = db.Column(db.Float)

    def __repr__(self):
        return '<%s - %s - %s - %s - %s - %s - %s - %s - %s - %s - %s - %s - %s>' % (
            self.id, self.co_funai, self.no_ti, self.ciclo_id, self.no_ciclo, self.nu_orbita, self.dt_t_um_formatada,
            self.dt_t_um, self.cr_nu_area_ha, self.dg_nu_area_ha, self.dr_nu_area_ha, self.ff_nu_area_ha,
            self.total_nu_area_ha)


class VisaoMonitoramentoDiarioPorTerraIndigenaCoordenacaoRegional(db.Model):
    __tablename__ = 'vwm_monitora_aviso_diario_por_ti_coordenacao_regional'
    __table_args__ = {'schema': 'funai'}

    id = db.Column(db.Integer, primary_key=True)
    ds_cr = db.Column(db.String(100))
    ciclo_id = db.Column(db.Integer)
    no_ciclo = db.Column(db.String(50))
    nu_orbita = db.Column(db.String(3))
    dt_t_um_formatada = db.Column(db.String(10))
    dt_t_um = db.Column(db.Date)
    cr_nu_area_ha = db.Column(db.Float)
    dg_nu_area_ha = db.Column(db.Float)
    dr_nu_area_ha = db.Column(db.Float)
    ff_nu_area_ha = db.Column(db.Float)
    total_nu_area_ha = db.Column(db.Float)

    def __repr__(self):
        return '<%s - %s - %s - %s - %s - %s - %s - %s - %s - %s - %s - %s>' % (
            self.id, self.ds_cr, self.ciclo_id, self.no_ciclo, self.nu_orbita, self.dt_t_um_formatada, self.dt_t_um,
            self.cr_nu_area_ha, self.dg_nu_area_ha, self.dr_nu_area_ha, self.ff_nu_area_ha, self.total_nu_area_ha)


class VisaoMonitoramentoDiarioPorUf(db.Model):
    __tablename__ = 'vwm_monitora_aviso_diario_por_uf'
    __table_args__ = {'schema': 'funai'}

    id = db.Column(db.Integer, primary_key=True)
    no_uf = db.Column(db.String(100))
    sg_uf = db.Column(db.String(2))
    ciclo_id = db.Column(db.Integer)
    no_ciclo = db.Column(db.String(50))
    nu_orbita = db.Column(db.String(3))
    dt_t_um_formatada = db.Column(db.String(10))
    dt_t_um = db.Column(db.Date)
    cr_nu_area_ha = db.Column(db.Float)
    dg_nu_area_ha = db.Column(db.Float)
    dr_nu_area_ha = db.Column(db.Float)
    ff_nu_area_ha = db.Column(db.Float)
    total_nu_area_ha = db.Column(db.Float)

    def __repr__(self):
        return '<%s - %s - %s - %s - %s - %s - %s - %s - %s - %s - %s - %s - %s>' % (
            self.id, self.no_uf, self.sg_uf, self.ciclo_id, self.no_ciclo, self.nu_orbita, self.dt_t_um_formatada,
            self.dt_t_um, self.cr_nu_area_ha, self.dg_nu_area_ha, self.dr_nu_area_ha, self.ff_nu_area_ha,
            self.total_nu_area_ha)


class TbEntregaTrimestralRadarSat2(db.Model):
    __tablename__ = 'tb_entrega_trimestral_radarsat2'
    __table_args__ = {'schema': 'funai'}

    id = db.Column(db.Integer, primary_key=True)
    nu_entrega = db.Column(db.SmallInteger)
    dt_entrega = db.Column(db.Date)
    st_finalizado = db.Column(db.Boolean)
    qt_total_registro_alerta = db.Column(db.Integer)
    qt_total_registro_agua = db.Column(db.Integer)
    dt_cadastro = db.Column(db.DateTime)

    def __repr__(self):
        return '<%s - %s - %s - %s - %s - %s - %s>' % (
            self.id, self.nu_entrega, self.dt_entrega, self.st_finalizado, self.qt_total_registro_alerta,
            self.qt_total_registro_agua, self.dt_cadastro)


class TbEntregaMensalRadarSat2(db.Model):
    __tablename__ = 'tb_entrega_radarsat2'
    __table_args__ = {'schema': 'funai'}

    id = db.Column(db.Integer, primary_key=True)
    nu_entrega = db.Column(db.SmallInteger)
    dt_entrega = db.Column(db.Date)
    st_finalizado = db.Column(db.Boolean)
    qt_total_registro = db.Column(db.Integer)
    dt_cadastro = db.Column(db.DateTime)

    def __repr__(self):
        return '<%s - %s - %s - %s - %s - %s>' % (
            self.id, self.nu_entrega, self.dt_entrega, self.st_finalizado, self.qt_total_registro, self.dt_cadastro)


class VisaoRadarSat2BeloMonteTiPorClasse(db.Model):
    __tablename__ = 'vwm_painel_radarsat2_belo_monte_ti_por_classe'
    __table_args__ = {'schema': 'funai'}

    id = db.Column(db.Integer, primary_key=True)
    co_funai = db.Column(db.Integer)
    no_ti = db.Column(db.String(100))
    ds_fase_ti = db.Column(db.String(100))
    nu_area_ti_ha = db.Column(db.Float)
    classe_id = db.Column(db.Integer)
    no_classe = db.Column(db.String(50))
    nu_entrega = db.Column(db.Text)
    nu_entrega_nu_area_ha = db.Column(db.Text)

    def __repr__(self):
        return '<%s - %s - %s - %s - %s - %s - %s - %s - %s>' % (
            self.id, self.co_funai, self.no_ti, self.ds_fase_ti, self.nu_area_ti_ha, self.classe_id, self.no_classe,
            self.nu_entrega, self.nu_entrega_nu_area_ha)


class VisaoTabelaInformacaoAtualizacao(db.Model):
    __tablename__ = 'vwm_tabela_informacao_atualizacao'
    __table_args__ = {'schema': 'aplicacao'}

    id = db.Column(db.Integer, primary_key=True)
    dt_atualizacao = db.Column(db.String(10))
    hr_atualizacao = db.Column(db.String(8))
    qtd_registro = db.Column(db.Integer)
    no_esquema = db.Column(db.String)
    no_tabela = db.Column(db.String)
    no_geoserver = db.Column(db.String(100))
    ds_comentario = db.Column(db.Text)
    dt_cadastro = db.Column(db.DateTime)

    def __repr__(self):
        return '<%s - %s - %s - %s - %s - %s - %s - %s>' % (
            self.id, self.dt_atualizacao, self.hr_atualizacao, self.qtd_registro, self.no_esquema, self.no_tabela,
            self.no_geoserver, self.ds_comentario)


class VisaoEntregaAvisoDiarioNuAvisoMaximo(db.Model):
    __tablename__ = 'vw_entrega_aviso_diario_nu_aviso_maximo'
    __table_args__ = {'schema': 'aplicacao'}

    nu_aviso = db.Column(db.Integer, primary_key=True)

    def __repr__(self):
        return '<%s>' % (self.nu_aviso)


class VisaoEntregaMensalDadosRadarSat2(db.Model):
    __tablename__ = 'vw_entrega_mensal_dados_radarsat2'
    __table_args__ = {'schema': 'aplicacao'}

    id = db.Column(db.Integer, primary_key=True)
    nu_entrega = db.Column(db.SmallInteger)
    no_referencia = db.Column(db.String(10))
    dt_entrega = db.Column(db.String(10))
    st_finalizado = db.Column(db.Boolean)
    qt_total_registro = db.Column(db.Integer)
    no_entrega_descricao = db.Column(db.Text)
    dt_cadastro = db.Column(db.String(22))
    nu_frame = db.Column(db.SmallInteger)
    no_frame_concluido = db.Column(db.String(14))
    dt_disponibilizado = db.Column(db.String(22))
    no_arquivo = db.Column(db.Text)

    def __repr__(self):
        return '<%s - %s - %s - %s>' % (self.id, self.nu_entrega, self.no_referencia, self.dt_entrega)


class TbEntregaBufferAte2015(db.Model):
    __tablename__ = "tb_entrega_buffer_ate2015"
    __table_args__ = {"schema": "funai"}

    id = db.Column(db.Integer, primary_key=True)
    tb_usuario_id = db.Column(db.Integer)
    nu_entrega = db.Column(db.Integer)
    dt_entrega = db.Column(db.Date)
    st_finalizado = db.Column(db.Boolean)
    qt_total_registro = db.Column(db.DateTime)
    dt_cadastro = db.Column(db.DateTime)

    def __init__(self, tb_usuario_id, nu_entrega, dt_entrega, st_finalizado, qt_total_registro):
        self.tb_usuario_id = tb_usuario_id
        self.nu_entrega = nu_entrega
        self.dt_entrega = dt_entrega
        self.st_finalizado = st_finalizado
        self.qt_total_registro = qt_total_registro
        self.dt_cadastro = "now()"

    def to_json(self):
        return dict(id=self.id,
                    usuario_id=self.tb_usuario_id,
                    nu_entrega=self.nu_entrega,
                    dt_entrega=self.dt_entrega,
                    st_finalizado=self.st_finalizado,
                    qt_total_registro=self.qt_total_registro,
                    dt_cadastro=self.dt_cadastro)

    def __repr__(self):
        return self.to_json()


class TbUsuarioEntregaBufferAte2015(db.Model):
    __tablename__ = "tb_usuario_entrega_buffer_ate2015"
    __table_args__ = {"schema": "funai"}

    id = db.Column(db.Integer, primary_key=True)
    tb_entrega_id = db.Column(db.Integer)
    img_mascara_buffer_ate2015_id = db.Column(db.Integer)

    def __init__(self, tb_entrega_id, img_mascara_buffer_ate2015_id):
        self.tb_entrega_id = tb_entrega_id
        self.img_mascara_buffer_ate2015_id = img_mascara_buffer_ate2015_id

    def to_json(self):
        return dict(id=self.id,
                    tb_entrega_id=self.tb_entrega_id,
                    img_mascara_buffer_ate2015_id=self.img_mascara_buffer_ate2015_id)

    def __repr__(self):
        return self.to_json()


class VisaoEntregaBufferAte2015(db.Model):
    __tablename__ = 'vw_entrega_buffer_ate2015'
    __table_args__ = {'schema': 'aplicacao'}

    id = db.Column(db.Integer, primary_key=True)
    no_usuario = db.Column(db.String(50))
    nu_entrega = db.Column(db.SmallInteger)
    dt_entrega = db.Column(db.String(10))
    st_finalizado = db.Column(db.Boolean)
    qt_total_registro = db.Column(db.Integer)
    dt_cadastro = db.Column(db.String(22))

    def to_json(self):
        return dict(id=self.id,
                    no_usuario=self.no_usuario,
                    nu_entrega=self.nu_entrega,
                    dt_entrega=self.dt_entrega,
                    st_finalizado=self.st_finalizado,
                    qt_total_registro=self.qt_total_registro,
                    dt_cadastro=self.dt_cadastro)

    def __repr__(self):
        return self.to_json()


class VisaoBaseCartograficaAvisoDiario(db.Model):
    __tablename__ = 'vw_base_cartografica_aviso_diario'
    __table_args__ = {'schema': 'aplicacao'}

    id = db.Column(db.Integer, primary_key=True)
    no_base = db.Column(db.String(30))
    no_orgao = db.Column(db.String(80))
    ds_comentario = db.Column(db.Text)
    dt_atualizacao = db.Column(db.Text)

    def to_json(self):
        return dict(id=self.id,
                    no_base=self.no_base,
                    no_orgao=self.no_orgao,
                    ds_comentario=self.ds_comentario)

    def __repr__(self):
        return self.to_json()


class VisaoServidorServicoMonitorado(db.Model):
    __tablename__ = 'vw_servidor_servico_monitorado'
    __table_args__ = {'schema': 'aplicacao'}

    id = db.Column(db.Integer, primary_key=True)
    monitorado_id = db.Column(db.Integer)
    servico_id = db.Column(db.Integer)
    servidor_id = db.Column(db.Integer)
    no_servidor = db.Column(db.String(200))
    nu_ip = db.Column(db.String(15))
    ds_descricao_servidor = db.Column(db.Text)
    st_ativo_servidor = db.Column(db.Boolean)
    no_servico = db.Column(db.String(150))
    ds_descricao_servico = db.Column(db.Text)
    st_ativo_servico = db.Column(db.Boolean)
    st_status = db.Column(db.String(50))
    ds_status = db.Column(db.String(50))
    st_situacao = db.Column(db.String(50))
    ds_situacao = db.Column(db.String(50))
    dt_servico = db.Column(db.DateTime)
    ds_anotacao = db.Column(db.Text)
    dt_atualizacao = db.Column(db.DateTime)
    dt_cadastro = db.Column(db.DateTime)
    nu_ano = db.Column(db.Text)
    nu_mes = db.Column(db.Text)
    nu_dia = db.Column(db.Text)
    nu_hora = db.Column(db.Text)
    nu_minuto = db.Column(db.Text)
    nu_segundo = db.Column(db.Text)

    def to_json(self):
        return dict(id=self.id,
                    monitorado_id=self.monitorado_id,
                    servico_id=self.servico_id,
                    servidor_id=self.servidor_id,
                    no_servidor=self.no_servidor,
                    nu_ip=self.nu_ip,
                    ds_descricao_servidor=self.ds_descricao_servidor,
                    st_ativo_servidor=self.st_ativo_servidor,
                    no_servico=self.no_servico,
                    ds_descricao_servico=self.ds_descricao_servico,
                    st_ativo_servico=self.st_ativo_servico,
                    st_status=self.st_status,
                    ds_status=self.ds_status,
                    st_situacao=self.st_situacao,
                    ds_situacao=self.ds_situacao,
                    ds_anotacao=self.ds_anotacao,
                    dt_servico=self.dt_servico,
                    dt_atualizacao=self.dt_atualizacao,
                    dt_cadastro=self.dt_cadastro,
                    nu_ano=['','%s Ano' % (self.nu_ano)][len(self.nu_ano) > 0],
                    nu_mes=['','%s Mês' % (self.nu_mes)][len(self.nu_mes) > 0],
                    nu_dia=['','%s Dia' % (self.nu_dia)][len(self.nu_dia) > 0],
                    nu_hora=['','%sh' % (self.nu_hora)][len(self.nu_hora) > 0],
                    nu_minuto=['','%sm' % (self.nu_minuto)][len(self.nu_minuto) > 0],
                    nu_segundo=['','%ss' % (self.nu_segundo)][len(self.nu_segundo) > 0])

    def __repr__(self):
        return self.to_json()

    def to_status_mount(self):
        return self.query.filter(VisaoServidorServicoMonitorado.no_servico.ilike('%.mount')).order_by(text(
            "COALESCE(dt_servico,dt_cadastro) DESC")).all()

    def to_status_notin_active(self):
        return self.query.filter(VisaoServidorServicoMonitorado.st_status.notin_(['active','activating'])).order_by(
            text("COALESCE(dt_servico,dt_cadastro) DESC")).all()

    def to_status_notilike_mount(self):
        return self.query.filter(VisaoServidorServicoMonitorado.st_ativo_servico == 't', VisaoServidorServicoMonitorado.no_servico.notilike('%.mount')).order_by(
            text("COALESCE(dt_servico,dt_cadastro) DESC")).all()

class VisaoEntregaAvisoDiarioAbaAno(db.Model):
    __tablename__ = 'vw_entrega_aviso_diario_aba_ano'
    __table_args__ = {'schema': 'aplicacao'}

    id = db.Column(db.Integer, primary_key=True)
    nu_ano = db.Column(db.Integer)
    ds_aba_ano_entrega = db.Column(db.String(14))
    st_aba_active = db.Column(db.String(6))
    ds_aba_id = db.Column(db.String(14))

    def to_json(self):
        return dict(id=self.id,
                    nu_ano=self.nu_ano,
                    ds_aba_ano_entrega=self.ds_aba_ano_entrega,
                    st_aba_active=self.st_aba_active,
                    ds_aba_id=self.ds_aba_id)

    def __repr__(self):
        return self.to_json()

class VisaoAlertaUrgenteGerado(db.Model):
    __tablename__ = 'vw_alerta_urgente_gerado'
    __table_args__ = {'schema': 'funai'}

    id = db.Column(db.Integer, primary_key=True)
    nu_referencia = db.Column(db.Integer)
    no_ciclo = db.Column(db.Text)
    no_ti = db.Column(db.Text)
    ds_cr = db.Column(db.String(100))
    no_municipio = db.Column(db.Text)
    no_usuario = db.Column(db.String(50))
    no_estagio = db.Column(db.Text)
    dt_envio = db.Column(db.String(10))
    nu_total_poligonos = db.Column(db.Integer)

    def to_json(self):
        return dict(id=self.id, 
                    nu_referencia=self.nu_referencia, 
                    no_ciclo=self.no_ciclo, 
                    no_ti=self.no_ti, 
                    ds_cr=self.ds_cr, 
                    no_municipio=self.no_municipio, 
                    no_usuario=self.no_usuario, 
                    no_estagio=self.no_estagio,
                    dt_envio=self.dt_envio,
                    nu_total_poligonos=self.nu_total_poligonos)

    def __repr__(self):
        return self.to_json()

class VisaoUsuarioInterpreteMonitora(db.Model):
    __tablename__ = 'vw_usuario_interprete_monitora'
    __table_args__ = {'schema': 'aplicacao'}

    id = db.Column(db.Integer, primary_key=True)
    no_usuario = db.Column(db.String(50))
    ds_email = db.Column(db.String(50))
    no_tabela_alerta = db.Column(db.String(150))
    no_tabela = db.Column(db.String(150))

    def to_json(self):
        return dict(id=self.id, 
                    no_usuario=self.no_usuario, 
                    ds_email=self.ds_email, 
                    no_tabela_alerta=self.no_tabela_alerta, 
                    no_tabela=self.no_tabela)

    def __repr__(self):
        return self.to_json()

class VisaoInfoAreaTotalTerraIndigenaAMZ(db.Model):
    __tablename__ = 'vwm_informacao_area_total_terra_indigena_amz_km2'
    __table_args__ = {'schema': 'aplicacao'}

    nu_ordem = db.Column(db.Integer, primary_key=True)
    no_limite = db.Column(db.Text)
    nu_quantidade = db.Column(db.Integer)
    nu_area_km2 = db.Column(db.Text)
    dt_ultima_atualizacao = db.Column(db.String(10))

    def to_json(self):
        return dict(nu_ordem=self.nu_ordem,
                    no_limite=self.no_limite,
                    nu_quantidade=self.nu_quantidade,
                    nu_area_km2=self.nu_area_km2,
                    dt_ultima_atualizacao=self.dt_ultima_atualizacao)

    def __repr__(self):
        return self.to_json()
