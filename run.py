# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

import os

from app import app

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port)