# -*- encoding: utf-8 -*-
# Copyright 2016 Altamiro Rodrigues <altamiro.rodrigues@hexgis.com>

from app import app, manager

from app.command.awifs_deter_b import awifs
from app.command.terra_indigena import terra_indigena
from app.command.terra_indigena_estudo import terra_indigena_estudo
from app.command.coordenacao_regional import coordenacao_regional
from app.command.aldeia_indigena import aldeia_indigena
from app.command.criar_entrega_buffer_ate2015 import criar_entrega_buffer_ate2015
from app.command.email_servicos_monitorados import email_servicos_monitorados
from app.command.atualizar_catalogo_landsat import atualizar_catalogo_landsat
from app.command.novo_deter_a import novo_deter_a
from app.command.deter_public_awifs import deter_public_awifs
from app.command.novo_awifs_deter_b import novo_awifs_deter_b
from app.command.atualizar_json_imagens_aviso import atualizar_json_imagens_aviso


app.app_context().push()
if __name__ == "__main__":
    manager.run()
